"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'lawandgauge.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.ModelList(
            _('Administration'),
            column=1,
            collapsible=False,
            models=(
                'account.models.User',
                'django.contrib.auth.models.Group',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Accounts'),
            column=1,
            collapsible=False,
            models=(
                'account.models.Admin',
                'account.models.Contractor',
                'account.models.InvitedContractor',
                'account.models.Client',
                'account.models.PhoneCode',
                'account.models.SharedMap',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Services'),
            column=1,
            collapsible=False,
            models=(
                'account.models.LawBranch',
                'account.models.LawInstitute',
                'account.models.ServiceTip',
                'account.models.Service',
                'account.models.Currency',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Chat'),
            collapsible=False,
            column=1,
            models=(
                'chat.models.Chat',
                'chat.models.Message',
                'chat.models.Attachment',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Map'),
            collapsible=False,
            column=1,
            models=(
                'map.models.Country',
                'map.models.City',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Localization'),
            collapsible=False,
            column=1,
            models=(
                'localization.models.TranslatedText',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Mail'),
            collapsible=False,
            column=1,
            models=(
                'mail.models.*',
            ),
        ))

        self.children.append(modules.Group(
            title=_("Landing builder"),
            column=1,
            collapsible=True,
            children=[
                modules.ModelList(
                    _('Main'),
                    models=[
                        'landing_builder.models.Landing',
                        'landing_builder.models.DeletedLandings',
                    ]
                ),
                modules.ModelList(
                    title=_('Screens'),
                    models=(
                        'landing_builder.models.MainScreen',
                        'landing_builder.models.DescriptionScreen',
                        'landing_builder.models.AdvantagesScreen',
                        'landing_builder.models.FeedbackScreen',
                        'landing_builder.models.MapScreen',
                        'landing_builder.models.SignupScreen',
                        'landing_builder.models.OtherLangingsScreen',
                        'landing_builder.models.PartnersScreen',
                        'landing_builder.models.ContactsScreen',
                        'landing_builder.models.FaqScreen',
                        'landing_builder.models.PricesScreen',
                    )
                ),
                modules.ModelList(
                    title=_('Data'),
                    models=(
                        'landing_builder.models.Advantage',
                        'landing_builder.models.Feedback',
                        'landing_builder.models.Partner',
                        'landing_builder.models.Office',
                        'faq.models.FaqItem',
                        'landing_builder.models.Package',
                        # 'landing_builder.models.FooterItem',
                    )
                )
            ]
        ))

        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=3,
        ))


