from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import TemplateView

from faq.views import FaqView
from landing.views import LandingView
from landing_builder.views import LandingBuilderView, LandingRegistration
from lawandgauge.settings import MEDIA_URL, MEDIA_ROOT

urlpatterns = [

    # url(r'^$', IndexView.as_view(), name='index'),
    url(r'^$', LandingView.as_view(), name='index'),
    url(r'^chat/', include('chat.urls', namespace='chat', app_name='chat')),
    url(r'^account/', include('account.urls', namespace='account', app_name='account')),
    url(r'^landing/', include('landing.urls', namespace='landing', app_name='landing')),
    url(r'^map/', include('map.urls', namespace='map', app_name='map')),
    url(r'^localization/', include('localization.urls', namespace='localization', app_name='localization')),

    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
] + static(MEDIA_URL, document_root=MEDIA_ROOT)



urlpatterns += [
    url(r'^about/', TemplateView.as_view(template_name='landing/about.html'), name='about'),
    url(r'^ru/about/', TemplateView.as_view(template_name='landing/about_ru.html'), name='about'),
    url(r'^en/about/', TemplateView.as_view(template_name='landing/about_en.html'), name='about'),
    url(r'^our_lawyers/', TemplateView.as_view(template_name='landing/our_lawyers.html'), name='our_lawyers'),
    url(r'^mission/', TemplateView.as_view(template_name='landing/mission.html'), name='mission'),
    url(r'^privacy_policy/', TemplateView.as_view(template_name='landing/privacy_policy.html'), name='privacy'),
    url(r'^angular/views/chat/', TemplateView.as_view(template_name='views/chat.html')),
    url(r'^angular/views/profile/', TemplateView.as_view(template_name='views/profile.html')),
    url(r'^angular/views/login/', TemplateView.as_view(template_name='views/login.html')),
    url(r'^angular/views/landing/', TemplateView.as_view(template_name='views/landing.html')),
    url(r'^angular/templates/header', TemplateView.as_view(template_name='templates/header.html')),
    url(r'^angular/templates/sidebar/', TemplateView.as_view(template_name='templates/sidebar.html')),
]

landing_builder_patterns = [
    url(r'^lb/job_request/$', LandingRegistration.as_view(), name='job_request'),
    url(r'^(?P<slug>[\w\-]+)/$', LandingBuilderView.as_view(), name='landing_builder')
]

urlpatterns += i18n_patterns(
    url(r'^faq/$', FaqView.as_view(), name='faq'),
    url(r'^', include(landing_builder_patterns)),
)
