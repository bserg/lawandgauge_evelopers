from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
import json
from django.http import HttpResponse


class JsonResponseMixin(object):
    def json_response(self, **kwargs):
        return HttpResponse(json.dumps(kwargs), 'json')

    def json_success(self, msg=None, **kwargs):
        return self.json_response(success=True, msg=msg, **kwargs)

    def json_fail(self, error=None, **kwargs):
        return self.json_response(success=False, error=error, **kwargs)


class LoginRequiredJsonMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(LoginRequiredJsonMixin, self).dispatch(request, *args, **kwargs)
        else:
            return self.json_fail(error=_('User is not authenticated'))


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class RequiredPermissionsJsonMixin(object):
    required_permissions = []

    def get_required_permissions(self):
        return self.required_permissions

    def dispatch(self, request, *args, **kwargs):
        if self.required_permissions and not request.user.has_perms(self.get_required_permissions()):
            return self.json_fail(error=_('You have not access to this action'))
        return super(RequiredPermissionsJsonMixin, self).dispatch(request, *args, **kwargs)