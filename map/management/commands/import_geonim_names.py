import csv

from django.core.management.base import BaseCommand

YANDEX_TRANSLATE_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
GOOGLE_TRANSLATE_URL = 'https://www.googleapis.com/language/translate/v2'

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--type')
        parser.add_argument('path', type=str)

    def handle(self, *args, **options):
        type = options['type']
        path = options['path']
        f = open(path, 'rb')
        r = csv.reader(f, delimiter="|")
        if type == 'city':
            for i in r:
                pass

        elif type == 'country':
            pass
        else:
            return