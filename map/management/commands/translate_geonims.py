from django.core.management.base import NoArgsCommand, BaseCommand
from django.core.paginator import Paginator
from django.db.models import Q
import requests
from account.models import Localization
from lawandgauge.settings import YANDEX_TRANSLATE_KEY, GOOGLE_API_KEY
from localization.models import Locale, Translation
from map.models import Country, CountryLocalization, City, CityLocalization

YANDEX_TRANSLATE_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
GOOGLE_TRANSLATE_URL = 'https://www.googleapis.com/language/translate/v2'

class Command(BaseCommand):

    def get_yandex_translate(self, lang, text):
        params = {
            'key': YANDEX_TRANSLATE_KEY,
            'lang': 'en-' + lang,
            'text': text,
            'format': 'plain'
        }
        r = requests.post(YANDEX_TRANSLATE_URL, params=params)
        if r.status_code == 200:
            data = r.json()
            if data['code'] == 200:
                return data['text'][0]
            else:
                print 'ERROR', data

    def get_google_translate(self, lang, text):
        params = {
            'key': GOOGLE_API_KEY,
            'format': 'text',
            'q': text,
            'source': 'en',
            'target': lang
        }
        r = requests.post(GOOGLE_TRANSLATE_URL, params=params)
        if r.status_code == 200:
            data = r.json()
            return data['data']['translations'][0]['translatedText']

    def handle(self, *args, **options):
        import urllib3
        urllib3.disable_warnings()
        locales = Locale.objects.all()
        countries = Country.objects.all()
        separator = u'";"'
        for locale in locales:
            countries_lang = countries.filter(translations__value='', translations__locale=locale)
            string = u'"%s"' % separator.join(countries_lang.values_list('name', flat=True))

            translate = self.get_yandex_translate(locale.code, string)

            if translate:
                for country, translated_name in zip(countries_lang, translate.strip('"').split(separator)):
                    if translated_name:
                        country.translations.filter(locale=locale).update(value=translated_name.capitalize())
                        print 'COUNTRY TRANSLATED', locale.code, country.name, translated_name.encode('utf-8')

        cities_all = City.objects.all()
        for locale in locales:
            paginator = Paginator(cities_all.filter(translations__value='', translations__locale=locale), 100)
            for page in range(paginator.num_pages):
                cities = paginator.page(page).object_list
                string = u'"%s"' % separator.join(cities.values_list('name', flat=True))
                translate = self.get_yandex_translate(locale.code, string)
                try:
                    translated_names = translate.strip('"').split(separator)
                except:
                    translated_names = []
                if not translate or (translated_names and len(translated_names) != cities.count()):
                    print 'ERROR', 'translating not found. Trying individual translating...'
                    for city in cities:
                        translated_name = self.get_yandex_translate(locale.code, city.name)
                        if translated_name:
                            city.translations.filter(locale=locale).update(value=translated_name.capitalize())
                            print 'CITY TRANSLATED INDIVIDUALLY', locale.code, city.name.encode('utf-8'),  translated_name.encode('utf-8')
                else:
                    for city, translated_name in zip(cities, translated_names):
                        if translated_name:
                            city.translations.filter(locale=locale).update(value=translated_name.capitalize())
                            print 'CITY TRANSLATED', locale.code, city.name.encode('utf-8'),  translated_name.encode('utf-8')

