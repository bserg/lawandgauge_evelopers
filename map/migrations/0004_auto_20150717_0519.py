# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.contenttypes.models import ContentType

from django.db import models, migrations
from localization.models import Translation


def translated_names_to_field(app, schema_editor):
    db_alias = schema_editor.connection.alias
    _models = ['City', 'Country']
    for model_name in _models:
        model = app.get_model('map', model_name)
        ct = ContentType.objects.get_for_model(model)
        for item in model.objects.using(db_alias).all():
            translation = Translation.objects.using(db_alias).filter(content_type=ct, object_id=item.pk, locale__code='ru').first()
            print translation
            if translation:
                item.name_ru = translation.value
                item.save()

class Migration(migrations.Migration):

    dependencies = [
        ('map', '0003_auto_20150525_0751'),
    ]

    operations = [
        migrations.RenameField(
            model_name='city',
            old_name='name',
            new_name='name_en'
        ),
        migrations.RenameField(
            model_name='country',
            old_name='name',
            new_name='name_en'
        ),
        migrations.AlterField(
            model_name='city',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='city',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='country',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='country',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.DeleteModel(
            name='CityLocalization',
        ),
        migrations.DeleteModel(
            name='CountryLocalization',
        ),
        migrations.RunPython(translated_names_to_field, reverse_code=migrations.RunPython.noop),
    ]
