# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('google_id', models.CharField(max_length=255, null=True, verbose_name='Google ID', blank=True)),
                ('lat', models.FloatField(default=0, verbose_name='Latitude')),
                ('lng', models.FloatField(default=0, verbose_name='Longitude')),
            ],
            options={
                'verbose_name': '\u0413\u043e\u0440\u043e\u0434',
                'verbose_name_plural': 'Cities',
            },
        ),
        migrations.CreateModel(
            name='CityLocalization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=10, verbose_name='\u041a\u043e\u0434')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('city', models.ForeignKey(related_name='localizations', verbose_name='\u0413\u043e\u0440\u043e\u0434', to='map.City')),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('code', models.CharField(max_length=10, verbose_name='\u041a\u043e\u0434')),
                ('google_id', models.CharField(max_length=255, null=True, verbose_name='Google ID', blank=True)),
            ],
            options={
                'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0430',
                'verbose_name_plural': 'Countries',
            },
        ),
        migrations.CreateModel(
            name='CountryLocalization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=10, verbose_name='\u041a\u043e\u0434')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('country', models.ForeignKey(related_name='localizations', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', to='map.Country')),
            ],
        ),
        migrations.AddField(
            model_name='city',
            name='country',
            field=models.ForeignKey(related_name='cities', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True, to='map.Country', null=True),
        ),
    ]
