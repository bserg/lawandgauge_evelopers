# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0002_auto_20150522_2101'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='city',
            options={'verbose_name': 'City', 'verbose_name_plural': 'Cities'},
        ),
        migrations.AlterModelOptions(
            name='country',
            options={'verbose_name': 'Country', 'verbose_name_plural': 'Countries'},
        ),
        migrations.AlterField(
            model_name='city',
            name='country',
            field=models.ForeignKey(related_name='cities', verbose_name='Country', blank=True, to='map.Country', null=True),
        ),
        migrations.AlterField(
            model_name='city',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='citylocalization',
            name='city',
            field=models.ForeignKey(related_name='localizations', verbose_name='City', to='map.City'),
        ),
        migrations.AlterField(
            model_name='citylocalization',
            name='code',
            field=models.CharField(max_length=10, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='citylocalization',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='country',
            name='code',
            field=models.CharField(max_length=10, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='country',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='countrylocalization',
            name='country',
            field=models.ForeignKey(related_name='localizations', verbose_name='Country', to='map.Country'),
        ),
        migrations.AlterField(
            model_name='countrylocalization',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
    ]
