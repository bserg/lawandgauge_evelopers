from django.db.models import Count
from django.utils.translation import ugettext as _, get_language
from django.views.generic import FormView, View
import requests
from account.models import User, TYPE_CONTRACTOR

from lawandgauge.mixins import JsonResponseMixin
from lawandgauge.settings import GOOGLE_API_KEY
from map.forms import SearchCountryForm, SearchCityForm
from map.models import Country, City

GOOGLE_GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json'


class GetCountries(JsonResponseMixin, View):
    def get(self, request):
        return self.json_success(
            data=[country.as_dict() for country in Country.objects.all()]
        )


class SearchCountry(JsonResponseMixin, FormView):
    #TODO: refactor it!
    form_class = SearchCountryForm

    def form_valid(self, form):
        params = form.cleaned_data
        params['key'] = GOOGLE_API_KEY
        r = requests.get(GOOGLE_GEOCODE_URL, params=params)
        if not r.status_code == 200:
            return self.json_fail(error=_('Service unavailable'))
        data = r.json()
        if data and data.get('status') == 'OK':
            for result in data.get('results', []):
                adr = result.get('address_components')[0]
                if adr.get('types')[0] == 'country':
                    item = {
                        'name': adr.get('long_name'),
                        'code': adr.get('short_name'),
                        'id': result.get('place_id'),
                        'lat': result['geometry']['location']['lat'],
                        'lng': result['geometry']['location']['lng'],
                    }
                    country, created = Country.objects.get_or_create(name=item['name'], code=item['code'], google_id=item['id'])

                    return self.json_success(**country.as_dict(lang=params.get('language')))

            return self.json_fail(error=_('Country not found'))

        return self.json_fail(error=data.get('status'))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class GetCities(JsonResponseMixin, View):
    def get(self, request):
        return self.json_success(
            data=[city.as_dict() for city in City.objects.filter(country=request.GET.get('country'))]
        )


class SearchCity(JsonResponseMixin, FormView):
    #TODO: refactor it!
    form_class = SearchCityForm

    def get(self, request, *args, **kwargs):
        params = dict(address=request.GET.get('city', ''))
        params['key'] = GOOGLE_API_KEY
        r = requests.get(GOOGLE_GEOCODE_URL, params=params)
        if not r.status_code == 200:
            return self.json_fail(error=_('Service unavailable'))
        data = r.json()
        if data and data.get('status') == 'OK':
            for result in data.get('results', []):
                adr = result.get('address_components')[0]
                if adr.get('types')[0] == 'locality':
                    item = {
                        'name': adr.get('long_name'),
                        'code': adr.get('short_name'),
                        'id': result.get('place_id'),
                        'lat': result['geometry']['location']['lat'],
                        'lng': result['geometry']['location']['lng'],
                    }

                    return self.json_success(**item)

            return self.json_fail(error=_('City not found'))

        return self.json_fail(error=data.get('status'))

    def form_valid(self, form):
        params = form.cleaned_data
        params['key'] = GOOGLE_API_KEY
        r = requests.get(GOOGLE_GEOCODE_URL, params=params)
        if not r.status_code == 200:
            return self.json_fail(error=_('Service unavailable'))
        data = r.json()
        if data and data.get('status') == 'OK':
            for result in data.get('results', []):
                adr = result.get('address_components')[0]
                if adr.get('types')[0] == 'locality':
                    item = {
                        'name': adr.get('long_name'),
                        'code': adr.get('short_name'),
                        'id': result.get('place_id'),
                        'lat': result['geometry']['location']['lat'],
                        'lng': result['geometry']['location']['lng'],
                    }
                    city, created = City.objects.get_or_create(country=params.get('country'), name=item['name'])
                    if created:
                        city.google_id = item['id']
                        city.lat, city.lng = item['lat'], item['lng']
                        city.save()

                    return self.json_success(**city.as_dict(params.get('language')))

            return self.json_fail(error=_('City not found'))

        return self.json_fail(error=data.get('status'))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class GetMapData(JsonResponseMixin, View):
    def get(self, request):
        contractors = User.objects.filter(is_active=True, type=TYPE_CONTRACTOR).order_by('-date_joined')
        cities = City.objects.filter(pk__in=contractors.values_list('city_id', flat=True))\
            .annotate(contractors_count=Count('users'))
        cities_data = [{
            'id': c.pk,
            'google_id': c.google_id,
            'name': c.name,
            'count': c.contractors_count,
            'lat': c.lat,
            'lng': c.lng,
            'contractors': [{'first_name': contractor.first_name,
                             'last_name': contractor.last_name,
                             'avatar': contractor.get_avatar(177, 177)
                             } for contractor in contractors.filter(city=c)]
        } for c in cities]

        return self.json_success(data=cities_data)
