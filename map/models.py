from django.db import models
from django.utils.translation import ugettext as _, get_language

from localization.models import TranslatedModelMixin


class Country(TranslatedModelMixin, models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    code = models.CharField(_('Code'), max_length=10)
    google_id = models.CharField(_('Google ID'), max_length=255, blank=True, null=True)

    @property
    def name(self):
        field = 'name_' + get_language()
        if hasattr(self, field):
            return getattr(self, field)

    def as_dict(self):
        data = {
            'id': self.pk,
            'name': self.name,
            'code': self.code,
            'google_id': self.google_id
        }
        return data

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')


class City(TranslatedModelMixin, models.Model):
    country = models.ForeignKey(Country, verbose_name=_('Country'), related_name='cities', blank=True, null=True)
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    google_id = models.CharField(_('Google ID'), max_length=255, blank=True, null=True)
    lat = models.FloatField(_('Latitude'), default=0)
    lng = models.FloatField(_('Longitude'), default=0)

    @property
    def name(self):
        field = 'name_' + get_language()
        if hasattr(self, field):
            return getattr(self, field)

    def as_dict(self):
        data = {
            'id': self.pk,
            'country': self.country_id,
            'name': self.name,
            'google_id': self.google_id,
            'lat': self.lat,
            'lng': self.lng
        }
        return data

    @staticmethod
    def autocomplete_search_fields():
        return ('name_ru__icontains',)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('City')
        verbose_name_plural = _('Cities')
