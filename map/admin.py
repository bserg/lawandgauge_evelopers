from django.contrib import admin

from map.models import Country, City


class CountryAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'code', 'google_id')
    search_fields = ['name_en', 'name_ru']

admin.site.register(Country, CountryAdmin)

class CityAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'lat', 'lng', 'google_id')
    search_fields = ['name_en', 'name_ru']

admin.site.register(City, CityAdmin)