from django import forms
from map.models import Country


class SearchCountryForm(forms.Form):
    address = forms.CharField(min_length=3)
    language = forms.CharField(required=False)


class SearchCityForm(forms.Form):
    country = forms.ModelChoiceField(queryset=Country.objects.all(), required=False)
    address = forms.CharField(min_length=3)
    language = forms.CharField(required=False)


class CountryForm(forms.ModelForm):
    class Meta:
        model = Country
        exclude = []
