from django.contrib import admin
from faq.models import FaqItem


class FaqItemAdmin(admin.ModelAdmin):
    list_display = ['question', 'order', 'active']

admin.site.register(FaqItem, FaqItemAdmin)