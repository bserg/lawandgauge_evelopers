from django.db import models
from django.utils.translation import ugettext_lazy as _, get_language


class FaqItem(models.Model):
    question_ru = models.CharField(_('Question') + ' (ru)', max_length=255, blank=True)
    question_en = models.CharField(_('Question') + ' (en)', max_length=255, blank=True)
    answer_ru = models.TextField(_('Answer') + ' (ru)', blank=True)
    answer_en = models.TextField(_('Answer') + ' (en)', blank=True)
    anchor_link = models.CharField(_('Anchor link'), max_length=50, unique=True)
    order = models.PositiveSmallIntegerField(_('Order'), default=0)
    active = models.BooleanField(_('Active'), default=True)

    @property
    def question(self):
        return getattr(self, 'question_' + get_language(), None)

    @property
    def answer(self):
        return getattr(self, 'answer_' + get_language(), None)

    def __unicode__(self):
        return self.question

    class Meta:
        ordering = ['order']
        verbose_name = _('FAQ item')
        verbose_name_plural = _('FAQ')
