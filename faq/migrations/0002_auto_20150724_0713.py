# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='faqitem',
            old_name='answer',
            new_name='answer_ru',
        ),
        migrations.RenameField(
            model_name='faqitem',
            old_name='question',
            new_name='question_ru',
        ),
        migrations.AddField(
            model_name='faqitem',
            name='answer_en',
            field=models.CharField(max_length=255, verbose_name='Answer (en)', blank=True),
        ),
        migrations.AddField(
            model_name='faqitem',
            name='question_en',
            field=models.CharField(max_length=255, verbose_name='Question (en)', blank=True),
        ),
    ]
