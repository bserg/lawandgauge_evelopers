# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FaqItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=255, verbose_name='Question')),
                ('answer', models.TextField(verbose_name='Answer')),
                ('anchor_link', models.CharField(unique=True, max_length=50, verbose_name='Anchor link')),
                ('order', models.PositiveSmallIntegerField(default=0, verbose_name='Order')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': 'FAQ item',
                'verbose_name_plural': 'FAQ',
            },
        ),
    ]
