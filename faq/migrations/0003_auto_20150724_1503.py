# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0002_auto_20150724_0713'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faqitem',
            name='answer_en',
            field=models.TextField(verbose_name='Answer (en)', blank=True),
        ),
        migrations.AlterField(
            model_name='faqitem',
            name='answer_ru',
            field=models.TextField(verbose_name='Answer (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='faqitem',
            name='question_ru',
            field=models.CharField(max_length=255, verbose_name='Question (ru)', blank=True),
        ),
    ]
