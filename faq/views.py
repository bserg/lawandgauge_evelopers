from django.views.generic import ListView

from faq.models import FaqItem


class FaqView(ListView):
    queryset = FaqItem.objects.filter(active=True)
    context_object_name = 'items'
    template_name = 'faq.html'