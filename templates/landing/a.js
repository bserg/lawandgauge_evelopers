var styles = [
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#c1c1c1"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "color": "#808080"
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#b3b3b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dadada"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "weight": 1.8
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#d7d7d7"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ebebeb"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#a7a7a7"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ebebeb"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#696969"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#737373"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#c9c9c9"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {},
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#dadada"
            }
        ]
    },
]
var map;
var service;
var geocoder;
var bounds;
var markerImage;
var infoWindow;
var markers = [];
var visible_markers = [];

var metro = [];

var RADIUS = 30000;
var RADIUS_SUBWAY_STATION = 3500;
var loaded_city = {};


function initialize() {

    // INIT MAP AND PLACE_SEARCH //

    var mapOptions = {
        zoom: 1,
        center: new google.maps.LatLng(55.749792, 37.632495),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: styles,
        disableDefaultUI: true
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    service = new google.maps.places.PlacesService(map);
    infoWindow = new google.maps.InfoWindow({
        // align tooltip
        pixelOffset: new google.maps.Size(-7, 0)
    });
    bounds = new google.maps.LatLngBounds();
    geocoder = new google.maps.Geocoder();

    // INIT MARKER IMAGES //

    markerImage = {
        'cafe': {
            url: '/static/movenpick/images/cafe_marker.png',
            size: new google.maps.Size(44, 57),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 38),
            scaledSize: new google.maps.Size(30, 39)
        },
        'shop': {
            url: '/static/movenpick/images/shop_marker.png',
            size: new google.maps.Size(44, 57),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 38),
            scaledSize: new google.maps.Size(30, 39)
        },
        'restaurant': {
            url: '/static/movenpick/images/restaurant_marker.png',
            size: new google.maps.Size(44, 57),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 38),
            scaledSize: new google.maps.Size(30, 39)
        },
        'boutique': {
            url: '/static/movenpick/images/restaurant_marker.png',
            size: new google.maps.Size(44, 57),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 38),
            scaledSize: new google.maps.Size(30, 39)
        }
    }

    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
        map.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });

    google.maps.event.trigger(map, 'resize');

    for (var i = 0; i < backendPlaces.length; i++) {
        createMarker(backendPlaces[i]);
    }
    if (goto){
        for (var i=0; i<markers.length; i++){
            if (markers[i].id == goto){
                markers[i].setMap(map);
                map.setCenter(markers[i].position);
                map.setZoom(16);
                google.maps.event.trigger(markers[i], 'click');
            }
        }
    } else {
        var city = $('input[name=city]').val();
        geocoder.geocode({ 'address': city}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                map.setZoom(14);
                setTimeout(showClosestMarkers(results[0].geometry.location, RADIUS), 0 );
            } else {
                console.log(status);
            }
        });
    }
}

//____________DRAW MARKERS_________________//

function createMarker(place) {
    var position = new google.maps.LatLng(place.lat, place.lng);
    var image = markerImage[place.type];
    bounds.extend(position);
    var marker = new google.maps.Marker({
        position: position,
        icon: image,
        id: place.id,
        title: place.name,
        animation: google.maps.Animation.DROP
    });

    marker.type = place.type;

    markers.push(marker);

    var tooltip_content = '<div id="balloon">' +
        '<h3>' + place.name + '</h3>' +
        '<div class="desc">' + place.description + '</div>' +
        '<div class="adr">' + place.address + '</div>' +
        '<div class="ph">' + place.phone + '</div>' +
        '</div>';

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.setContent(tooltip_content);
        infoWindow.open(map, this);
    });

    // animation
    function toggleBounce() {
        marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function(){marker.setAnimation(null);}, 750);
    }

    google.maps.event.addListener(marker, 'click', toggleBounce);

    // add element to list of places in filter form
    var li = '<li style="display:none;" id="forMarker_' + place.id + '" class="' + place.type.substring(0, 4) + '"><span>' + place.address + '</span></li>';
    $('ul.adress').append(li);
     $(".scroll").customScrollbar("resize", true);
    google.maps.event.addDomListener(document.getElementById("forMarker_" + place.id), "click",
        function () {
            map.setCenter(position);
            map.setZoom(16);
            google.maps.event.trigger(marker, 'click')
        }
    );
}

function findCity(city, callback){
    geocoder.geocode({ 'address': city}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            callback(results[0]);
        }
    });
}

function findMetro(city, name, callback) {
    geocoder.geocode({ 'address': city}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

            var request = {
                location: results[0].geometry.location,
                radius: RADIUS,
                name: name,
                types: ['subway_station'],
                language: 'ru'
            };

            service.nearbySearch(request, function (results, status) {
                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    callback(results[0])
                }
            });
        }
    });
}

//____________SHOW CLOSEST MARKERS TO LOCATION___________//

function rad(x) {
    return x * Math.PI / 180;
}

function showClosestMarkers(location, radius, type, chain) {
    var bounds = new google.maps.LatLngBounds();

    //clear list of places in filter bar
    $('ul.adress li').css('display', 'none');

    //calc closest markers
    var lat = location.lat();
    var lng = location.lng();
    var R = 6371000;
    for (i = 0; i < markers.length; i++) {
        if (type && markers[i].type != type){
            continue
        }
        if (type && chain && (markers[i].type != type || markers[i].title.toLowerCase().indexOf(chain.toLowerCase()) == -1)) {
            continue
        }
        var mlat = markers[i].position.lat();
        var mlng = markers[i].position.lng();
        var dLat = rad(mlat - lat);
        var dLong = rad(mlng - lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        if (d < radius) {

            visible_markers.push(markers[i]);

            //add to list in filter bar
            $('ul.adress li[id=forMarker_' + markers[i].id + ']').css('display', 'block');
            $(".scroll").customScrollbar("resize", true);
            setMarkersMap(map, visible_markers)
            bounds.extend(markers[i].position);
            map.fitBounds(bounds);
            if (map.zoom <= 11){
                map.setZoom(map.zoom + 2);
            } else if(map.zoom > 16){
                map.setZoom(16);
            }
        }
    }
}

function showMarkersInCity(city){
    geocoder.geocode({ 'address': city}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            showClosestMarkers(results[0].geometry.location, RADIUS)
        }
    })
}

//____________MANAGE MARKERS___________//

function setMarkersMap(map, markers){
    for (var i=0; i<markers.length; i++){
        markers[i].setMap(map);
    }
}

function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearAllMarkers() {
    setAllMap(null);
}

function showAllMarkers() {
    setAllMap(map);
}

function deleteAllMarkers() {
    clearMarkers();
    markers = [];
}

google.maps.event.addDomListener(window, 'load', initialize);