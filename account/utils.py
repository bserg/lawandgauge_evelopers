import hashlib
import re
from account.models import User, PhoneCode, TYPE_CLIENT, TYPE_CONTRACTOR
from landing.models import ClientIntention, Resume
from lawandgauge.settings import VK_SECRET_KEY, VK_APP_ID


def check_phone_code(phone, code):
    """
    Validation phone with code
    """
    try:
        phone_code = PhoneCode.objects.get(phone=phone, code=code, active=True)
    except:
        return False
    phone_code.active = False
    phone_code.save()
    return True


def check_vk_auth(user_id, request):
    if not request:
        return False
    vk_auth_string = request.COOKIES.get('vk_app_' + VK_APP_ID)
    if not vk_auth_string:
        return False
    params = {param.split('=')[0]: param.split('=')[1] for param in vk_auth_string.split('&')}
    if user_id != params.get('mid'):
        return False
    user_id = params.get('mid')
    sid = params.get('sid')
    secret = params.get('secret')
    expire = params.get('expire')
    sig = params.get('sig')

    try:
        string = 'expire=' + expire + 'mid=' + user_id + 'secret=' + secret + 'sid=' + sid + VK_SECRET_KEY
        m = hashlib.md5()
        m.update(string)

        if m.hexdigest().lower() == sig.lower():
            return True
    except:
        return False


def check_fb_auth(user_id, access_token):
    import facebook
    try:
        graph =facebook.GraphAPI(access_token=access_token)
        user_data = graph.get_object('me')
        id = user_data.get('id')
    except:
        return False

    if id == user_id:
        return True


class PhoneAuthBackend(object):
    def authenticate(self, phone=None, code=None, **kwargs):
        try:
            user = User.objects.get(phone=re.sub(r'[\s\-+]', '', phone))
        except User.DoesNotExist:
            return
        except:
            return
        if not check_phone_code(phone, code):
            return
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class VKAuthBackend(object):
    def authenticate(self, user_id=None, **kwargs):
        social = kwargs.get('social')
        if social != 'vk':
            return
        try:
            user = User.objects.get(vk_id=user_id)
        except User.DoesNotExist:
            return
        except:
            return
        if not check_vk_auth(user_id, kwargs.get('request')):
            return
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class FBAuthBackend(object):
    def authenticate(self, user_id=None, **kwargs):
        social = kwargs.get('social')
        if social != 'fb':
            return
        try:
            user = User.objects.get(fb_id=user_id)
        except User.DoesNotExist:
            return
        except:
            return
        access_token = kwargs.get('access_token')
        if not check_fb_auth(user_id, access_token):
            return
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
