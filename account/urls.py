from django.conf.urls import patterns, url
from account.views import Login, Logout, GetCurrentUserView, SetTimezone, GetUsersView, PhoneCodeGenerating, \
    Registration, RegistrationUpdate, GetServiceTips, GetCurrencies, UploadImageToVK, AvatarUpdateView, \
    MainInfoUpdateView, AddressUpdateView, ProfessionalInfoUpdateView, DiplomCreateView, LicenseCreateView, \
    DiplomDeleteView, LicenseDeleteView, AssociationCreateView, AssociationDeleteView, NotificationsUpdateView, \
    ServiceCreateView, ServiceDeleteView, UploadImageToFB, PhoneUpdateView, ResumeDeleteView, ConnectVKAuth, \
    ConnectFBAuth, CheckRegistration, LawBranchCreateView, LawBranchDeleteView, LawInstituteCreateView, \
    LawInstituteDeleteView

urlpatterns = patterns('',
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^login/status/$', Login.as_view(), name='login_status'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^data/$', GetCurrentUserView.as_view(), name='get_current_user'),
    url(r'^tz/$', SetTimezone.as_view(), name='set_timezone'),
    url(r'^users/$', GetUsersView.as_view(), name='get_users'),
    url(r'^services/$', GetServiceTips.as_view(), name='get_services'),
    url(r'^currencies/$', GetCurrencies.as_view(), name='get_currencies'),

    url(r'generate_phone_code/$', PhoneCodeGenerating.as_view(), name='generate_phone_code'),
    url(r'registration/$', Registration.as_view(), name='registration'),
    url(r'registration/check/$', CheckRegistration.as_view(), name='registration_check'),
    url(r'registration/(?P<token>\w+)/$', RegistrationUpdate.as_view(), name='registration_update'),
    url(r'upload_image_vk/$', UploadImageToVK.as_view(), name='upload_image_vk'),
    url(r'upload_image_fb/$', UploadImageToFB.as_view(), name='upload_image_fb'),
)

urlpatterns += patterns('',
    url(r'^cabinet/avatar/', AvatarUpdateView.as_view(), name='cabinet_avatar'),
    url(r'^cabinet/main_info/', MainInfoUpdateView.as_view(), name='cabinet_main_info'),
    url(r'^cabinet/address/', AddressUpdateView.as_view(), name='cabinet_address'),
    url(r'^cabinet/professional_info/', ProfessionalInfoUpdateView.as_view(), name='cabinet_professional_info'),
    url(r'^cabinet/diplom/add/', DiplomCreateView.as_view(), name='cabinet_diplom_add'),
    url(r'^cabinet/diplom/delete/(?P<pk>\d+)/', DiplomDeleteView.as_view(), name='cabinet_diplom_delete'),
    url(r'^cabinet/license/add/', LicenseCreateView.as_view(), name='cabinet_license_add'),
    url(r'^cabinet/license/delete/(?P<pk>\d+)/', LicenseDeleteView.as_view(), name='cabinet_license_delete'),
    url(r'^cabinet/association/add/', AssociationCreateView.as_view(), name='cabinet_association_add'),
    url(r'^cabinet/association/delete/(?P<pk>\d+)/', AssociationDeleteView.as_view(), name='cabinet_association_delete'),
    url(r'^cabinet/service/add/', ServiceCreateView.as_view(), name='cabinet_service_add'),
    url(r'^cabinet/service/delete/(?P<pk>\d+)/', ServiceDeleteView.as_view(), name='cabinet_service_delete'),

    url(r'^cabinet/lawbranch/add/', LawBranchCreateView.as_view(), name='cabinet_lawbranch_add'),
    url(r'^cabinet/lawbranch/delete/(?P<pk>\d+)/', LawBranchDeleteView.as_view(), name='cabinet_lawbranch_delete'),

    url(r'^cabinet/lawinstitute/add/', LawInstituteCreateView.as_view(), name='cabinet_lawinstitute_add'),
    url(r'^cabinet/lawinstitute/delete/(?P<pk>\d+)/', LawInstituteDeleteView.as_view(), name='cabinet_lawinstitute_delete'),


    url(r'^cabinet/notifications/', NotificationsUpdateView.as_view(), name='cabinet_notifications'),
    url(r'^cabinet/resume/delete/', ResumeDeleteView.as_view(), name='cabinet_resume_delete'),
)

urlpatterns += patterns('',
    url(r'^cabinet/phone/', PhoneUpdateView.as_view(), name='cabinet_phone'),
    url(r'^cabinet/vk/', ConnectVKAuth.as_view(), name='cabinet_auth_connect_vk'),
    url(r'^cabinet/fb/', ConnectFBAuth.as_view(), name='cabinet_auth_connect_fb'),
)
