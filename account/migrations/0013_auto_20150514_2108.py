# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0012_auto_20150514_1438'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contractorprofile',
            name='user',
        ),
        migrations.RemoveField(
            model_name='contractorregtoken',
            name='email',
        ),
        migrations.AddField(
            model_name='user',
            name='country',
            field=models.CharField(max_length=255, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='linkedin',
            field=models.CharField(max_length=100, verbose_name='\u0410\u043a\u043a\u0430\u0443\u043d\u0442 LinkedIn', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='resume',
            field=models.FileField(upload_to=b'account', null=True, verbose_name='\u0420\u0435\u0437\u044e\u043c\u0435', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='services',
            field=models.TextField(verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='site',
            field=models.CharField(max_length=100, verbose_name='\u0421\u0430\u0439\u0442', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='specialization',
            field=models.TextField(verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='status',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u042e\u0440\u0438\u0441\u0442'), (2, '\u0410\u0434\u0432\u043e\u043a\u0430\u0442'), (3, '\u041d\u043e\u0442\u0430\u0440\u0438\u0443\u0441'), (4, '\u0411\u0443\u0445\u0433\u0430\u043b\u0442\u0435\u0440')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='type',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='\u0422\u0438\u043f \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u0430', choices=[(1, 'Client'), (2, '\u0418\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c')]),
        ),
        migrations.DeleteModel(
            name='ContractorProfile',
        ),
    ]
