# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0011_auto_20150514_1433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contractorprofile',
            name='status',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u042e\u0440\u0438\u0441\u0442'), (2, '\u0410\u0434\u0432\u043e\u043a\u0430\u0442'), (3, '\u041d\u043e\u0442\u0430\u0440\u0438\u0443\u0441'), (4, '\u0411\u0443\u0445\u0433\u0430\u043b\u0442\u0435\u0440')]),
        ),
    ]
