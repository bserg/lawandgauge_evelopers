# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0025_auto_20150522_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicetip',
            name='parent',
            field=models.ForeignKey(related_name='service_tips', verbose_name='\u0418\u043d\u0441\u0442\u0438\u0442\u0443\u0442 \u043f\u0440\u0430\u0432\u0430', blank=True, to='account.LawInstitute', null=True),
        ),
    ]
