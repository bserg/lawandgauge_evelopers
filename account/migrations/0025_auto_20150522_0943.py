# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0023_auto_20150521_0901'),
    ]

    operations = [
        migrations.CreateModel(
            name='Localization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('code', models.CharField(help_text='For example "ru", "en", "de", etc.', max_length=2, verbose_name='\u041a\u043e\u0434')),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
            ],
            options={
                'verbose_name': 'Localization',
                'verbose_name_plural': 'Localizations',
            },
        ),
    ]
