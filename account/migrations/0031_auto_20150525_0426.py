# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import account.models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0030_contractoraddress_contractorassociation_contractordiplom_contractorlicense_notification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contractordiplom',
            name='file',
            field=models.FileField(upload_to=account.models.get_file_path, verbose_name='Diplom'),
        ),
        migrations.AlterField(
            model_name='contractorlicense',
            name='file',
            field=models.FileField(upload_to=account.models.get_file_path, verbose_name='License'),
        ),
    ]
