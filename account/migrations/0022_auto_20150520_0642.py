# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0001_initial'),
        ('account', '0021_auto_20150520_0641'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='city',
            field=models.ForeignKey(verbose_name='City', blank=True, to='map.City', null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='country',
            field=models.ForeignKey(verbose_name='Country', blank=True, to='map.Country', null=True),
        ),
    ]
