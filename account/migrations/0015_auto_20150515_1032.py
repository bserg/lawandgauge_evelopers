# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0014_user_passed'),
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
            ],
            options={
                'verbose_name': 'Administrator',
                'proxy': True,
                'verbose_name_plural': 'Administrators',
            },
            bases=('account.user',),
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
            ],
            options={
                'verbose_name': 'Client',
                'proxy': True,
                'verbose_name_plural': 'Clients',
            },
            bases=('account.user',),
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Contractor',
            fields=[
            ],
            options={
                'verbose_name': '\u0418\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c',
                'proxy': True,
                'verbose_name_plural': 'Contractors',
            },
            bases=('account.user',),
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterModelOptions(
            name='phonecode',
            options={'verbose_name': 'Authorization code', 'verbose_name_plural': 'Authorization codes'},
        ),
        migrations.AlterField(
            model_name='user',
            name='passed',
            field=models.BooleanField(default=False, help_text='Check if user passed the interview', verbose_name='Passed'),
        ),
        migrations.AlterField(
            model_name='user',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='\u0422\u0438\u043f \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u0430', choices=[(1, 'Client'), (2, '\u0418\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c')]),
        ),
    ]
