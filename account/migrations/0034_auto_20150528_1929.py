# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import account.models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0033_contractoraddress_house'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.ImageField(upload_to=account.models.avatar_upload_to, null=True, verbose_name='Avatar', blank=True),
        ),
    ]
