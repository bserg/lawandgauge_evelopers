# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='type',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='Account type', choices=[(1, 'Customer'), (2, 'Contractor')]),
        ),
    ]
