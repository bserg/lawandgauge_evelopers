# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0034_auto_20150528_1929'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='language',
            field=models.CharField(default=b'en', max_length=2, verbose_name='Language', choices=[(b'ru', 'Russian'), (b'en', 'English')]),
        ),
    ]
