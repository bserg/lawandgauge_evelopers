# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0015_auto_20150515_1032'),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
                ('parent', models.ForeignKey(related_name='children', verbose_name='Parent', blank=True, to='account.Service', null=True)),
            ],
            options={
                'verbose_name': 'Service',
                'verbose_name_plural': '\u0423\u0441\u043b\u0443\u0433\u0438',
            },
        ),
        migrations.CreateModel(
            name='UserService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price_min', models.FloatField(default=0, verbose_name='Minimal price')),
                ('price_max', models.FloatField(default=0, verbose_name='Maximum price')),
                ('service', models.ForeignKey(verbose_name='Service', to='account.Service')),
            ],
        ),
        migrations.AddField(
            model_name='user',
            name='city',
            field=models.CharField(max_length=255, verbose_name='City', blank=True),
        ),
        migrations.RemoveField(
            model_name='user',
            name='services',
        ),
        migrations.AddField(
            model_name='userservice',
            name='user',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='user',
            name='services',
            field=models.ManyToManyField(to='account.Service', verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438', through='account.UserService', blank=True),
        ),
    ]
