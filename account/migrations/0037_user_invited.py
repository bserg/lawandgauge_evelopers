# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0036_invitedcontractor'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='invited',
            field=models.BooleanField(default=False, help_text='Checked if contractor has been invited manually', verbose_name='Invited'),
        ),
    ]
