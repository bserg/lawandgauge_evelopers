# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0001_initial'),
        ('account', '0026_auto_20150522_2101'),
    ]

    operations = [
        migrations.AddField(
            model_name='lawbranch',
            name='default_locale',
            field=models.ForeignKey(verbose_name='Default locale', blank=True, to='localization.Locale', null=True),
        ),
        migrations.AddField(
            model_name='lawinstitute',
            name='default_locale',
            field=models.ForeignKey(verbose_name='Default locale', blank=True, to='localization.Locale', null=True),
        ),
        migrations.AddField(
            model_name='servicetip',
            name='default_locale',
            field=models.ForeignKey(verbose_name='Default locale', blank=True, to='localization.Locale', null=True),
        ),
    ]
