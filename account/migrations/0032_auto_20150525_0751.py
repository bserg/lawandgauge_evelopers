# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0031_auto_20150525_0426'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='admin',
            options={'verbose_name': 'Administrator', 'verbose_name_plural': 'Administrators'},
        ),
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': 'Client', 'verbose_name_plural': 'Clients'},
        ),
        migrations.AlterModelOptions(
            name='contractor',
            options={'verbose_name': 'Contractor', 'verbose_name_plural': 'Contractors'},
        ),
        migrations.AlterModelOptions(
            name='currency',
            options={'verbose_name': 'Currency', 'verbose_name_plural': 'Currencies'},
        ),
        migrations.AlterModelOptions(
            name='lawbranch',
            options={'verbose_name': 'Branch of Law', 'verbose_name_plural': 'Branches of Law'},
        ),
        migrations.AlterModelOptions(
            name='lawinstitute',
            options={'verbose_name': 'Law Institute', 'verbose_name_plural': 'Law Institutes'},
        ),
        migrations.AlterModelOptions(
            name='phonecode',
            options={'verbose_name': 'Authorization code', 'verbose_name_plural': 'Authorization codes'},
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'User', 'verbose_name_plural': 'Users'},
        ),
        migrations.AlterField(
            model_name='contractoraddress',
            name='city',
            field=models.ForeignKey(verbose_name='City', blank=True, to='map.City', null=True),
        ),
        migrations.AlterField(
            model_name='contractoraddress',
            name='country',
            field=models.ForeignKey(verbose_name='Country', blank=True, to='map.Country', null=True),
        ),
        migrations.AlterField(
            model_name='contractoraddress',
            name='user',
            field=models.OneToOneField(related_name='address', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='contractorassociation',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name', blank=True),
        ),
        migrations.AlterField(
            model_name='contractordiplom',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name', blank=True),
        ),
        migrations.AlterField(
            model_name='contractorlicense',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name', blank=True),
        ),
        migrations.AlterField(
            model_name='contractorregtoken',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='contractorregtoken',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='contractorregtoken',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, verbose_name='Token', editable=False),
        ),
        migrations.AlterField(
            model_name='contractorregtoken',
            name='user',
            field=models.ForeignKey(verbose_name='Contractor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='currency',
            name='code',
            field=models.CharField(max_length=3, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='currency',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='lawbranch',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='lawbranch',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='lawbranch',
            name='order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Order'),
        ),
        migrations.AlterField(
            model_name='lawinstitute',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='lawinstitute',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='lawinstitute',
            name='order',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Order'),
        ),
        migrations.AlterField(
            model_name='lawinstitute',
            name='parent',
            field=models.ForeignKey(related_name='institutes', verbose_name='Branch of law', to='account.LawBranch'),
        ),
        migrations.AlterField(
            model_name='localization',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='localization',
            name='code',
            field=models.CharField(help_text='For example "ru", "en", "de", etc.', max_length=2, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='localization',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='phonecode',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='phonecode',
            name='code',
            field=models.CharField(max_length=50, verbose_name='Code', blank=True),
        ),
        migrations.AlterField(
            model_name='phonecode',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='phonecode',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='service',
            name='currency',
            field=models.ForeignKey(verbose_name='Currency', blank=True, to='account.Currency', null=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='service',
            name='parent',
            field=models.ForeignKey(verbose_name='Law Institute', blank=True, to='account.LawInstitute', null=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='price_max',
            field=models.FloatField(null=True, verbose_name='Maximum price', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='price_min',
            field=models.FloatField(null=True, verbose_name='Minimal price', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='user',
            field=models.ForeignKey(related_name='services', verbose_name='User', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='servicetip',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='servicetip',
            name='parent',
            field=models.ForeignKey(related_name='service_tips', verbose_name='Law Institute', blank=True, to='account.LawInstitute', null=True),
        ),
        migrations.AlterField(
            model_name='sharedmap',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='sharedmap',
            name='img',
            field=models.ImageField(upload_to=b'shared_map', null=True, verbose_name='Image', blank=True),
        ),
        migrations.AlterField(
            model_name='sharedmap',
            name='user',
            field=models.ForeignKey(related_name='shared_maps', verbose_name='User', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.ImageField(upload_to=b'avatars', null=True, verbose_name='Avatar', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='city',
            field=models.ForeignKey(related_name='users', verbose_name='City', blank=True, to='map.City', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='country',
            field=models.ForeignKey(verbose_name='Country', blank=True, to='map.Country', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='fb_id',
            field=models.CharField(max_length=100, verbose_name='FB ID', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='linkedin',
            field=models.CharField(max_length=100, verbose_name='LinkedIn', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='passed',
            field=models.BooleanField(default=False, help_text='Check if user passed the interview', verbose_name='Passed'),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='Phone', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='resume',
            field=models.FileField(upload_to=b'account', null=True, verbose_name='Resume', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='site',
            field=models.CharField(max_length=100, verbose_name='Site', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='specialization',
            field=models.TextField(verbose_name='Specialization', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='status',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Status', choices=[(1, 'Lawyer'), (2, 'Advocate'), (3, 'Notary'), (4, 'Accountant')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Account type', choices=[(1, 'Client'), (2, 'Contractor')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='tz',
            field=models.CharField(max_length=100, verbose_name='Timezone', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='vk_id',
            field=models.CharField(max_length=100, verbose_name='VK ID', blank=True),
        ),
    ]
