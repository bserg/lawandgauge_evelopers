# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0018_user_services'),
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=3, verbose_name='Code')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('fa_class', models.CharField(max_length=100, verbose_name='Font Awesome class', blank=True)),
            ],
            options={
                'verbose_name': 'Currency',
                'verbose_name_plural': 'Currencies',
            },
        ),
        migrations.AlterModelOptions(
            name='userservice',
            options={'verbose_name': 'User service', 'verbose_name_plural': 'User services'},
        ),
        migrations.AddField(
            model_name='userservice',
            name='currency',
            field=models.ForeignKey(verbose_name='Currency', blank=True, to='account.Currency', null=True),
        ),
    ]
