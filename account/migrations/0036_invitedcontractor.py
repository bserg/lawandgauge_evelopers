# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0035_user_language'),
    ]

    operations = [
        migrations.CreateModel(
            name='InvitedContractor',
            fields=[
            ],
            options={
                'verbose_name': 'Invited contractor',
                'proxy': True,
                'verbose_name_plural': 'Invited contractors',
            },
            bases=('account.contractor',),
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
