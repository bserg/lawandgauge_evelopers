# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_auto_20150508_2117'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContractorProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.CharField(max_length=255, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430')),
                ('status', models.PositiveSmallIntegerField(blank=True, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u042e\u0440\u0438\u0441\u0442'), (2, '\u0410\u0434\u0432\u043e\u043a\u0430\u0442'), (3, '\u041d\u043e\u0442\u0430\u0440\u0438\u0443\u0441'), (4, '\u0411\u0443\u0445\u0433\u0430\u043b\u0442\u0435\u0440')])),
                ('site', models.CharField(max_length=100, verbose_name='\u0421\u0430\u0439\u0442', blank=True)),
                ('linkedin', models.CharField(max_length=100, verbose_name='\u0410\u043a\u043a\u0430\u0443\u043d\u0442 LinkedIn', blank=True)),
                ('resume', models.FileField(upload_to=b'account', null=True, verbose_name='\u0420\u0435\u0437\u044e\u043c\u0435', blank=True)),
                ('specialization', models.TextField(verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f', blank=True)),
                ('services', models.TextField(verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438', blank=True)),
            ],
            options={
                'verbose_name': 'Contractor profile',
                'verbose_name_plural': 'Contractor profiles',
            },
        ),
        migrations.CreateModel(
            name='LawyerRegToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.UUIDField(default=uuid.uuid4, verbose_name='\u0422\u043e\u043a\u0435\u043d', editable=False)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
            ],
            options={
                'verbose_name': '\u0420\u0435\u0437\u044e\u043c\u0435 \u0442\u043e\u043a\u0435\u043d',
                'verbose_name_plural': '\u0420\u0435\u0437\u044e\u043c\u0435 \u0442\u043e\u043a\u0435\u043d\u044b',
            },
        ),
        migrations.CreateModel(
            name='PhoneCode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.CharField(max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('code', models.CharField(max_length=50, verbose_name='Code', blank=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
            ],
            options={
                'verbose_name': 'Phone code',
                'verbose_name_plural': 'Phone codes',
            },
        ),
        migrations.AddField(
            model_name='user',
            name='social',
            field=models.CharField(max_length=100, verbose_name='Social ID', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
        ),
        migrations.AddField(
            model_name='lawyerregtoken',
            name='user',
            field=models.ForeignKey(verbose_name='\u042e\u0440\u0438\u0441\u0442', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='contractorprofile',
            name='user',
            field=models.OneToOneField(related_name='contractor_profile', verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL),
        ),
    ]
