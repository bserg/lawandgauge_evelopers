# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('map', '0002_auto_20150522_2101'),
        ('account', '0029_sharedmap'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContractorAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.CharField(max_length=255, verbose_name='Street', blank=True)),
                ('apartment', models.CharField(max_length=255, verbose_name='Apartment', blank=True)),
                ('zip', models.CharField(max_length=10, verbose_name='ZIP code', blank=True)),
                ('city', models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True, to='map.City', null=True)),
                ('country', models.ForeignKey(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True, to='map.Country', null=True)),
                ('user', models.ForeignKey(related_name='addresses', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Address',
                'verbose_name_plural': 'Addresses',
            },
        ),
        migrations.CreateModel(
            name='ContractorAssociation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f', blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(related_name='associations', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Association',
                'verbose_name_plural': 'Associations',
            },
        ),
        migrations.CreateModel(
            name='ContractorDiplom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f', blank=True)),
                ('file', models.FileField(upload_to=b'contractor_diplomas', verbose_name='Diplom')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(related_name='diplomas', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Diplom',
                'verbose_name_plural': 'Diplomas',
            },
        ),
        migrations.CreateModel(
            name='ContractorLicense',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f', blank=True)),
                ('file', models.FileField(upload_to=b'contractor_licenses', verbose_name='License')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(related_name='licenses', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'License',
                'verbose_name_plural': 'Licenses',
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('new_order_to_phone', models.BooleanField(default=False, verbose_name='New order notification to phone')),
                ('new_order_to_email', models.BooleanField(default=False, verbose_name='New order notification to email')),
                ('new_message_to_phone', models.BooleanField(default=False, verbose_name='New message notification to phone')),
                ('new_message_to_email', models.BooleanField(default=False, verbose_name='New message notification to email')),
                ('other_notifications_to_phone', models.BooleanField(default=False, verbose_name='Other notifications to phone')),
                ('other_notifications_to_email', models.BooleanField(default=False, verbose_name='Other notifications to email')),
                ('user', models.OneToOneField(related_name='notifications', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Notification',
                'verbose_name_plural': 'Notifications',
            },
        ),
    ]
