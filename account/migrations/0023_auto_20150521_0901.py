# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0022_auto_20150520_0642'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceTip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
            ],
            options={
                'verbose_name': 'Service tip',
                'verbose_name_plural': 'Services tips',
            },
        ),
        migrations.RemoveField(
            model_name='userservice',
            name='currency',
        ),
        migrations.RemoveField(
            model_name='userservice',
            name='service',
        ),
        migrations.RemoveField(
            model_name='userservice',
            name='user',
        ),
        migrations.AlterModelOptions(
            name='admin',
            options={'verbose_name': '\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440', 'verbose_name_plural': '\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440\u044b'},
        ),
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': '\u041a\u043b\u0438\u0435\u043d\u0442', 'verbose_name_plural': '\u041a\u043b\u0438\u0435\u043d\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='currency',
            options={'verbose_name': '\u0412\u0430\u043b\u044e\u0442\u0430', 'verbose_name_plural': '\u0412\u0430\u043b\u044e\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='lawbranch',
            options={'verbose_name': '\u041e\u0442\u0440\u0430\u0441\u043b\u044c \u043f\u0440\u0430\u0432\u0430', 'verbose_name_plural': '\u041e\u0442\u0440\u0430\u0441\u043b\u0438 \u043f\u0440\u0430\u0432\u0430'},
        ),
        migrations.AlterModelOptions(
            name='lawinstitute',
            options={'verbose_name': '\u0418\u043d\u0441\u0442\u0438\u0442\u0443\u0442 \u043f\u0440\u0430\u0432\u0430', 'verbose_name_plural': '\u0418\u043d\u0441\u0442\u0438\u0442\u0443\u0442\u044b \u043f\u0440\u0430\u0432\u0430'},
        ),
        migrations.AlterModelOptions(
            name='phonecode',
            options={'verbose_name': '\u041a\u043e\u0434 \u0430\u0432\u0442\u043e\u0440\u0438\u0437\u0430\u0446\u0438\u0438', 'verbose_name_plural': 'Authorization codes'},
        ),
        migrations.AlterModelOptions(
            name='service',
            options={'verbose_name': 'User service', 'verbose_name_plural': 'User services'},
        ),
        migrations.RemoveField(
            model_name='service',
            name='active',
        ),
        migrations.RemoveField(
            model_name='service',
            name='description',
        ),
        migrations.RemoveField(
            model_name='user',
            name='services',
        ),
        migrations.AddField(
            model_name='service',
            name='currency',
            field=models.ForeignKey(verbose_name='\u0412\u0430\u043b\u044e\u0442\u0430', blank=True, to='account.Currency', null=True),
        ),
        migrations.AddField(
            model_name='service',
            name='price_max',
            field=models.FloatField(null=True, verbose_name='\u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u0446\u0435\u043d\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='service',
            name='price_min',
            field=models.FloatField(null=True, verbose_name='\u041c\u0438\u043d\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u0446\u0435\u043d\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='service',
            name='user',
            field=models.ForeignKey(related_name='services', verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='currency',
            name='code',
            field=models.CharField(max_length=3, verbose_name='\u041a\u043e\u0434'),
        ),
        migrations.AlterField(
            model_name='lawinstitute',
            name='parent',
            field=models.ForeignKey(related_name='institutes', verbose_name='\u041e\u0442\u0440\u0430\u0441\u043b\u044c \u043f\u0440\u0430\u0432\u0430', to='account.LawBranch'),
        ),
        migrations.AlterField(
            model_name='phonecode',
            name='code',
            field=models.CharField(max_length=50, verbose_name='\u041a\u043e\u0434', blank=True),
        ),
        migrations.AlterField(
            model_name='service',
            name='parent',
            field=models.ForeignKey(verbose_name='\u0418\u043d\u0441\u0442\u0438\u0442\u0443\u0442 \u043f\u0440\u0430\u0432\u0430', blank=True, to='account.LawInstitute', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='city',
            field=models.ForeignKey(related_name='users', verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True, to='map.City', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='country',
            field=models.ForeignKey(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True, to='map.Country', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='fb_id',
            field=models.CharField(max_length=100, verbose_name='ID Facebook', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='passed',
            field=models.BooleanField(default=False, help_text='\u041e\u0442\u043c\u0435\u0442\u044c\u0442\u0435, \u0435\u0441\u043b\u0438 \u043a\u0430\u043d\u0434\u0438\u0434\u0430\u0442 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u043f\u0440\u043e\u0448\u0435\u043b \u0438\u043d\u0442\u0435\u0440\u0432\u044c\u044e', verbose_name='Passed'),
        ),
        migrations.AlterField(
            model_name='user',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='\u0422\u0438\u043f \u0430\u043a\u043a\u0430\u0443\u043d\u0442\u0430', choices=[(1, '\u041a\u043b\u0438\u0435\u043d\u0442'), (2, '\u0418\u0441\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='vk_id',
            field=models.CharField(max_length=100, verbose_name='ID \u0412 \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0435', blank=True),
        ),
        migrations.DeleteModel(
            name='UserService',
        ),
        migrations.AddField(
            model_name='servicetip',
            name='parent',
            field=models.ForeignKey(related_name='services', verbose_name='\u0418\u043d\u0441\u0442\u0438\u0442\u0443\u0442 \u043f\u0440\u0430\u0432\u0430', blank=True, to='account.LawInstitute', null=True),
        ),
    ]
