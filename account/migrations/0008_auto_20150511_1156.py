# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0007_auto_20150511_1105'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contractorregtoken',
            options={'verbose_name': 'Contractor registration token', 'verbose_name_plural': 'Contractor registration tokens'},
        ),
        migrations.AddField(
            model_name='contractorregtoken',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430', blank=True),
        ),
    ]
