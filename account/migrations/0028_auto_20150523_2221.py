# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0027_auto_20150523_2151'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lawbranch',
            name='default_locale',
        ),
        migrations.RemoveField(
            model_name='lawinstitute',
            name='default_locale',
        ),
        migrations.RemoveField(
            model_name='servicetip',
            name='default_locale',
        ),
    ]
