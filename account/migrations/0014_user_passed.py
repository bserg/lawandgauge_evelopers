# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0013_auto_20150514_2108'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='passed',
            field=models.BooleanField(default=False, verbose_name='Passed'),
        ),
    ]
