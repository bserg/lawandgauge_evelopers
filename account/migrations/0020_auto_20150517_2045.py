# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0019_auto_20150516_2152'),
    ]

    operations = [
        migrations.CreateModel(
            name='LawBranch',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
                ('order', models.PositiveSmallIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
            ],
            options={
                'verbose_name': 'Branch of Law',
                'verbose_name_plural': 'Branches of Law',
            },
        ),
        migrations.CreateModel(
            name='LawInstitute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
                ('order', models.PositiveSmallIntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('parent', models.ForeignKey(related_name='institutes', verbose_name='Branch of law', to='account.LawBranch')),
            ],
            options={
                'verbose_name': 'Law Institute',
                'verbose_name_plural': 'Law Institutes',
            },
        ),
        migrations.AlterField(
            model_name='service',
            name='parent',
            field=models.ForeignKey(related_name='services', verbose_name='Law Institute', blank=True, to='account.LawInstitute', null=True),
        ),
    ]
