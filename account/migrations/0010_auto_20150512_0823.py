# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0009_auto_20150511_1201'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='social',
        ),
        migrations.AddField(
            model_name='user',
            name='fb_id',
            field=models.CharField(max_length=100, verbose_name='FB ID', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='vk_id',
            field=models.CharField(max_length=100, verbose_name='VK ID', blank=True),
        ),
    ]
