# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20150510_2220'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContractorRegToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.UUIDField(default=uuid.uuid4, verbose_name='\u0422\u043e\u043a\u0435\u043d', editable=False)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e')),
                ('user', models.ForeignKey(verbose_name='\u042e\u0440\u0438\u0441\u0442', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u0420\u0435\u0437\u044e\u043c\u0435 \u0442\u043e\u043a\u0435\u043d',
                'verbose_name_plural': '\u0420\u0435\u0437\u044e\u043c\u0435 \u0442\u043e\u043a\u0435\u043d\u044b',
            },
        ),
        migrations.RemoveField(
            model_name='lawyerregtoken',
            name='user',
        ),
        migrations.DeleteModel(
            name='LawyerRegToken',
        ),
    ]
