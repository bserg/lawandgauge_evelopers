# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0038_auto_20150717_0519'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='language',
            field=models.CharField(default=b'en', choices=[(b'ru', 'Russian'), (b'en', 'English')], max_length=2, blank=True, null=True, verbose_name='Language'),
        ),
    ]
