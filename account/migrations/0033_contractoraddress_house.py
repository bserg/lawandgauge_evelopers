# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0032_auto_20150525_0751'),
    ]

    operations = [
        migrations.AddField(
            model_name='contractoraddress',
            name='house',
            field=models.CharField(max_length=255, verbose_name='House', blank=True),
        ),
    ]
