# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0016_auto_20150515_2153'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='services',
        ),
    ]
