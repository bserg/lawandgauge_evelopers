# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0017_remove_user_services'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='services',
            field=models.ManyToManyField(to='account.Service', verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438', through='account.UserService', blank=True),
        ),
    ]
