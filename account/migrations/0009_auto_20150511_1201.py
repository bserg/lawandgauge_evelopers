# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0008_auto_20150511_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contractorregtoken',
            name='user',
            field=models.ForeignKey(verbose_name='\u042e\u0440\u0438\u0441\u0442', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
