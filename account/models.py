import uuid
from django.utils import translation
import os

from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _, get_language
from django.contrib.auth.models import AbstractUser
from django.db import models
from sorl.thumbnail.shortcuts import get_thumbnail

from lawandgauge.settings import SITE_URL, STATIC_URL, LANGUAGES
from localization.models import TranslatedModelMixin
from mail.models import MailTemplate, TEMPLATE_INVITION_CONTRACTOR, Mail
from mail.utils import _mail
from map.models import Country, City

TYPE_CLIENT = 1
TYPE_CONTRACTOR = 2
TYPES = (
    (TYPE_CLIENT, _('Client')),
    (TYPE_CONTRACTOR, _('Contractor')),
)

CONTRACTOR_LAWYER = 1
CONTRACTOR_NOTARY = 2
CONTRACTOR_ACCOUNTANT = 3

CONTRACTOR_STATUS = (
    (CONTRACTOR_LAWYER, _('Lawyer')),
    (CONTRACTOR_NOTARY, _('Notary')),
    (CONTRACTOR_ACCOUNTANT, _('Accountant')),
)


def avatar_upload_to(instance, filename):
    try:
        ext = filename.rsplit('.', 1)[1]
    except:
        ext = ''
    if ext:
        return 'avatars/%s.%s' % (str(uuid.uuid4()).replace('-', ''), ext)
    else:
        return 'avatars/%s' % str(uuid.uuid4()).replace('-', '')


class User(AbstractUser):
    phone = models.CharField(_('Phone'), max_length=100, blank=True)
    vk_id = models.CharField(_('VK ID'), max_length=100, blank=True)
    fb_id = models.CharField(_('FB ID'), max_length=100, blank=True)

    type = models.PositiveSmallIntegerField(_('Account type'), choices=TYPES, blank=True, null=True)
    avatar = models.ImageField(_('Avatar'), upload_to=avatar_upload_to, blank=True, null=True)
    tz = models.CharField(_('Timezone'), max_length=100, blank=True)
    language = models.CharField(_('Language'), max_length=2, choices=LANGUAGES, blank=True, null=True)

    country = models.ForeignKey(Country, verbose_name=_('Country'), blank=True, null=True)
    city = models.ForeignKey(City, verbose_name=_('City'), related_name='users', blank=True, null=True)

    status = models.PositiveSmallIntegerField(_('Status'), choices=CONTRACTOR_STATUS, blank=True, null=True)
    site = models.CharField(_('Site'), max_length=100, blank=True)
    linkedin = models.CharField(_('LinkedIn'), max_length=100, blank=True)
    resume = models.FileField(_('Resume'), upload_to='account', blank=True, null=True)
    specialization = models.TextField(_('Specialization'), blank=True)

    passed = models.BooleanField(_('Passed'), default=False, help_text=_('Check if user passed the interview'))
    invited = models.BooleanField(_('Invited'), default=False, help_text=_('Checked if contractor has been invited manually'))

    def set_uuid_username(self, save=False):
        self.username = str(uuid.uuid4()).replace('-', '')[:30]
        if save:
            self.save()

    def get_name(self):
        return self.get_full_name() or self.phone or self.username

    def get_avatar(self, width=177, height=177):
        if self.avatar:
            return get_thumbnail(self.avatar, "%dx%d" % (width, height), crop='center').url
        else:
            return STATIC_URL + 'img/default_avatar.png'

    def as_dict(self):
        data = {
            'id': self.pk,
            'name': self.get_name(),
            'first_name': self.first_name,
            'last_name': self.last_name,
            'phone': self.phone,
            'email': self.email,
            'type': self.type,
            'avatar': self.get_avatar(),
            'avatar_100x100': self.get_avatar(100, 100),
            'lang': self.language or get_language(),
            'auth': {'phone': self.phone, 'vk_id': self.vk_id, 'fb_id': self.fb_id},
            'is_active': self.is_active,
            'is_admin': self.is_superuser or self.is_admin()
        }
        if self.type == TYPE_CONTRACTOR:
            data.update({
                'status': self.status,
                'country': self.country_id,
                'country_name': self.country.name if self.country else None,
                'city': self.city_id,
                'city_name': self.city.name if self.city else None,
                'site': self.site,
                'linkedin': self.linkedin,
            })
            if hasattr(self, 'resume') and self.resume:
                data.update(resume={'name': os.path.basename(self.resume.name), 'delete_url': reverse('account:cabinet_resume_delete'), 'url': self.resume.url})
            if hasattr(self, 'address') and self.address:
                data.update(address={
                    'country': self.address.country_id,
                    'city': self.address.city_id,
                    'street': self.address.street,
                    'house': self.address.house,
                    'apartment': self.address.apartment,
                    'zip': self.address.zip,
                })
            data.update(
                diplomas=[d.as_dict() for d in self.diplomas.all()],
                licenses=[l.as_dict() for l in self.licenses.all()],
                associations=[a.as_dict() for a in self.associations.all()],
            )
            if hasattr(self, 'notifications') and self.notifications:
                data.update(notifications={
                    'new_order_to_phone': self.notifications.new_order_to_phone,
                    'new_order_to_email': self.notifications.new_order_to_email,
                    'new_message_to_phone': self.notifications.new_message_to_phone,
                    'new_message_to_email': self.notifications.new_message_to_email,
                    'other_notifications_to_phone': self.notifications.other_notifications_to_phone,
                    'other_notifications_to_email': self.notifications.other_notifications_to_email,
                })
            data.update(services=[s.as_dict() for s in self.services.all()])
        return data

    def is_admin(self):
        return self.has_perms(['add_chat', 'change_chat', 'delete_chat'])

    def mail(self, language=None):
        _mail(TEMPLATE_INVITION_CONTRACTOR, recipient=self, language=language,
              extra_context={'link': SITE_URL + reverse('account:registration_update', kwargs={'token': self.username})})

    def save(self, *args, **kwargs):
        if self.pk is not None:
            if self.type == TYPE_CONTRACTOR:
                orig = User.objects.get(pk=self.pk)
                if self.passed and not orig.passed:
                    self.mail()
        return super(User, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.get_name()

    class Meta:
        verbose_name = _('User').encode('utf-8')
        verbose_name_plural = _('Users').encode('utf-8')


class Client(User):

    def __init__(self, *args, **kwargs):
        self._meta.get_field('type').default = TYPE_CLIENT
        self._meta.get_field('type').editable = False
        super(Client, self).__init__(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _('Client').encode('utf-8')
        verbose_name_plural = _('Clients').encode('utf-8')


class Contractor(User):

    def __init__(self, *args, **kwargs):
        self._meta.get_field('type').default = TYPE_CONTRACTOR
        self._meta.get_field('type').editable = False
        super(Contractor, self).__init__(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _('Contractor').encode('utf-8')
        verbose_name_plural = _('Contractors').encode('utf-8')


class InvitedContractor(Contractor):

    def save(self, **kwargs):
        if self.pk is None:
            self.set_uuid_username()
            self.set_unusable_password()
            self.is_active = False
            self.passed = True
            self.invited = True
            super(InvitedContractor, self).save(**kwargs)
            self.mail()

        super(InvitedContractor, self).save(**kwargs)

    class Meta:
        proxy = True
        verbose_name = _('Invited contractor').encode('utf-8')
        verbose_name_plural = _('Invited contractors').encode('utf-8')


class Admin(User):
    class Meta:
        proxy = True
        verbose_name = _('Administrator').encode('utf-8')
        verbose_name_plural = _('Administrators').encode('utf-8')


class PhoneCode(models.Model):
    phone = models.CharField(_('Phone'), max_length=100)
    code = models.CharField(_('Code'), max_length=50, blank=True)
    date = models.DateTimeField(_('Date'), auto_now_add=True)
    active = models.BooleanField(_('Active'), default=True)

    def __unicode__(self):
        return self.phone

    class Meta:
        verbose_name = _('Authorization code').encode('utf-8')
        verbose_name_plural = _('Authorization codes').encode('utf-8')


class LawBranch(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    active = models.BooleanField(_('Active'), default=True)
    order = models.PositiveSmallIntegerField(_('Order'), default=0)

    @property
    def name(self):
        field = 'name_' + get_language()
        if hasattr(self, field):
            return getattr(self, field)

    def as_dict(self):
        return {'id': self.id, 'name': self.name, 'institutes': [i.as_dict() for i in self.institutes.filter(active=True) if i.name]}

    def __unicode__(self):
        return self.name or self.name_ru or self.name_en

    class Meta:
        verbose_name = _('Branch of Law').encode('utf-8')
        verbose_name_plural = _('Branches of Law').encode('utf-8')


class LawInstitute(TranslatedModelMixin, models.Model):
    parent = models.ForeignKey(LawBranch, verbose_name=_('Branch of law'), related_name='institutes')
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    active = models.BooleanField(_('Active'), default=True)
    order = models.PositiveSmallIntegerField(_('Order'), default=0)

    @property
    def name(self):
        field = 'name_' + get_language()
        if hasattr(self, field):
            return getattr(self, field)

    def as_dict(self):
        return {'id': self.id, 'name': self.name, 'service_tips': [s.as_dict() for s in self.service_tips.all() if s.name]}

    def __unicode__(self):
        return self.name or self.name_ru or self.name_en

    class Meta:
        verbose_name = _('Law Institute').encode('utf-8')
        verbose_name_plural = _('Law Institutes').encode('utf-8')


class ServiceTip(TranslatedModelMixin, models.Model):
    parent = models.ForeignKey(LawInstitute, verbose_name=_('Law Institute'), related_name='service_tips', blank=True, null=True)
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)

    @property
    def name(self):
        field = 'name_' + get_language()
        if hasattr(self, field):
            return getattr(self, field)

    def as_dict(self):
        return {'name': self.name}

    def __unicode__(self):
        return self.name or self.name_ru or self.name_en

    class Meta:
        verbose_name = _('Service tip').encode('utf-8')
        verbose_name_plural = _('Services tips').encode('utf-8')


class Service(models.Model):
    user = models.ForeignKey(User, verbose_name=_('User'), related_name='services', null=True)
    parent = models.ForeignKey(LawInstitute, verbose_name=_('Law Institute'), blank=True, null=True)
    name = models.CharField(_('Name'), max_length=255)
    price_min = models.FloatField(_('Minimal price'), null=True, blank=True)
    price_max = models.FloatField(_('Maximum price'), null=True, blank=True)
    currency = models.ForeignKey('Currency', verbose_name=_('Currency'), blank=True, null=True)

    def as_dict(self):
        data = {
            'id': self.pk,
            'parent': self.parent_id,
            'name': self.name,
            'price_min': self.price_min,
            'price_max': self.price_max,
            'currency': self.currency.as_dict() if self.currency else None,
            'delete_url': reverse('account:cabinet_service_delete', kwargs={'pk': self.pk})
        }
        if self.parent:
            data.update({
                'branch': self.parent.parent.name,
                'institute': self.parent.name,
            })
        return data

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('User service').encode('utf-8')
        verbose_name_plural = _('User services').encode('utf-8')


class Currency(models.Model):
    code = models.CharField(_('Code'), max_length=3)
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    fa_class = models.CharField(_('Font Awesome class'), max_length=100, blank=True)

    @property
    def name(self):
        field = 'name_' + get_language()
        if hasattr(self, field):
            return getattr(self, field)

    def as_dict(self):
        return {
            'id': self.pk,
            'code': self.code,
            'name': self.name,
            'fa_class': self.fa_class
        }

    def __unicode__(self):
        return _(self.name)

    class Meta:
        verbose_name = _('Currency').encode('utf-8')
        verbose_name_plural = _('Currencies').encode('utf-8')


class ContractorRegToken(models.Model):
    """DEPRECATED"""
    user = models.ForeignKey(User, verbose_name=_('Contractor'), blank=True, null=True)
    token = models.UUIDField(_('Token'), default=uuid.uuid4, editable=False)
    date = models.DateTimeField(_('Date'), auto_now_add=True)
    active = models.BooleanField(_('Active'), default=True)

    def clean(self):
        if not ((hasattr(self, 'user') and self.user) or self.email):
            raise ValidationError(_('Specify user or email'))

    def url(self):
        return SITE_URL + reverse('account:contractor_registration_extended', kwargs={'token': str(self.token)})

    def __unicode__(self):
        return str(self.token)

    class Meta:
        verbose_name = _('Contractor registration token').encode('utf-8')
        verbose_name_plural = _('Contractor registration tokens').encode('utf-8')


class SharedMap(models.Model):
    img = models.ImageField(_('Image'), upload_to='shared_map', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name=_('User'), related_name='shared_maps')
    date = models.DateTimeField(_('Date'), auto_now_add=True)

    def __unicode__(self):
        return self.img.name

    class Meta:
        verbose_name = _('Shared map')
        verbose_name_plural = _('Shared maps')


# __PRIVATE CABINET___#


def get_file_path(instance, filename):
    return 'contractor/%s/%s' % (instance.user.pk, filename)


class ContractorAddress(models.Model):
    user = models.OneToOneField(User, limit_choices_to={'pk__in': User.objects.filter(type=TYPE_CONTRACTOR).values_list('pk', flat=True)}, related_name='address')
    country = models.ForeignKey(Country, verbose_name=_('Country'), blank=True, null=True)
    city = models.ForeignKey(City, verbose_name=_('City'), blank=True, null=True)
    street = models.CharField(_('Street'), max_length=255, blank=True)
    house = models.CharField(_('House'), max_length=255, blank=True)
    apartment = models.CharField(_('Apartment'), max_length=255, blank=True)
    zip = models.CharField(_('ZIP code'), max_length=10, blank=True)

    class Meta:
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')


class ContractorDiplom(models.Model):
    user = models.ForeignKey(User, limit_choices_to={'pk__in': User.objects.filter(type=TYPE_CONTRACTOR).values_list('pk', flat=True)}, related_name='diplomas')
    name = models.CharField(_('Name'), max_length=255, blank=True)
    file = models.FileField(_('Diplom'), upload_to=get_file_path)
    date = models.DateTimeField(auto_now_add=True)

    def as_dict(self):
        data = {
            'id': self.pk,
            'name': self.name,
            'url': self.file.url if self.file else '',
            'delete_url': reverse('account:cabinet_diplom_delete', kwargs={'pk': self.pk})
        }
        return data

    class Meta:
        verbose_name = _('Diplom')
        verbose_name_plural = _('Diplomas')


class ContractorLicense(models.Model):
    user = models.ForeignKey(User, limit_choices_to={'pk__in': User.objects.filter(type=TYPE_CONTRACTOR).values_list('pk', flat=True)}, related_name='licenses')
    name = models.CharField(_('Name'), max_length=255, blank=True)
    file = models.FileField(_('License'), upload_to=get_file_path)
    date = models.DateTimeField(auto_now_add=True)

    def as_dict(self):
        data = {
            'id': self.pk,
            'name': self.name,
            'url': self.file.url if self.file else '',
            'delete_url': reverse('account:cabinet_license_delete', kwargs={'pk': self.pk})
        }
        return data

    class Meta:
        verbose_name = _('License')
        verbose_name_plural = _('Licenses')


class ContractorAssociation(models.Model):
    user = models.ForeignKey(User, limit_choices_to={'pk__in': User.objects.filter(type=TYPE_CONTRACTOR).values_list('pk', flat=True)}, related_name='associations')
    name = models.CharField(_('Name'), max_length=255, blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def as_dict(self):
        data = {
            'id': self.pk,
            'name': self.name,
            'delete_url': reverse('account:cabinet_association_delete', kwargs={'pk': self.pk})
        }
        return data

    class Meta:
        verbose_name = _('Association')
        verbose_name_plural = _('Associations')


class Notification(models.Model):
    user = models.OneToOneField(User, limit_choices_to={'pk__in': User.objects.filter(type=TYPE_CONTRACTOR).values_list('pk', flat=True)}, related_name='notifications')
    new_order_to_phone = models.BooleanField(_('New order notification to phone'), default=False)
    new_order_to_email = models.BooleanField(_('New order notification to email'), default=False)
    new_message_to_phone = models.BooleanField(_('New message notification to phone'), default=False)
    new_message_to_email = models.BooleanField(_('New message notification to email'), default=False)
    other_notifications_to_phone = models.BooleanField(_('Other notifications to phone'), default=False)
    other_notifications_to_email = models.BooleanField(_('Other notifications to email'), default=False)

    class Meta:
        verbose_name = _('Notification')
        verbose_name_plural = _('Notifications')

