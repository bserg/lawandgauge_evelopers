import re

from django.utils.translation import ugettext as _, get_language
from django import forms
from django.core.exceptions import ValidationError

from account.models import PhoneCode, User, Service, ContractorAddress, ContractorDiplom, ContractorLicense, \
    ContractorAssociation, Notification, LawBranch, LawInstitute


#TODO create PhoneField


class PhoneCodeForm(forms.ModelForm):
    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone:
            phone = re.sub(r'[\s\-+]', '', phone)
        return phone

    class Meta:
        model = PhoneCode
        fields = ['phone']


class RegistrationForm(forms.ModelForm):
    code = forms.CharField(min_length=4, max_length=4, required=False)
    access_token = forms.CharField(max_length=255, required=False)

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone:
            phone = re.sub(r'[\s\-+]', '', phone)
        if phone and User.objects.filter(phone=phone).exists():
            raise ValidationError(_('User with this phone already registered'), code=1)
        return phone

    def clean_vk_id(self):
        vk_id = self.cleaned_data['vk_id']
        if vk_id and User.objects.filter(vk_id=vk_id).exists():
            raise ValidationError(_('Account with this social id already registered'), code=2)
        return vk_id

    def clean_fb_id(self):
        fb_id = self.cleaned_data['fb_id']
        if fb_id and User.objects.filter(fb_id=fb_id).exists():
            raise ValidationError(_('Account with this social id already registered'), code=3   )
        return fb_id

    class Meta:
        model = User
        fields = ['type', 'phone', 'vk_id', 'fb_id', 'code', 'access_token', 'first_name', 'last_name', 'language']


class RegistrationUpdateForm(forms.ModelForm):

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone:
            phone = re.sub(r'[\s\-+]', '', phone)
        return phone

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'phone', 'email', 'country', 'city',
                  'status', 'site', 'linkedin', 'resume', 'specialization', 'language']


class CheckRegistrationForm(forms.Form):
    phone = forms.CharField(required=False)
    vk_id = forms.CharField(required=False)
    fb_id = forms.CharField(required=False)


class ServiceForm(forms.ModelForm):
    branch = forms.ModelChoiceField(queryset=LawBranch.objects.filter(active=True), required=False)
    branch_name = forms.CharField(required=False)
    institute_name = forms.CharField(required=False)

    def clean_branch_name(self):
        branch_name = self.cleaned_data['branch_name']
        branch = self.cleaned_data['branch']
        if not branch:
            if LawBranch.objects.filter(**{'name_' + get_language(): branch_name}).exists():
                raise ValidationError(_('Law Branch with same name exists'))
        return branch_name

    def clean_institute_name(self):
        institute_name = self.cleaned_data['institute_name']
        branch = self.cleaned_data['branch']
        if not branch:
            branch_name = self.cleaned_data['branch_name']
            if LawInstitute.objects.filter(**{'name_' + get_language(): institute_name, 'parent__name_' + get_language(): branch_name}).exists():
                raise ValidationError(_('Such Law Institute exists'))
        return institute_name

    class Meta:
        model = Service
        exclude = ['user']


class LawBranchForm(forms.Form):
    name = forms.CharField(max_length=255)

    def clean_name(self):
        name = self.cleaned_data['name']
        lang = get_language()
        if LawBranch.objects.filter(**{'name_' + lang: name}).exists():
            raise ValidationError(_('Law Branch with same name exists'))
        return name


class LawInstituteForm(forms.ModelForm):
    name = forms.CharField(max_length=255)

    def clean(self):
        data = super(LawInstituteForm, self).clean()
        parent = data['parent']
        name = data['name']

        if LawInstitute.objects.filter(**{'name_' + get_language(): name, 'parent': parent}).exists():
            raise ValidationError(_('Such Law Institute exists'))

        return data

    class Meta:
        model = LawInstitute
        fields = ['parent']


class ShareMapForm(forms.Form):
    token = forms.CharField()
    image_url = forms.URLField()
    upload_url = forms.URLField(required=False)
    avatar = forms.URLField(required=False)
    name = forms.CharField(required=False)
    city = forms.CharField(required=False)

# ___PRIVATE CABINET FORMS___ #

class AvatarUpdateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AvatarUpdateForm, self).__init__(*args, **kwargs)
        self.fields['avatar'].required = True

    class Meta:
        model = User
        fields = ['avatar']


class MainInfoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'country', 'city', 'avatar']


class AddressForm(forms.ModelForm):
    class Meta:
        model = ContractorAddress
        exclude = ['user']


class ProfessionalInfoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['site', 'linkedin', 'status', 'resume']


class DiplomForm(forms.ModelForm):
    class Meta:
        model = ContractorDiplom
        exclude = ['date', 'user']


class LicenseForm(forms.ModelForm):
    class Meta:
        model = ContractorLicense
        exclude = ['date', 'user']


class AssociationForm(forms.ModelForm):
    class Meta:
        model = ContractorAssociation
        exclude = ['date', 'user']


class NotificationForm(forms.ModelForm):
    class Meta:
        model = Notification
        exclude = ['user']


class PhoneUpdateForm(forms.ModelForm):
    code = forms.CharField(max_length=4)

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if phone:
            phone = re.sub(r'[\s\-+]', '', phone)
        return phone

    class Meta:
        model = User
        fields = ['phone', 'code']

