import random
from django.utils import translation
import re
import urllib2
from StringIO import StringIO
import os
from django.conf.global_settings import SERVER_EMAIL

from django.core.urlresolvers import reverse
from PIL import Image, ImageOps, ImageDraw, ImageFont
from django.core.files.base import ContentFile
from django.core.mail import mail_managers, send_mail
from django.http.response import HttpResponseForbidden, Http404, HttpResponse
from django.template import Context
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _, activate, get_language, LANGUAGE_SESSION_KEY
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View, FormView, UpdateView, DeleteView
from django.views.generic.edit import CreateView
import requests

from account.forms import PhoneCodeForm, RegistrationForm, RegistrationUpdateForm, ServiceForm, ShareMapForm, \
    AvatarUpdateForm, MainInfoForm, AddressForm, ProfessionalInfoForm, DiplomForm, LicenseForm, AssociationForm, \
    NotificationForm, PhoneUpdateForm, CheckRegistrationForm, LawBranchForm, LawInstituteForm
from account.models import User, PhoneCode, TYPE_CLIENT, TYPE_CONTRACTOR, LawBranch, Currency, ServiceTip, SharedMap, \
    ContractorAddress, ContractorDiplom, ContractorLicense, ContractorAssociation, Notification, Service, LawInstitute
from account.utils import check_phone_code, check_vk_auth, check_fb_auth
from chat.models import Message, MESSAGE_TYPE_TEXT
from chat.sockjs_server import MESSAGE_TYPE_NOTIFICATION
from lawandgauge.mixins import JsonResponseMixin, LoginRequiredJsonMixin, RequiredPermissionsJsonMixin
from lawandgauge.settings import UNISENDER_API_KEY, BASE_DIR, DEFAULT_FROM_EMAIL, MANAGERS
from mail.models import MailTemplate, TEMPLATE_REGISTRATION_CONTRACTOR, Mail
from mail.utils import _mail


class Login(JsonResponseMixin, View):
    def post(self, request):
        credential_fields = ['username', 'password', 'phone', 'code', 'social', 'user_id', 'access_token'] #TODO: social auth
        credentials = {field: request.POST.get(field) for field in credential_fields}
        user = authenticate(request=request, **credentials)
        if user is not None:
            if user.is_active:
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                if user.language in ['ru', 'en']:
                    request.session[LANGUAGE_SESSION_KEY] = user.language
                return self.json_success(msg=_('Authentication successfully'))
            else:
                return self.json_fail(error=_('Authentication fault. Account is not active.'))
        else:
            return self.json_fail(error=_('Authentication fault. Check credentials.'))


class Logout(JsonResponseMixin, View):
    def post(self, request):
        logout(request)
        return self.json_success(msg=_('Logout successfully'))


class LoginStatus(JsonResponseMixin, View):
    def get(self, request):
        return self.json_success(is_authenticated=request.user.is_authenticated())


class Registration(JsonResponseMixin, CreateView):
    model = User
    form_class = RegistrationForm

    def send_email_managers(self):
        send_mail(
            _('New account registered on Law&Gauge.com'),
            _('New account {uid} registered on Law&Gauge.com').format(uid=self.object.get_full_name() or self.object.username),
            SERVER_EMAIL,
            [a[1] for a in MANAGERS],
            fail_silently=True
        )

    def form_valid(self, form):
        data = form.cleaned_data
        user = User(
            type=data.get('type'),
            first_name=data.get('first_name', ''),
            last_name=data.get('last_name', ''),
            language=data.get('language')
        )

        if check_phone_code(data.get('phone'), data.get('code')):
            user.phone = data.get('phone')
        elif check_vk_auth(data.get('vk_id'), self.request):
            user.vk_id = data.get('vk_id')
        elif check_fb_auth(data.get('fb_id'), data.get('access_token')):
            user.fb_id = data.get('fb_id')
        else:
            return self.json_fail(error=_('Registration error. Check passed credentials.'))

        user.set_uuid_username()
        user.set_unusable_password()
        user.save()

        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, user)

        if user.type == TYPE_CLIENT:
            chat = user.user_chats.filter(default=True).first()
            message = self.request.POST.get('message', '')
            if message:
                Message.objects.create(chat=chat, sender=user, type=MESSAGE_TYPE_TEXT, text=message)
        elif user.type == TYPE_CONTRACTOR:
            user.is_active = False
            user.save()

        self.object = user
        self.send_email_managers()

        return self.json_success(msg=_('Account registered successfully'), token=user.username)

    def form_invalid(self, form):
        return self.json_fail(error=form.errors.as_json())


class RegistrationUpdate(JsonResponseMixin, UpdateView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'token'
    form_class = RegistrationUpdateForm
    template_name = 'landing/contractor_extra_registration.html'

    def form_valid(self, form):
        if form.cleaned_data['phone'] and self.object.phone != form.cleaned_data['phone'] \
                and User.objects.filter(phone=form.cleaned_data['phone']).exists():
            return self.json_fail(error=_('User with this phone already registered'))
        user = form.save()

        old_lang = get_language()
        if user.language in ['ru', 'en']:
            translation.activate(user.language)

        if user.type == TYPE_CONTRACTOR and user.passed:
            user.is_active = True
            user.save()
            chat = user.user_chats.filter(default=True).first()
            for u in chat.participans.all():
                if u.is_admin():
                    Message.objects.create(chat=chat, sender=u, type=MESSAGE_TYPE_TEXT, text=_('Congratulations to the registration'))
                    break

        elif user.type == TYPE_CONTRACTOR and not user.passed:
            _mail(TEMPLATE_REGISTRATION_CONTRACTOR, recipient=user, language=user.language)

        services_count = self.request.POST.get('services_count', 0)
        try:
            services_count = int(services_count)
        except:
            services_count = 0

        for i in range(services_count):
            if i != 0:
                sform = ServiceForm(self.request.POST, prefix=i)
            else:
                sform = ServiceForm(self.request.POST)

            if sform.is_valid():
                service = sform.save(commit=False)
                service.user = user
                service.save()

                lang = get_language()

                if i != 0:
                    branch = sform.cleaned_data['%d-branch' % i]
                    branch_name = sform.cleaned_data['%d-branch_name' % i]
                    institute_name = sform.cleaned_data['%d-institute_name' % i]
                else:
                    branch = sform.cleaned_data['branch']
                    branch_name = sform.cleaned_data['branch_name']
                    institute_name = sform.cleaned_data['institute_name']

                if branch_name and institute_name:
                    branch = LawBranch.objects.create(**{'name_' + lang: branch_name})
                    institute = LawInstitute.objects.create(**{'name_' + lang: institute_name, 'parent': branch})
                    service.parent = institute
                    service.save()
                elif branch and institute_name:
                    institute = LawInstitute.objects.create(**{'name_' + lang: institute_name, 'parent': branch})
                    service.parent = institute
                    service.save()

                if not ServiceTip.objects.filter(**{'name_' + lang: service.name}).exists():
                    ServiceTip.objects.create(**{'name_' + lang: service.name, 'parent': service.parent})
            else:
                return self.json_fail(error=sform.errors)

        translation.activate(old_lang)

        return self.json_success(msg=_('Account updated successfully'), token=user.username)

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)

    def get(self, request, *args, **kwargs):
        try:
            user = self.get_object()
        except:
            raise Http404
        if user.type == TYPE_CLIENT:
            raise Http404
        elif user.type == TYPE_CONTRACTOR and not user.passed:
            raise Http404
        elif user.type == TYPE_CONTRACTOR and user.is_active:
            raise Http404
        else:
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
        return super(RegistrationUpdate, self).get(request, *args, **kwargs)


class CheckRegistration(JsonResponseMixin, FormView):
    form_class = CheckRegistrationForm

    def form_valid(self, form):
        phone = form.cleaned_data['phone']
        vk_id = form.cleaned_data['vk_id']
        fb_id = form.cleaned_data['fb_id']

        try:
            if phone:
                phone = re.sub(r'[\s\-]+', '', phone)
                user = User.objects.get(phone=phone)
            elif vk_id:
                user = User.objects.get(vk_id=vk_id)
            elif fb_id:
                user = User.objects.get(fb_id=fb_id)
            else:
                return self.json_fail(error=_('Error'))
        except:
            return self.json_success(need_registration=None)
        else:
            return self.json_success(token=user.username, need_registration=not bool(user.is_active or (user.first_name and user.last_name and user.email)))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class GetServiceTips(JsonResponseMixin, View):
    def get(self, request):
        data = [b.as_dict() for b in LawBranch.objects.filter(active=True) if b.name]
        return self.json_success(data=data)


class GetCurrencies(JsonResponseMixin, View):
    def get(self, request):
        data = [c.as_dict() for c in Currency.objects.all()]
        return self.json_success(data=data)


class PhoneCodeGenerating(JsonResponseMixin, FormView):
    """
    Generates Code and send it to phone number
    """
    form_class = PhoneCodeForm

    def get(self, request, *args, **kwargs):
        return HttpResponseForbidden

    def send_code(self, phone, code):
        url = 'http://api.unisender.com/ru/api/sendSms/'
        params = {
            'format': 'json',
            'api_key': UNISENDER_API_KEY,
            'phone': phone,
            'sender': 'lawandgauge',
            'text': _('Confirmation code: ') + str(code)
        }
        r = requests.get(url, params=params)

    def form_valid(self, form):
        phone_code = form.save(commit=False)
        PhoneCode.objects.filter(phone=phone_code.phone).update(active=False)
        code = random.randint(1000, 9999)
        while PhoneCode.objects.filter(active=True, code=code).exists():
            code = random.randint(1000, 9999)
        phone_code.code = code
        phone_code.save()
        self.send_code(phone_code.phone, phone_code.code)
        return self.json_success(msg=_('Code generated'))

    def form_invalid(self, form):
        activate('en')
        return self.json_fail(error=form.errors)


class GetCurrentUserView(JsonResponseMixin, View):
    def get(self, request):
        if request.user.is_authenticated():
            return self.json_success(**request.user.as_dict())
        else:
            return self.json_fail(error=_('User is not authenticated'))


class GetUsersView(JsonResponseMixin, LoginRequiredJsonMixin, RequiredPermissionsJsonMixin, View):
    required_permissions = ['chat.add_chat', 'chat.change_chat', 'chat.delete_chat']

    def get(self, request):
        users = User.objects.filter(is_active=True)
        return self.json_success(data=[
            u.as_dict() for u in users
        ])


class SetTimezone(JsonResponseMixin, LoginRequiredJsonMixin, View):
    def post(self, request):
        tz_name = request.POST.get('tz')
        if tz_name:
            user = request.user
            user.tz = tz_name
            user.save()
            return self.json_success(msg=_('Timezone set successfully'))
        return self.json_fail(error=_('Error setting timezone'))


class UploadImageToVK(JsonResponseMixin, FormView):
    form_class = ShareMapForm

    def post(self, request, *args, **kwargs):
        token = request.POST.get('token')
        if not token or not User.objects.filter(username=token).exists():
            return self.json_fail(error=_('Token is not valid'))
        self.user = User.objects.get(username=token)
        return super(UploadImageToVK, self).post(request, *args, **kwargs)

    def _get_image(self, url, avatar_url=None, name=None, city=None):
        try:
            r = urllib2.urlopen(url.encode('utf-8'))
        except:
            return None
        image = StringIO(r.read())
        map = Image.open(image).convert('RGB')
        width, height = map.size
        map = map.crop(((width - 1200)/2, (height - 630)/2, (width + 1200)/2, (height + 630)/2))
        foreground = Image.open(os.path.join(BASE_DIR, 'static', 'img', 'share_map_foreground_2.png'))
        mask = Image.open(os.path.join(BASE_DIR, 'static', 'img', 'mask.png')).convert('L')
        if avatar_url:
            r = urllib2.urlopen(avatar_url.encode('utf-8'))
            avatar = Image.open(StringIO(r.read())).convert('RGB')
        else:
            if self.user.avatar:
                avatar = Image.open(self.user.avatar).convert('RGB')
            else:
                avatar = Image.open(os.path.join(BASE_DIR, 'static', 'img', 'default_avatar.jpg')).convert('RGB')
        avatar = ImageOps.fit(avatar, mask.size)
        avatar.putalpha(mask)

        map.paste(foreground, (0, 0), foreground)
        map.paste(avatar, (600 - 100, 320 - 100), avatar)

        draw = ImageDraw.Draw(map)
        font = ImageFont.truetype(os.path.join(BASE_DIR, 'static', 'fonts', 'roboto-light.ttf'), 38)
        font1 = ImageFont.truetype(os.path.join(BASE_DIR, 'static', 'fonts', 'roboto-light.ttf'), 22)
        name = name or self.user.get_full_name()
        name_w, name_h = draw.textsize(name.upper(), font=font)
        draw.text((600 - name_w/2, 60 - name_h/2), name.upper(), (23, 36, 63), font=font)
        if city or self.user.city:
            city = city or self.user.city.name
            city_w, city_h = draw.textsize(city.upper(), font=font1)
            draw.text((600 - city_w/2, 550), city.upper(), (255, 255, 255), font=font1)

        map_io = StringIO()
        map.save(map_io, format='PNG')
        shared_map = SharedMap.objects.create(user=self.user)
        shared_map.img.save('shared_map.png', ContentFile(map_io.getvalue()))
        return shared_map.img

    def _find_marker(self, image):
        im = Image.open(image)
        im = im.convert('RGB')
        pixels = im.load()
        size = im.size
        for x in range(size[0]):
            for y in range(size[1]):
                r, g, b = pixels[x, y]
                if r in range(200, 256) and g in range(51) and b in range(51):
                    return x, y

    def form_valid(self, form):
        data = form.cleaned_data
        upload_url = data.get('upload_url')
        image_url = data.get('image_url')
        avatar_url = data.get('avatar')
        image = self._get_image(image_url, avatar_url=avatar_url)
        if image:
            # image = get_thumbnail(image, '1200x630', crop='center')
            # print image.url
            r = requests.post(upload_url, files={'photo': image})
            if r.status_code == 200:
                data = r.json()
                data.update({'image_url': image.url})
                return self.json_success(data=data)
            else:
                return self.json_fail(error=_('Upload error %d') % r.status_code)

        return self.json_fail(error=_('Unexpected error'))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class UploadImageToFB(UploadImageToVK):
    def form_valid(self, form):
        data = form.cleaned_data
        image_url = data.get('image_url')
        avatar_url = data.get('avatar')
        name = data.get('name')
        city = data.get('city')
        try:
            image = self._get_image(image_url, avatar_url=avatar_url, name=name, city=city)
        except Exception as e:
            return self.json_fail(error=_('Unexpected error. %s') % str(e))
        # image = get_thumbnail(image, '1200x630', crop='center')
        if image:
            return self.json_success(data={'image_url': image.url})
        else:
            return self.json_fail(error=_('Error while processing image'))

#___PRIVATE CABINET METHODS___#

class CabinetView(JsonResponseMixin, LoginRequiredJsonMixin, UpdateView):
    queryset = User.objects.filter(type=TYPE_CONTRACTOR)

    def get_object(self, queryset=None):
        return self.request.user

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class AvatarUpdateView(CabinetView):
    form_class = AvatarUpdateForm

    def form_valid(self, form):
        # self.object = form.save()
        # return self.json_success(msg=_('Avatar updated successfully'), avatar_url=self.object.get_avatar())
        return HttpResponse('METHOD DEPRECATED')


class MainInfoUpdateView(CabinetView):
    form_class = MainInfoForm

    def form_valid(self, form):
        self.object = form.save()
        return self.json_success(msg=_('Main information updated successfully'), avatar_url=self.object.get_avatar())


class AddressUpdateView(CabinetView):
    form_class = AddressForm

    def get_object(self, queryset=None):
        user = self.request.user
        address, created = ContractorAddress.objects.get_or_create(user=user)
        return address

    def form_valid(self, form):
        address = form.save()
        return self.json_success(msg=_('Address updated successfully'))


class ProfessionalInfoUpdateView(CabinetView):
    form_class = ProfessionalInfoForm

    def form_valid(self, form):
        user = form.save()
        return self.json_success(msg=_('Main information updated successfully'))


class DiplomCreateView(JsonResponseMixin, LoginRequiredJsonMixin, CreateView):
    model = ContractorDiplom
    form_class = DiplomForm

    def form_valid(self, form):
        diplom = form.save(commit=False)
        diplom.user = self.request.user
        diplom.save()
        return self.json_success(
            msg=_('Diplom added successfully'),
            delete_url=reverse('account:cabinet_diplom_delete', kwargs={'pk': diplom.pk})
        )

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class DiplomDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    model = ContractorDiplom

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        diplom = self.get_object()
        diplom.delete()
        return self.json_success(msg=_('Diplom deleted successfully'))


class LicenseCreateView(JsonResponseMixin, LoginRequiredJsonMixin, CreateView):
    model = ContractorLicense
    form_class = LicenseForm

    def form_valid(self, form):
        license = form.save(commit=False)
        license.user = self.request.user
        license.save()
        return self.json_success(
            msg=_('License added successfully'),
            delete_url=reverse('account:cabinet_license_delete', kwargs={'pk': license.pk})
        )


class LicenseDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    model = ContractorLicense

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        license = self.get_object()
        license.delete()
        return self.json_success(msg=_('License deleted successfully'))


class AssociationCreateView(JsonResponseMixin, LoginRequiredJsonMixin, CreateView):
    model = ContractorAssociation
    form_class = AssociationForm

    def form_valid(self, form):
        association = form.save(commit=False)
        association.user = self.request.user
        association.save()
        return self.json_success(
            msg=_('Association added successfully'),
            delete_url=reverse('account:cabinet_association_delete', kwargs={'pk': association.pk})
        )


class AssociationDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    model = ContractorAssociation

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        association = self.get_object()
        association.delete()
        return self.json_success(msg=_('Association deleted successfully'))


class ServiceCreateView(JsonResponseMixin, LoginRequiredJsonMixin, CreateView):
    model = Service
    form_class = ServiceForm

    def form_valid(self, form):
        service = form.save(commit=False)
        service.user = self.request.user
        service.save()

        branch = form.cleaned_data['branch']
        branch_name = form.cleaned_data['branch_name']
        institute_name = form.cleaned_data['institute_name']

        lang = get_language()

        if branch_name and institute_name:
            branch = LawBranch.objects.create(**{'name_' + lang: branch_name})
            institute = LawInstitute.objects.create(**{'name_' + lang: institute_name, 'parent': branch})
            service.parent = institute
            service.save()
        elif branch and institute_name:
            institute = LawInstitute.objects.create(**{'name_' + lang: institute_name, 'parent': branch})
            service.parent = institute
            service.save()

        ServiceTip.objects.get_or_create(**{'name_' + lang: service.name, 'parent': service.parent})
        return self.json_success(
            msg=_('Service added successfully'),
            delete_url=reverse('account:cabinet_service_delete', kwargs={'pk': service.pk})
        )

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class ServiceDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    model = Service

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        service = self.get_object()
        service.delete()
        return self.json_success(msg=_('Service deleted successfully'))


class LawBranchCreateView(JsonResponseMixin, LoginRequiredJsonMixin, CreateView):
    model = LawBranch
    form_class = LawBranchForm

    def form_valid(self, form):
        name = form.cleaned_data['name']
        branch = LawBranch.objects.create(**{'name_' + get_language(): name})
        return self.json_success(
            msg=_('Law branch added successfully'),
            delete_url=reverse('account:cabinet_lawbranch_delete', kwargs={'pk': branch.pk})
        )

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class LawBranchDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    model = LawBranch

    def post(self, request, *args, **kwargs):
        branch = self.get_object()
        branch.delete()
        return self.json_success(msg=_('Branch deleted successfully'))


class LawInstituteCreateView(JsonResponseMixin, LoginRequiredJsonMixin, CreateView):
    model = LawInstitute
    form_class = LawInstituteForm

    def form_valid(self, form):
        institute = form.save(commit=False)
        name = form.cleaned_data['name']
        setattr(institute, 'name_' + get_language(), name)
        institute.save()
        return self.json_success(
            msg=_('Law branch added successfully'),
            delete_url=reverse('account:cabinet_lawinstitute_delete', kwargs={'pk': institute.pk})
        )

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class LawInstituteDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    model = LawInstitute

    def post(self, request, *args, **kwargs):
        institute = self.get_object()
        institute.delete()
        return self.json_success(msg=_('Law institute deleted successfully'))


class NotificationsUpdateView(CabinetView):
    form_class = NotificationForm

    def get_object(self, queryset=None):
        user = self.request.user
        notifications, created = Notification.objects.get_or_create(user=user)
        return notifications

    def form_valid(self, form):
        notifications = form.save()
        return self.json_success(msg=_('Notifications updated successfully'))


class PhoneUpdateView(JsonResponseMixin, LoginRequiredJsonMixin, UpdateView):
    form_class = PhoneUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        user = self.get_object()
        phone = form.cleaned_data['phone']
        code = form.cleaned_data['code']
        if PhoneCode.objects.filter(phone=phone, code=code, active=True).exists():
            PhoneCode.objects.filter(phone=phone, code=code, active=True).update(active=False)
            user.phone = phone
            user.save()
            return self.json_success(msg=_('Phone updated successfully'))
        else:
            return self.json_fail(error=_('Code is not valid'))

    def form_invalid(self, form):

        return self.json_fail(error=form.errors)


class ResumeDeleteView(JsonResponseMixin, LoginRequiredJsonMixin, UpdateView):

    def get_object(self, queryset=None):
        return self.request.user

    def post(self, request, *args, **kwargs):
        user = self.get_object()
        user.resume = None
        user.save()
        return self.json_success(msg=_('Resume deleted'))


class ConnectVKAuth(JsonResponseMixin, LoginRequiredJsonMixin, View):
    def post(self, request):
        user_id = request.POST.get('user_id')
        if check_vk_auth(user_id, request):
            user = self.request.user
            user.vk_id = user_id
            user.save()
            return self.json_success(msg=_('VK authentication connected successfully'))
        else:
            return self.json_fail(error=_('VK Authentication failed'))

class ConnectFBAuth(JsonResponseMixin, LoginRequiredJsonMixin, View):
    def post(self, request):
        user_id = request.POST.get('user_id')
        access_token = request.POST.get('access_token')
        if check_fb_auth(user_id, access_token):
            user = self.request.user
            user.fb_id = user_id
            user.save()
            return self.json_success(msg=_('FB authentication connected successfully'))
        else:
            return self.json_fail(error=_('FB Authentication failed'))

