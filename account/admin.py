from django.contrib import admin
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe

from account.models import User, TYPE_CONTRACTOR, PhoneCode, Contractor, Client, TYPE_CLIENT, Admin, \
    Service, Currency, LawBranch, LawInstitute, ServiceTip, SharedMap, ContractorDiplom, ContractorLicense, \
    ContractorAssociation, InvitedContractor


class UserAdmin(admin.ModelAdmin):
    list_display = ('avatar_preview', 'username', 'first_name', 'last_name', 'type', 'email', 'phone', 'vk_id', 'fb_id', 'is_active')
    list_filter = ('type', 'is_active')
    search_fields = ('first_name', 'last_name', 'email', 'phone', 'vk_id', 'fb_id')
    fieldsets = (
        ('', {
            'fields': ('username', 'first_name', 'last_name', 'phone', 'email', 'vk_id', 'fb_id', 'avatar', 'tz', 'language', 'is_active'),
        }),
        (_('Contractor fields'), {
            'fields': ('country', 'city', 'status', 'site', 'linkedin', 'resume', 'specialization', 'passed'),
        })
    )
    raw_id_fields = ['city']
    autocomplete_lookup_fields = {
        'fk': ['city'],
    }

    def avatar_preview(self, obj):
        return mark_safe('<img width="50" src={}>'.format(obj.get_avatar()))
    avatar_preview.short_description = _('Avatar')


class RawUserAdmin(admin.ModelAdmin):
    list_display = ('avatar_preview', 'username', 'first_name', 'last_name', 'type', 'email', 'phone', 'vk_id', 'fb_id', 'is_active')
    raw_id_fields = ['city']
    autocomplete_lookup_fields = {
        'fk': ['city'],
    }

    def avatar_preview(self, obj):
        return mark_safe('<img width="50" src={}>'.format(obj.get_avatar()))
    avatar_preview.short_description = _('Avatar')

admin.site.register(User, RawUserAdmin)

class ClientAdmin(UserAdmin):
    list_display = ('avatar_preview', 'username', 'first_name', 'last_name', 'email', 'phone', 'vk_id', 'fb_id', 'is_active')
    list_filter = []

    def get_queryset(self, request):
        return self.model.objects.filter(type=TYPE_CLIENT)

admin.site.register(Client, ClientAdmin)

class DiplomInline(admin.TabularInline):
    model = ContractorDiplom
    extra = 0

class LicenseInline(admin.TabularInline):
    model = ContractorLicense
    extra = 0

class AssociationInline(admin.TabularInline):
    model = ContractorAssociation
    extra = 0

class ServiceInline(admin.TabularInline):
    model = Service
    extra = 0

class ContactorAdmin(UserAdmin):
    inlines = [DiplomInline, LicenseInline, AssociationInline, ServiceInline]
    list_display = ('avatar_preview', 'username', 'first_name', 'last_name', 'email', 'status', 'phone', 'vk_id', 'fb_id', 'country', 'city', 'passed', 'is_active')
    list_editable = ['passed']
    list_filter = []

    def get_queryset(self, request):
        return self.model.objects.filter(type=TYPE_CONTRACTOR)

admin.site.register(Contractor, ContactorAdmin)

class InvitedContractorAdmin(admin.ModelAdmin):
    list_display = ('avatar_preview', 'first_name', 'last_name', 'type', 'email', 'phone', 'vk_id', 'fb_id', 'is_active')
    search_fields = ('first_name', 'last_name', 'email', 'phone', 'vk_id', 'fb_id')
    fieldsets = (
        ('', {
            'fields': ('first_name', 'last_name', 'phone', 'email', 'vk_id', 'fb_id', 'avatar', 'country', 'language'),
        }),
    )

    def avatar_preview(self, obj):
        return mark_safe('<img width="50" src={}>'.format(obj.get_avatar()))
    avatar_preview.short_description = _('Avatar')

    def get_queryset(self, request):
        return self.model.objects.filter(type=TYPE_CONTRACTOR, invited=True)

admin.site.register(InvitedContractor, InvitedContractorAdmin)

class AdminAdmin(UserAdmin):
    list_display = ('avatar_preview', 'username', 'first_name', 'last_name', 'email', 'is_active')
    list_filter = []

    def get_queryset(self, request):
        return self.model.objects.filter(groups__permissions__codename='add_chat')

admin.site.register(Admin, AdminAdmin)

class PhoneCodeAdmin(admin.ModelAdmin):
    list_display = ('phone', 'code', 'active', 'date')

admin.site.register(PhoneCode, PhoneCodeAdmin)
admin.site.register(Service)

class ServiceTipAdmin(admin.ModelAdmin):
    list_display = ['name_ru', 'name_en', 'parent']

admin.site.register(ServiceTip, ServiceTipAdmin)

class ServiceTipInline(admin.TabularInline):
    model = ServiceTip
    extra = 0

class LawInstituteAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name_ru', 'name_en', 'parent', 'active')
    list_filter = ['parent']
    search_fields = ['name_ru', 'name_en']
    list_editable = ['name_ru', 'name_en']
    inlines = [ServiceTipInline]

admin.site.register(LawInstitute, LawInstituteAdmin)

class LawInstituteInline(admin.TabularInline):
    model = LawInstitute
    extra = 0

class LawBranchAdmin(admin.ModelAdmin):
    inlines = [LawInstituteInline]
    list_display = ('name_ru', 'name_en', 'active')

admin.site.register(LawBranch, LawBranchAdmin)
admin.site.register(Currency)
admin.site.register(SharedMap)