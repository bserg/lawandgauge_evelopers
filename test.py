# -*- coding: utf-8 -*-
from PIL import Image, ImageOps, ImageDraw, ImageFont

map = Image.open('/home/serega/projects/lawandgauge/static/img/staticmap.png').convert('RGB')

#foreground
foreground = Image.open('/home/serega/projects/lawandgauge/static/img/share_map_foreground.png').convert('RGBA')

#avatar
mask = Image.open('/home/serega/projects/lawandgauge/static/img/mask.png').convert('L')
avatar = Image.open('/home/serega/projects/lawandgauge/static/img/default_avatar.jpg')
avatar = avatar.convert('RGB')
avatar = ImageOps.fit(avatar, mask.size)
avatar.putalpha(mask)

map.paste(foreground, (0, 0), foreground)

map.paste(avatar, (552, 199), avatar)


draw = ImageDraw.Draw(map)
font = ImageFont.truetype("/home/serega/projects/lawandgauge/static/fonts/open-sans-light.ttf", 48)
name_w, name_h = draw.textsize(u"Иван Петров", font=font)
draw.text((640 - name_w/2, 740 - name_h/2), u"Иван Петров", (255, 255, 255), font=font)

city_w, city_h = draw.textsize(u"Москва", font=font)
draw.text((640 - city_w/2, 500 - city_h/2), u"Москва", (255, 255, 255), font=font)


map.save('/home/serega/temp/output.png', format='PNG')
# shared_map = SharedMap.objects.create(user=self.request.user)
# shared_map.img.save('shared_map.png', ContentFile(map_io.getvalue()))