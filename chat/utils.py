import datetime
from django.contrib import auth
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.utils.importlib import import_module
import pytz
from lawandgauge import settings

_engine = import_module(settings.SESSION_ENGINE)


def get_session(session_key): 
    return _engine.SessionStore(session_key)


def get_user(session):
    class Dummy(object):
        pass
    django_request = Dummy()
    django_request.session = session
    return auth.get_user(django_request)


def format_date(date, tz=None):
    now = timezone.now()
    if tz:
        try:
            now = datetime.datetime.now(pytz.timezone(tz))
            date = timezone.localtime(date, pytz.timezone(tz))
        except Exception as e:
            pass
    if now.date() == date.date():
        return _('Today') + ' ' + date.strftime('%H:%M')
    elif 2 > (now.date() - date.date()).days >= 1:
        return _('Yesterday') + ' ' + date.strftime('%H:%M')
    else:
        return date.strftime('%d.%m.%Y %H:%M')