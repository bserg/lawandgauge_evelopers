import json
import uuid
import datetime
import os

from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.html import escape
from django.utils.translation import ugettext as _
from django.db import models
import pytz

from account.models import User, Contractor, Client, InvitedContractor
from chat.utils import format_date


class Chat(models.Model):
    uid = models.UUIDField(_('UID'), default=uuid.uuid4, editable=False, unique=True)
    title = models.CharField(_('Ttile'), max_length=255)
    participans = models.ManyToManyField(User, verbose_name=_('Participants'), related_name='user_chats', blank=True)
    default = models.BooleanField(_('Default'), default=False)
    date = models.DateTimeField(_('Date'), auto_now_add=True)
    last_message = models.DateTimeField(_('Last message date'), blank=True, null=True)
    active = models.BooleanField(_('Active'), default=True)

    def is_active(self):
        return self.active and all([p.active for p in self.participans])

    def get_last_message(self):
        try:
            return self.chat_messages.latest()
        except Message.DoesNotExist:
            return

    def get_title(self, user=None):
        if self.default and user and user.is_admin():
            client = self.participans.exclude(pk=user.pk).first()
            if client:
                return client.get_name()
        return self.title

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-default', '-last_message', 'title']
        verbose_name = _('Chat').encode('utf-8')
        verbose_name_plural = _('Chats').encode('utf-8')


MESSAGE_TYPE_TEXT = 1
MESSAGE_TYPE_PROPOSAL = 2
MESSAGE_TYPE_PAYMENT = 3
MESSAGE_TYPE_TEXT_LINK = 4

MESSAGE_TYPES = (
    (MESSAGE_TYPE_TEXT, _('Text')),
    (MESSAGE_TYPE_TEXT_LINK, _('Text with links')),
    (MESSAGE_TYPE_PROPOSAL, _('Proposal')),
    (MESSAGE_TYPE_PAYMENT, _('Payment')),
)


class Message(models.Model):
    sender = models.ForeignKey(User, verbose_name=_('Sender'))
    chat = models.ForeignKey(Chat, verbose_name=_('Chat'),  related_name='chat_messages')
    type = models.PositiveSmallIntegerField(_('Type'), choices=MESSAGE_TYPES, default=MESSAGE_TYPE_TEXT)
    text = models.TextField(_('Text'), max_length=1000, blank=True)

    date = models.DateTimeField(_('Date'), auto_now_add=True)
    active = models.BooleanField(_('Active'), default=True)

    def as_dict(self, tz=None):
        data = dict(
            sender=self.sender.pk,
            chat=str(self.chat.uid),
            type=self.type,
            text=escape(self.text),
            date=format_date(self.date, tz),
            meta={'new_message_count': None},
            is_first=self.is_first(tz)
        )
        data['attachments'] = Attachment.as_dict(self.message_attachments.all())
        return data

    def is_first(self, tz=None):
        try:
            date = timezone.localtime(self.date, pytz.timezone(tz))
        except:
            date = self.date
        try:
            return Message.objects.filter(chat=self.chat, date__range=[date.date(), date.date() + datetime.timedelta(days=1)]).earliest('date') == self
        except:
            return False

    def as_json(self):
        data = self.as_dict()
        return json.dumps(data)

    def __unicode__(self):
        return self.text

    class Meta:
        ordering = ['date']
        get_latest_by = 'date'
        verbose_name = _('Message').encode('utf-8')
        verbose_name_plural = _('Messages').encode('utf-8')


class Attachment(models.Model):
    message = models.ForeignKey(Message, verbose_name=_('Message'), related_name='message_attachments', blank=True, null=True)
    uid = models.UUIDField(_('UUID'), default=uuid.uuid4, editable=False)
    file = models.FileField(_('File'), upload_to='attachments')
    date = models.DateTimeField(_('Date'), auto_now=True)

    def get_uid(self):
        return str(self.uid)

    def get_name(self):
        return os.path.basename(self.file.name)

    def get_url(self):
        return reverse('chat:attachment', kwargs={'uid': self.uid})

    def get_download_url(self):
        return self.get_url() + '?download=yes'

    @staticmethod
    def as_dict(attachments):
        return [{'filename': a.get_name(), 'url': a.get_url(), 'download_url': a.get_download_url()}
                for a in attachments]

    def __unicode__(self):
        return self.file.name

    class Meta:
        verbose_name = _('Attachment').encode('utf-8')
        verbose_name_plural = _('Attachments').encode('utf-8')


@receiver(post_save, sender=Message)
def update_last_message(sender, instance, created, **kwargs):
    if created:
        chat = instance.chat
        chat.last_message = instance.date
        chat.save()


@receiver(post_save, sender=User)
@receiver(post_save, sender=Contractor)
@receiver(post_save, sender=InvitedContractor)
@receiver(post_save, sender=Client)
def create_default_chat(sender, instance, created, **kwargs):
    if created:
        admins = User.objects.filter(Q(groups__permissions__codename='add_chat') | Q(user_permissions__codename='add_chat'))
        chat = Chat.objects.create(
            title='LegalSpace',
            default=True,
        )
        chat.participans.add(instance, *admins)