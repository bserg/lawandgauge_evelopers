# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0004_chat_uid'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='chat',
            options={'ordering': ['-default', '-last_message', 'title'], 'verbose_name': 'Chat', 'verbose_name_plural': 'Chats'},
        ),
        migrations.AddField(
            model_name='attachment',
            name='uid',
            field=models.UUIDField(default=uuid.uuid4, verbose_name='UUID', editable=False),
        ),
        migrations.AlterField(
            model_name='chat',
            name='participans',
            field=models.ManyToManyField(related_name='user_chats', verbose_name='Participans', to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
