# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0003_auto_20150415_1120'),
    ]

    operations = [
        migrations.AddField(
            model_name='chat',
            name='uid',
            field=models.UUIDField(default=uuid.uuid4, verbose_name='UID', unique=True, editable=False),
        ),
    ]
