# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'attachments', verbose_name='\u0424\u0430\u0439\u043b')),
                ('date', models.DateTimeField(auto_now=True, verbose_name='Date')),
            ],
            options={
                'verbose_name': 'Attachment',
                'verbose_name_plural': 'Attachments',
            },
        ),
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Ttile')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date')),
                ('last_message', models.DateTimeField(verbose_name='Last message date', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('participans', models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='Participans', blank=True)),
            ],
            options={
                'ordering': ['-last_message', 'title'],
                'verbose_name': 'Chat',
                'verbose_name_plural': 'Chats',
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442', blank=True)),
                ('type', models.PositiveSmallIntegerField(default=1, verbose_name='Type', choices=[(1, '\u0422\u0435\u043a\u0441\u0442'), (2, 'Proposal'), (3, 'Payment')])),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('chat', models.ForeignKey(related_name='chat_messages', verbose_name='Chat', to='chat.Chat')),
                ('sender', models.ForeignKey(verbose_name='Sender', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'get_latest_by': 'date',
                'verbose_name': 'Message',
                'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.AddField(
            model_name='attachment',
            name='message',
            field=models.ForeignKey(verbose_name='Message', blank=True, to='chat.Message', null=True),
        ),
    ]
