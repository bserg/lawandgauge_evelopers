# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0005_auto_20150418_2217'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attachment',
            options={'verbose_name': '\u0412\u043b\u043e\u0436\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0412\u043b\u043e\u0436\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterModelOptions(
            name='chat',
            options={'ordering': ['-default', '-last_message', 'title'], 'verbose_name': '\u0427\u0430\u0442', 'verbose_name_plural': '\u0427\u0430\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['date'], 'get_latest_by': 'date', 'verbose_name': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterField(
            model_name='attachment',
            name='date',
            field=models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='attachment',
            name='message',
            field=models.ForeignKey(related_name='message_attachments', verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435', blank=True, to='chat.Message', null=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name='chat',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='chat',
            name='default',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e'),
        ),
        migrations.AlterField(
            model_name='chat',
            name='last_message',
            field=models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0433\u043e \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='participans',
            field=models.ManyToManyField(related_name='user_chats', verbose_name='\u0423\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0438', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='title',
            field=models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a'),
        ),
        migrations.AlterField(
            model_name='message',
            name='active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name='message',
            name='chat',
            field=models.ForeignKey(related_name='chat_messages', verbose_name='\u0427\u0430\u0442', to='chat.Chat'),
        ),
        migrations.AlterField(
            model_name='message',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='message',
            name='type',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='\u0422\u0438\u043f', choices=[(1, '\u0422\u0435\u043a\u0441\u0442'), (4, '\u0422\u0435\u043a\u0441\u0442 \u0441\u043e \u0441\u0441\u044b\u043b\u043a\u0430\u043c\u0438'), (2, '\u041f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u0435'), (3, '\u041f\u043b\u0430\u0442\u0435\u0436')]),
        ),
    ]
