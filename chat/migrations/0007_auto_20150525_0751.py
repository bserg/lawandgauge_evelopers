# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0006_auto_20150508_2117'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attachment',
            options={'verbose_name': 'Attachment', 'verbose_name_plural': 'Attachments'},
        ),
        migrations.AlterModelOptions(
            name='chat',
            options={'ordering': ['-default', '-last_message', 'title'], 'verbose_name': 'Chat', 'verbose_name_plural': 'Chats'},
        ),
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['date'], 'get_latest_by': 'date', 'verbose_name': 'Message', 'verbose_name_plural': 'Messages'},
        ),
        migrations.AlterField(
            model_name='attachment',
            name='date',
            field=models.DateTimeField(auto_now=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='attachment',
            name='file',
            field=models.FileField(upload_to=b'attachments', verbose_name='File'),
        ),
        migrations.AlterField(
            model_name='attachment',
            name='message',
            field=models.ForeignKey(related_name='message_attachments', verbose_name='Message', blank=True, to='chat.Message', null=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='chat',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='chat',
            name='default',
            field=models.BooleanField(default=False, verbose_name='Default'),
        ),
        migrations.AlterField(
            model_name='chat',
            name='last_message',
            field=models.DateTimeField(null=True, verbose_name='Last message date', blank=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='participans',
            field=models.ManyToManyField(related_name='user_chats', verbose_name='Participants', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Ttile'),
        ),
        migrations.AlterField(
            model_name='message',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='message',
            name='chat',
            field=models.ForeignKey(related_name='chat_messages', verbose_name='Chat', to='chat.Chat'),
        ),
        migrations.AlterField(
            model_name='message',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(verbose_name='Sender', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='message',
            name='text',
            field=models.TextField(max_length=1000, verbose_name='Text', blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='type',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='Type', choices=[(1, 'Text'), (4, 'Text with links'), (2, 'Proposal'), (3, 'Payment')]),
        ),
    ]
