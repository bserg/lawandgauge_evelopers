# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attachment',
            name='message',
            field=models.ForeignKey(related_name='message_attachments', verbose_name='Message', blank=True, to='chat.Message', null=True),
        ),
        migrations.AlterField(
            model_name='chat',
            name='last_message',
            field=models.DateTimeField(null=True, verbose_name='Last message date', blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='text',
            field=models.TextField(max_length=1000, verbose_name='\u0422\u0435\u043a\u0441\u0442', blank=True),
        ),
    ]
