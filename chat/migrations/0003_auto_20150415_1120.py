# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0002_auto_20150415_0533'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='message',
            options={'ordering': ['date'], 'get_latest_by': 'date', 'verbose_name': 'Message', 'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f'},
        ),
        migrations.AddField(
            model_name='chat',
            name='default',
            field=models.BooleanField(default=False, verbose_name='Default'),
        ),
    ]
