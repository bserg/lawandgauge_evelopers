from django import forms
from chat.models import Message, Attachment, Chat


class UploadFileForm(forms.ModelForm):
    class Meta:
        model = Attachment
        fields = ['file']


class SendMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        exclude = ('sender', 'date', 'active')


class ChatForm(forms.ModelForm):
    class Meta:
        model = Chat
        fields = ['title', 'participans']
