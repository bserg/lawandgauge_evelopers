import json
from datetime import datetime
import brukva
from django.core.management.base import NoArgsCommand, BaseCommand
from django.utils import timezone
import pytz
from sockjs.tornado.router import SockJSRouter
from sockjs.tornado.periodic import Callback as periodic_callback
from chat.sockjs_server import ChatConnection, MESSAGE_TYPE_NOTIFICATION
import tornado.web
import tornado.ioloop
from lawandgauge.settings import SOCKJS_PORT


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('port', nargs='+', type=int)

    def send_midnight(self, redis_client):
        for client in ChatConnection.clients:
            try:
                now = timezone.localtime(timezone.now(), pytz.timezone(client.tz))
            except:
                now = timezone.now()
            if now.hour == now.minute == 0:
                redis_client.publish(client.channel, json.dumps({'type': 20, 'text': 'day separator'}))

    def handle(self, *args, **options):
        redis_client = brukva.Client()
        redis_client.connect()
        try:
            port = int(options['port'][0])
        except:
            port = SOCKJS_PORT
        router = SockJSRouter(ChatConnection)
        app = tornado.web.Application(router.urls)
        app.listen(port)
        instance = tornado.ioloop.IOLoop.instance()
        periodic_callback(lambda: self.send_midnight(redis_client), 60 * 1000, instance).start()
        instance.start()
