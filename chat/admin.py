from django.contrib import admin
from chat.models import Chat, Message, Attachment


class ChatAdmin(admin.ModelAdmin):
    list_display = ('title', 'uid', 'default', 'active')
    filter_horizontal = ('participans',)

admin.site.register(Chat, ChatAdmin)

admin.site.register(Attachment)

class AttachmentInline(admin.TabularInline):
    model = Attachment
    readonly_fields  = ('file', 'uid', 'date')

class MessageAdmin(admin.ModelAdmin):
    inlines = (AttachmentInline,)
    list_display = ('sender', 'chat', 'type', 'text')


admin.site.register(Message, MessageAdmin)
