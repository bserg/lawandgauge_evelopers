from django.conf.urls import patterns, url
from chat.views import GetAttachmentUrlView, DeleteAttachmentView, GetChatData, ChatView, UploadAttachment, \
    CreateChatView, UpdateChatView, SearchMessages, SendMessage


urlpatterns = patterns('',
    url(r'^$', ChatView.as_view(), name='get_chat_data'),
    url(r'^create/$', CreateChatView.as_view(), name='create_chat'),
    url(r'^update/(?P<uid>[\w\-]+)/$', UpdateChatView.as_view(), name='update_chat'),
    url(r'^data/$', GetChatData.as_view(), name='get_chat_data'),
    url(r'^search/$', SearchMessages.as_view(), name='search_messages'),
    url(r'^attachment/upload/', UploadAttachment.as_view(), name='upload_attachment'),
    url(r'^attachment/delete/(?P<uid>[\w\-]+)/$', DeleteAttachmentView.as_view(), name='delete_attachment'),
    url(r'^attachment/(?P<uid>[\w\-]+)/$', GetAttachmentUrlView.as_view(), name='attachment'),
)

urlpatterns += patterns('',
    url(r'^send/$', SendMessage.as_view(), name='send_message'),
)