import brukva
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.views.generic import FormView, TemplateView, View, ListView, DetailView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
import redis
from chat.forms import UploadFileForm, ChatForm
from chat.models import Chat, Message, Attachment
from chat.utils import format_date
from lawandgauge.mixins import JsonResponseMixin, LoginRequiredJsonMixin, LoginRequiredMixin, \
    RequiredPermissionsJsonMixin
from lawandgauge.settings import SOCKJS_URL


class ChatView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(ChatView, self).get_context_data(**kwargs)
        context['sockjs_url'] = SOCKJS_URL
        return context

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, request, *args, **kwargs):
        return super(ChatView, self).dispatch(request, *args, **kwargs)


class GetChatData(JsonResponseMixin, LoginRequiredJsonMixin, View):
    """
    Getting extended data for all user chats
    """
    def get(self, request):
        r = redis.StrictRedis()
        #TODO test below stuff
        # chats = self.request.user.user_chats.filter(active=True, participans__active=True)
        if request.user.is_admin():
            chats = Chat.objects.filter(active=True)
        else:
            chats = self.request.user.user_chats.filter(active=True)
        if not chats:
            return self.json_success(data=[])
        else:
            return self.json_success(data=[{
                "id": str(chat.uid),
                "title": chat.get_title(request.user),
                "is_default": chat.default,
                "last_message": chat.get_last_message().text if chat.get_last_message() else "",
                "members": {p.pk: p.as_dict() for p in chat.participans.all()},
                "messages": [m.as_dict(tz=request.user.tz) for m in chat.chat_messages.filter(active=True)],
                "newMessageCount": r.get('unread_{}_{}'.format(request.user.pk, str(chat.uid))) or 0,
            } for chat in chats])


class UploadAttachment(JsonResponseMixin, LoginRequiredJsonMixin, FormView):
    """
    Upload attachment
    """
    form_class = UploadFileForm

    def form_valid(self, form):
        object = form.save()
        return self.json_success(msg=_('File have been uploaded'), attachment=str(object.uid), delete_url=reverse('chat:delete_attachment', kwargs={'uid': str(object.uid)}))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class GetAttachmentUrlView(LoginRequiredMixin, DetailView):
    """
    Getting downloadable url for attachment
    """
    model = Attachment
    slug_field = 'uid'
    slug_url_kwarg = 'uid'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.message.chat.participans.filter(pk=request.user.pk).exists():
            raise PermissionDenied
        if request.GET.get('download', None):
            response = HttpResponse(self.object.file, content_type='application/octet-stream')
            response['Content-Disposition'] = u'attachment; filename="%s"' % self.object.get_name()
            return response
        else:
            return HttpResponseRedirect(self.object.file.url)


class DeleteAttachmentView(JsonResponseMixin, LoginRequiredJsonMixin, DeleteView):
    """
    Deleting attachment
    """
    model = Attachment
    slug_field = 'uid'
    slug_url_kwarg = 'uid'

    def get(self, request, *args, **kwargs):
        return self.json_fail(error=_('Method not allowed'))

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.message.chat.participans.filter(pk=request.user.pk).exists():
            return self.json_fail(error=_('Access denied'))
        self.object.delete()
        return self.json_success(msg=_('Attachment have been deleted'))


class CreateChatView(JsonResponseMixin, LoginRequiredJsonMixin, RequiredPermissionsJsonMixin, CreateView):
    """
    Creating chat
    """
    model = Chat
    form_class = ChatForm
    required_permissions = ['chat.add_chat']

    def form_valid(self, form):
        chat = form.save()
        return self.json_success(msg=_('Chat has been created'), chat=str(chat.uid))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class UpdateChatView(JsonResponseMixin, LoginRequiredJsonMixin, RequiredPermissionsJsonMixin, UpdateView):
    """
    Changing chat data (title or participants)
    """
    model = Chat
    form_class = ChatForm
    slug_field = 'uid'
    slug_url_kwarg = 'uid'
    required_permissions = ['chat.change_chat']

    def form_valid(self, form):
        participans = form.cleaned_data['participans']
        if not participans:
            self.object = self.get_object()
            self.object.delete()
            return self.json_success(msg=_('Chat has been deleted'))
        else:
            self.object = form.save()
            self.object.participans.clear()
            self.object.participans.add(*[p.pk for p in participans])
            return self.json_success(msg=_('Chat has been updated'))

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class SearchMessages(JsonResponseMixin, LoginRequiredJsonMixin, View):
    """
    Searching messages for all user chats
    """
    model = Message

    def get(self, request):
        query = request.GET.get('query')
        if query:
            qs = self.model.objects.filter(
                chat__in=self.request.user.user_chats.filter(active=True),
                text__icontains=query.strip()
            )
            messages = [m.as_dict() for m in qs]
            return self.json_success(data=messages)
        else:
            return self.json_fail(error=_('Query is empty'))


class SendMessage(JsonResponseMixin, View):
    """IN PROGRESS"""
    def post(self, request):
        client = brukva.Client()
        client.connect()
        message = request.POST.get('message')
        chat = request.POST.get('chat')
        if not chat:
            chats = [str(c.uid) for c in Chat.objects.filter(active=True)]
            for chat in chats:
                client.publish(chat, message)
        else:
            client.publish(chat, message)

        return self.json_success(msg=_('Messages sent'))

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SendMessage, self).dispatch(request, *args, **kwargs)

