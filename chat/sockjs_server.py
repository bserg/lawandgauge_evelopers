import json
import re

import brukva
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.utils.html import escape
from sockjs.tornado import SockJSConnection

from chat.models import Message, Chat, MESSAGE_TYPE_TEXT, Attachment, MESSAGE_TYPE_PROPOSAL, MESSAGE_TYPE_PAYMENT, \
    MESSAGE_TYPE_TEXT_LINK
from chat.utils import get_session, get_user, format_date

PREFIX_MESSAGE = 'message'
PREFIX_NOTIFICATION = 'notification'
PREFIX_ATTACHMENT = 'attachment'
PREFIX_CONNECT = 'connect'
PREFIX_CLOSE = 'close'
PREFIX_HISTORY = 'history'
PREFIX_SEARCH = 'search'
PREFIX_GET_LAST_MESSAGES = 'get_last_messages'
PREFIX_GET_ALL_MESSAGES = 'get_messages'

MESSAGE_TYPE_NOTIFICATION = 10
MESSAGE_TYPE_START_TYPING = 11
MESSAGE_TYPE_STOP_TYPING = 12

MESSAGE_TYPE_SEPARATOR = 20

LAST_MESSAGES_NUM = 10

c = brukva.Client()
c.connect()


class ChatConnection(SockJSConnection):
    """
    channel - current chat channel
    channels - user active chats
    """
    channel = None
    channels = []
    clients = []

    def __init__(self, *args, **kwargs):
        super(ChatConnection, self).__init__(*args, **kwargs)
        self.clients.append(self)
        self.redis_client = brukva.Client()
        self.redis_client.connect()

    def on_open(self, request):
        session_id = request.get_cookie('sessionid')
        if not session_id:
            self.close()
            return

        session = get_session(session_id.value)
        self.user = get_user(session)
        if not self.user:
            self.close()
            return

        self.user_id = self.user.pk
        self.tz = self.user.tz

        self.redis_client.subscribe([chat.uid for chat in self.user.user_chats.all()])
        self.redis_client.listen(self.redis_message_callback)

    def on_message(self, message):
        """
        Handler for processing messages from SockJS client
        Messages are Json stringifies objects
        MESSAGE:
            {type: "message", text: <text>, attachments: [<file_uid>, ...]}
        NOTIFICATION:
            {type: "notification", subtype: NOTIFICATION_TYPE}
        CONNECT:
            {type: "connect", chat: <chat_uid>}
        CLOSE:
            {type: "close", chat: <chat_uid>}
        HISTORY:
            {type: "history", chat: <chat_uid>, number: <10>, docs_only:true|false}
        """
        try:
            data = json.loads(message)
        except:
            self.send_notification(_('Error occurred while processing message. It must be stringify JSON object.'))
            return

        try:
            if data['type'] == PREFIX_MESSAGE:
                if data.get('text') or data.get('attachments'):

                    if re.search(r'https?://\S+', data.get('text', '')):
                        type = MESSAGE_TYPE_TEXT_LINK
                    else:
                        type = MESSAGE_TYPE_TEXT

                    self.publish_message(type, data.get('text', ''), attachments=data.get('attachments'))
                else:
                    self.send_notification(_('Enter not empty message'))

            elif data['type'] == PREFIX_CONNECT:
                self.connect(data['chat'])

            elif data['type'] == PREFIX_HISTORY:
                self.get_history(num=data.get('number'), docs_only=data.get('docs_only'))

        except Exception as e:
            self.send_notification(_('Error occurred while processing message'))
            return

    def on_close(self):
        try:
            self.redis_client.unsubscribe(self.channels)
        except AttributeError:
            pass
        self.redis_client.disconnect()

    def subscribe(self, channel):
        """
        Subscribe to chat channel
        """
        if channel not in self.channels:
            self.channels.append(channel)
        self.redis_client.subscribe(channel)

    def connect(self, channel):
        try:
            self.chat = Chat.objects.get(uid=channel)
        except Chat.DoesNotExist:
            self.send_notification(_('Chat not found'))
        except ValueError:
            self.send_notification(_('Connection error'))
        else:
            if not self.user.is_admin() and self.user not in self.chat.participans.all():
                self.send_notification(_('Access to chat denied'))
            self.channel = channel
            self.subscribe(channel)
            self.send_notification(_('You are connected to chat ') + u'"%s"' % self.chat.title)

            count = c.get('unread_{}_{}'.format(self.user_id, channel))
            if count:
                c.decrby('unread_total_{}'.format(self.user_id), count)
            c.set('unread_{}_{}'.format(self.user_id, channel), 0)

    def _close(self, chat_uid):
        pass

    def redis_message_callback(self, message):
        """
        Callback for redis client. Send message to participants.
        """
        if message.kind == 'message':
            data = json.loads(message.body)
            self.update_meta_send(data)

    def publish_message(self, type, text, attachments=None):
        """
        Publishing message to redis -> send to chat
        Message types:
            MESSAGE_TYPE_TEXT = 1
            MESSAGE_TYPE_PROPOSAL = 2
            MESSAGE_TYPE_PAYMENT = 3
            MESSAGE_TYPE_TEXT_LINKS = 4
            MESSAGE_TYPE_NOTIFICATION = 10
            MESSAGE_TYPE_START_TYPING = 11
            MESSAGE_TYPE_STOP_TYPING = 12
        """
        message = {
            'sender': self.user.pk,
            'type': type,
            'text': escape(text),
            'chat': self.channel,
            'date': format_date(timezone.now(), self.tz),
            'meta': {'new_message_count': None}
        }

        attachment_objects = None

        if attachments:
            try:
                attachment_objects = Attachment.objects.filter(uid__in=attachments)
            except:
                self.send_notification(_('Attachments have incorrect format'))
                return

            message['attachments'] = Attachment.as_dict(attachment_objects)

        c.publish(self.channel, json.dumps(message))

        m = Message.objects.create(sender=self.user, chat=self.chat, text=text, type=type)

        if attachment_objects:
            attachment_objects.update(message=m)

    def send_notification(self, text):
        """
        Send notification to client
        """
        notification = {
            'type': MESSAGE_TYPE_NOTIFICATION,
            'text': text,
            'date': format_date(timezone.now(), tz=self.tz)
        }
        self.send(notification)

    def get_history(self, num=None, docs_only=None):
        messages = self.chat.chat_messages.all()
        if docs_only:
            messages = messages.filter(type__in=[MESSAGE_TYPE_PROPOSAL, MESSAGE_TYPE_PAYMENT, MESSAGE_TYPE_TEXT_LINK])
        if num and num < messages.count():
            messages = messages[messages.count()-num:]
        for message in messages:
            self.update_meta_send(message.as_dict(self.tz))

    def update_meta_send(self, data):
        def set_unread(count):
            if data.get('meta') is not None:
                data['meta']['new_message_count'] = count
            self.send(data)

        if data.get('sender') != self.user_id and data.get('chat') != self.channel:
            c.incr('unread_{0}_{1}'.format(self.user_id, data.get('chat')), set_unread)
        else:
            self.send(data)
