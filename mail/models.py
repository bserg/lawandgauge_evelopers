from smtplib import SMTPException
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db import models
from django.template import Context
from django.template.loader import render_to_string
from lawandgauge.settings import DEFAULT_FROM_EMAIL, LANGUAGES, SITE_URL


### TAMPLATES ###

TEMPLATE_REGISTRATION_CONTRACTOR = 1
TEMPLATE_REGISTRATION_CLIENT = 2
TEMPLATE_INVITION_CONTRACTOR = 3
TEMPLATE_LANDING_BUILDER_REQUEST = 4

TEMPLATE_TYPE = (
    (TEMPLATE_REGISTRATION_CONTRACTOR, _('Contractor registration')),
    (TEMPLATE_REGISTRATION_CLIENT, _('Client registration')),
    (TEMPLATE_INVITION_CONTRACTOR, _('Contractor invition')),
    (TEMPLATE_LANDING_BUILDER_REQUEST, _('Landing builder request'))
)

class MailTemplate(models.Model):
    subject_ru = models.CharField(_('Subject') + ' (ru)', max_length=255)
    subject_en = models.CharField(_('Subject') + ' (en)', max_length=255)
    template_ru = models.CharField(_('Template') + ' (ru)', max_length=255,
                                   help_text=_('Related path to RU version of email template'))
    template_en = models.CharField(_('Template') + ' (en)', max_length=255,
                                   help_text=_('Related path to EN version of email template'))
    email_from = models.EmailField(_('From'), default=DEFAULT_FROM_EMAIL)
    type = models.PositiveSmallIntegerField(_('Type'), choices=TEMPLATE_TYPE)

    def __unicode__(self):
        return self.get_type_display()

    class Meta:
        verbose_name = _('Mail template')
        verbose_name_plural = _('Mail templates')


### MAIL OBJECTS ###

class Mail(models.Model):
    mail_template = models.ForeignKey(MailTemplate, verbose_name=_('Mail template'))
    recipient = models.ForeignKey('account.User', verbose_name=_('Recipient'), blank=True, null=True)
    email = models.EmailField(_('Email'), blank=True)
    language = models.CharField(_('Language'), max_length=10, choices=LANGUAGES, blank=True)
    date = models.DateTimeField(_('Date'), auto_now_add=True)
    sent_success = models.BooleanField(_('Sent successfully'), default=True)
    error_log = models.TextField(_('Error log'), blank=True)

    def _get_context(self, **kwargs):
        context = {}
        for field in self._meta.get_fields():
            if field.name in ['mail_template', 'language', 'date']:
                continue
            context.update({field.name: field})
        context.update(kwargs)
        return context

    def _get_language(self):
        return self.language or self.recipient.language or 'en'

    def _render_template(self, **kwargs):
        lang = self._get_language()
        template = getattr(self.mail_template, 'template_' + lang)
        if template:
            context = {'recipient': self.recipient, 'SITE_URL': SITE_URL}
            context.update(kwargs)
            return render_to_string(template, Context(context))

    def send(self, **kwargs):
        lang = self._get_language()
        try:
            send_mail(getattr(self.mail_template, 'subject_' + lang), '',
                      self.mail_template.email_from, [self.email or self.recipient.email],
                      html_message=self._render_template(**kwargs))
        except SMTPException as e:
            self.sent_success = False
            self.error_log = str(e)
            self.save(no_send=True)

    def save(self, no_send=False, **kwargs):
        if not no_send:
            self.send()
        return super(Mail, self).save(**kwargs)

    def __unicode__(self):
        return self.email

    class Meta:
        ordering = ['-date']
        verbose_name = _('Email')
        verbose_name_plural = _('Emails')




