from mail.models import MailTemplate, Mail


def _mail(type, recipient=None, email='', language=None, extra_context=None):
    mail_template = MailTemplate.objects.filter(type=type).first()
    if mail_template:
        if extra_context:
            mail = Mail(
                mail_template=mail_template,
                recipient=recipient,
                email=email
            )
            if language in ['ru', 'en']:
                mail.language = language
            mail.save(no_send=True)
            mail.send(**extra_context)
        else:
            mail = Mail.objects.create(
                mail_template=mail_template,
                recipient=recipient,
                email=email
            )