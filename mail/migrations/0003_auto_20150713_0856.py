# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0002_mail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mail',
            name='language',
            field=models.CharField(blank=True, max_length=10, verbose_name='Language', choices=[(b'ru', 'Russian'), (b'en', 'English')]),
        ),
        migrations.AlterField(
            model_name='mailtemplate',
            name='email_from',
            field=models.EmailField(default=b'no-reply@lawandgauge.com', max_length=254, verbose_name='From'),
        ),
        migrations.AlterField(
            model_name='mailtemplate',
            name='subject_en',
            field=models.CharField(max_length=255, verbose_name='Subject (en)'),
        ),
        migrations.AlterField(
            model_name='mailtemplate',
            name='subject_ru',
            field=models.CharField(max_length=255, verbose_name='Subject (ru)'),
        ),
        migrations.AlterField(
            model_name='mailtemplate',
            name='template_en',
            field=models.CharField(help_text='Related path to EN version of email template', max_length=255, verbose_name='Template (en)'),
        ),
        migrations.AlterField(
            model_name='mailtemplate',
            name='template_ru',
            field=models.CharField(help_text='Related path to RU version of email template', max_length=255, verbose_name='Template (ru)'),
        ),
    ]
