# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mail', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.CharField(default=b'default', max_length=10, verbose_name='Language', choices=[(b'default', 'Default'), (b'ru', 'Russian'), (b'en', 'English')])),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date')),
                ('sent_success', models.BooleanField(default=True, verbose_name='Sent successfully')),
                ('error_log', models.TextField(verbose_name='Error log', blank=True)),
                ('mail_template', models.ForeignKey(verbose_name='Mail template', to='mail.MailTemplate')),
                ('recipient', models.ForeignKey(verbose_name='Recipient', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-date'],
                'verbose_name': 'Email',
                'verbose_name_plural': 'Emails',
            },
        ),
    ]
