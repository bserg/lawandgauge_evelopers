# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MailTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject_ru', models.CharField(max_length=255, verbose_name='Name(ru)')),
                ('subject_en', models.CharField(max_length=255, verbose_name='Name(en)')),
                ('template_ru', models.CharField(help_text='Related path to RU version of email template', max_length=255, verbose_name='Template(ru)')),
                ('template_en', models.CharField(help_text='Related path to EN version of email template', max_length=255, verbose_name='Template(en)')),
                ('email_from', models.EmailField(default=b'no-reply@localhost', max_length=254, verbose_name='From')),
                ('type', models.PositiveSmallIntegerField(verbose_name='Type', choices=[(1, 'Contractor registration'), (2, 'Client registration'), (3, 'Contractor invition'), (4, 'Landing builder request')])),
            ],
            options={
                'verbose_name': 'Mail template',
                'verbose_name_plural': 'Mail templates',
            },
        ),
    ]
