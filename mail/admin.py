from django.contrib import admin
from django.utils.translation import ugettext as _
from mail.models import MailTemplate, Mail

class MailTemplateAdmin(admin.ModelAdmin):
    list_display = ['type', 'subject_ru', 'email_from']

admin.site.register(MailTemplate, MailTemplateAdmin)

class MailAdmin(admin.ModelAdmin):
    search_fields = ['recipient__first_name', 'recipient__last_name', 'recipient__email']
    list_filter = ['mail_template']
    list_display = ['mail_template', 'recipient', '_language', 'date', 'sent_success']

    def _language(self, obj):
        lang = obj.language or obj.recipient.language or 'en'
        return lang + _(' (by recipient)')
    _language.short_description = _('Language')

admin.site.register(Mail, MailAdmin)