import uuid
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext as _
from django.db import models
from account.models import User


class Balance(models.Model):
    uid = models.CharField(_('UID'), max_length=32, default=uuid.uuid4().hex, editable=False)
    user = models.OneToOneField(User, verbose_name=_('User'), related_name='balance')

    def __unicode__(self):
        return self.uid

    class Meta:
        verbose_name = _('Balance')
        verbose_name_plural = _('Balances')


class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4(), editable=False)
    balance = models.ForeignKey(Balance, verbose_name=_('Balance'), related_name='balance_events')
    date = models.DateTimeField(_('Date'), auto_now_add=True)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name = _('Balance transaction')
        verbose_name_plural = _('Balance transactions')


@receiver(post_save, sender=User)
def create_balance(sender, instance, created, **kwargs):
    if created:
        Balance.objects.create(user=instance)