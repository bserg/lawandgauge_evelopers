Django==1.8
MySQL-python==1.2.5
argparse==1.2.1
backports.ssl-match-hostname==3.4.0.2
-e git://github.com/evilkost/brukva.git#egg=brukva
certifi==14.5.14
django-grappelli==2.6.4
django-redis-sessions==0.4.0
gunicorn==19.3.0
redis==2.10.3
sockjs-tornado==1.0.1
tornado==4.1
wsgiref==0.1.2
Pillow==2.8.1
sorl-thumbnail==12.2
pytz==2015.2
requests==2.7.0
facebook-sdk==0.4.0
django-mptt==0.7.3
urllib3==1.10.4
django-colorfield==0.1.4
django-polymorphic==0.7.1


