from django.utils.translation import activate


class LocalizationMiddleware(object):
    def process_request(self, request):
        if 'lang' in request.session['lang']:
            lang = request.session['lang']
            activate(lang)
