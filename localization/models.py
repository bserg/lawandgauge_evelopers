from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext as _, get_language


class Locale(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    code = models.CharField(_('Code'), max_length=2, help_text=_('For example "ru", "de", etc.'))
    is_default = models.BooleanField(_('Is default'), default=False)
    active = models.BooleanField(_('Active'), default=True)

    def get_name(self):
        return _(self.name)

    def save(self, *args, **kwargs):
        super(Locale, self).save(*args, **kwargs)
        if self.is_default:
            Locale.objects.exclude(pk=self.pk).update(is_default=False)

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = _('Locale')
        verbose_name_plural = _('Locales')


class Translation(models.Model):
    value = models.TextField(_('Translation'), blank=True)
    field = models.CharField(_('Field'), max_length=255, default='name')
    locale = models.ForeignKey(Locale, verbose_name=_('Locale'))

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return u'{} ({})'.format(self.value, self.locale.code)

    class Meta:
        verbose_name = _('Translation')
        verbose_name_plural = _('Translations')


class TranslatedModelMixin(models.Model):
    translations = GenericRelation(Translation, related_query_name='translations')

    class Meta:
        abstract = True

    class TranslationMeta:
        translated_fields = []

    def __init__(self, *args, **kwargs):
        super(TranslatedModelMixin, self).__init__(*args, **kwargs)
        for field in self.TranslationMeta.translated_fields:
            for code in Locale.objects.filter(active=True).values_list('code', flat=True):
                setattr(self, '{field}_{lang}'.format(field=field, lang=code), self.get_field_translation(field, code))

    def get_field_translation(self, field, code):
        if Locale.objects.filter(code=code, is_default=True).exists():
            if hasattr(self, field):
                return getattr(self, field)
            else:
                return ''
        try:
            tr = self.translations.get(field=field, locale__code=code, locale__active=True)
        except:
            return None
        else:
            return tr.value

    def save(self, *args, **kwargs):
        super(TranslatedModelMixin, self).save(*args, **kwargs)
        self.prepare_translations()

    def prepare_translations(self, code=None):
        """
        Prepare instance of Translation model for object
        """
        for field in self.TranslationMeta.translated_fields:
            if code is not None and not self.translations.filter(field=field, locale__code=code).exists():
                Translation.objects.create(content_object=self, field=field, locale__code=code)
            else:
                for locale in Locale.objects.all():
                    if not self.translations.filter(field=field, locale=locale).exists():
                        Translation.objects.create(content_object=self, field=field, locale=locale)


class TranslatedText(models.Model):
    slug = models.SlugField(_('Slug'), blank=True)
    text_en = models.TextField(_('Text') + ' (en)', blank=True)
    text_ru = models.TextField(_('Text') + ' (ru)', blank=True)

    def as_dict(self, use_slug=False):

        if use_slug:
            return {self.slug: {'en': self.text_en, 'ru': self.text_ru}}
        else:
            return {self.text_en: self.text_ru}

    def __unicode__(self):
        return getattr(self, 'text_' + get_language())

    class Meta:
        verbose_name = _('Translated text')
        verbose_name_plural = _('Translated texts')

