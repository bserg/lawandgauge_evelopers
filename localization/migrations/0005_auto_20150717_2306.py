# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.contenttypes.models import ContentType

from django.db import models, migrations
from localization.models import Translation


def translated_to_field(app, schema_editor):
    db_alias = schema_editor.connection.alias
    _models = ['TranslatedText']
    for model_name in _models:
        model = app.get_model('localization', model_name)
        ct = ContentType.objects.get_for_model(model)
        for item in model.objects.using(db_alias).all():
            translation = Translation.objects.using(db_alias).filter(content_type=ct, object_id=item.pk, locale__code='ru').first()
            print translation
            if translation:
                item.text_ru = translation.value
                item.save()


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0004_auto_20150525_0751'),
    ]

    operations = [
        migrations.RenameField(
            model_name='translatedtext',
            old_name='text',
            new_name='text_en'
        ),
        migrations.AlterField(
            model_name='translatedtext',
            name='text_en',
            field=models.TextField(verbose_name='Text', blank=True),
        ),
        migrations.AddField(
            model_name='translatedtext',
            name='text_ru',
            field=models.TextField(verbose_name='Text', blank=True),
        ),
        migrations.RunPython(translated_to_field, reverse_code=migrations.RunPython.noop)
    ]
