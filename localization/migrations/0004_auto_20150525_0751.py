# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0003_translatedtext'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locale',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='locale',
            name='code',
            field=models.CharField(help_text='For example "ru", "de", etc.', max_length=2, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='locale',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='translatedtext',
            name='text',
            field=models.TextField(verbose_name='Text'),
        ),
    ]
