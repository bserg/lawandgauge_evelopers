from django.core.management.base import NoArgsCommand, BaseCommand
from django.db.models.loading import get_model
from lawandgauge.settings import TRANSLATION_MODELS


class Command(BaseCommand):

    def handle(self, *args, **options):
        for model_string in TRANSLATION_MODELS:
            m = get_model(app_label=model_string.split('.')[0], model_name=model_string.split('.')[-1])
            for o in m.objects.all():
                o.prepare_translations()