from django.conf.urls import patterns, url

from localization.views import LanguageView, TranslatedTextsView

urlpatterns = patterns('',
    url(r'^lang/$', LanguageView.as_view(), name='language'),
    url(r'^translations/$', TranslatedTextsView.as_view(), name='translations'),
)
