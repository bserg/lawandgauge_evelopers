from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline
from django.contrib.contenttypes.forms import BaseGenericInlineFormSet
from django.utils.translation import ugettext as _
from django import forms

from localization.models import Locale, Translation, TranslatedText


class LocaleAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_default', 'active')

# admin.site.register(Locale, LocaleAdmin)

class TranslationAdminForm(forms.ModelForm):
    class Meta:
        model = Translation
        widgets = {
            'value': forms.TextInput(attrs={'style': 'width:200px'})
        }
        exclude = []


class TranslationAdmin(admin.ModelAdmin):
    form = TranslationAdminForm
    list_display = ('get_field_value', 'value', 'content_type',  'locale')
    list_editable = ['value']
    list_filter = ['locale']
    search_fields = ('content_object__', 'value')

    def get_queryset(self, request):
        return Translation.objects.filter(locale__active=True).exclude(locale__is_default=True)

    def get_changelist_form(self, request, **kwargs):
        return TranslationAdminForm

    def get_field_value(self, obj):
        if hasattr(obj.content_object, obj.field):
            return getattr(obj.content_object, obj.field)
        else:
            return 'Wrong "field" parameter'

    def get_search_field(self, obj):
        return 'content_object__' + obj.field

    get_field_value.short_description = _('Text')

# admin.site.register(Translation, TranslationAdmin)

class TranslationInlineFormSet(BaseGenericInlineFormSet):
    def __init__(self, *args, **kwargs):
        print args, kwargs
        super(TranslationInlineFormSet, self).__init__(*args, **kwargs)

class TranslationInline(GenericStackedInline):
    model = Translation
    extra = 0

    def get_queryset(self, request):
        return Translation.objects.filter(locale__active=True).exclude(locale__is_default=True)

class TranslatedTextAdmin(admin.ModelAdmin):
    list_display = ('text_en', 'text_ru', 'slug')
    search_fields = ('text_en', 'text_ru', 'slug')
    prepopulated_fields = {'slug': ['text_en']}

admin.site.register(TranslatedText, TranslatedTextAdmin)
