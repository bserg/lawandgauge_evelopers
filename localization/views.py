from django.conf.global_settings import LANGUAGE_COOKIE_NAME
from django.views.generic import View
from django.utils.translation import ugettext  as _, activate, LANGUAGE_SESSION_KEY, override, get_language

from lawandgauge.mixins import JsonResponseMixin
from localization.models import Locale, TranslatedText


class LanguageView(JsonResponseMixin, View):
    def get(self, request):
        return self.json_success(data=get_language())

    def post(self, request):
        lang = request.POST.get('lang')
        if lang not in ['en', 'ru']:    
            return self.json_fail(error=_('This language is unavailable'))

        if request.user.is_authenticated():
            request.user.language = lang
            request.user.save()

        response = self.json_success(msg=_('Language changed successfully'))
        if hasattr(request, 'session'):
            request.session[LANGUAGE_SESSION_KEY] = lang
        else:
            response.set_cookie(LANGUAGE_COOKIE_NAME, lang)

        return response


class TranslatedTextsView(JsonResponseMixin, View):
    def get(self, request):
        use_slug = request.GET.get('use_slug')
        if use_slug:
            data = {t.slug: {'en': t.text_en, 'ru': t.text_ru} for t in TranslatedText.objects.filter(slug__isnull=False)}
        else:
            data = {t.text_en: t.text_ru for t in TranslatedText.objects.filter(slug__isnull=False)}
        return self.json_success(data=data)
