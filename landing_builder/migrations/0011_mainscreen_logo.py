# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0010_screen_foreground_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='mainscreen',
            name='logo',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='Logo', choices=[(1, 'Color logo'), (2, 'White logo'), (3, 'Without logo')]),
        ),
    ]
