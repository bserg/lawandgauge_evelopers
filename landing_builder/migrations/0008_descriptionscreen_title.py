# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0007_auto_20150612_1659'),
    ]

    operations = [
        migrations.AddField(
            model_name='descriptionscreen',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Title', blank=True),
        ),
    ]
