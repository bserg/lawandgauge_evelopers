# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0006_deletedlandings'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='deletedlandings',
            options={'verbose_name': 'Deleted landing', 'verbose_name_plural': 'Deleted landings'},
        ),
        migrations.AddField(
            model_name='mainscreen',
            name='button_text',
            field=models.CharField(max_length=100, verbose_name='Button text', blank=True),
        ),
    ]
