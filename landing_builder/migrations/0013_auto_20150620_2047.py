# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0012_auto_20150619_1314'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactsScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('title', models.CharField(max_length=255, verbose_name='Title', blank=True)),
            ],
            options={
                'verbose_name': 'Offices screen',
                'verbose_name_plural': 'Offices screens',
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('address', models.CharField(max_length=255, verbose_name='Address')),
                ('phone', models.CharField(max_length=100, verbose_name='Phone')),
                ('lat', models.FloatField(default=0, verbose_name='Latitude')),
                ('lng', models.FloatField(default=0, verbose_name='Longitude')),
            ],
            options={
                'verbose_name': 'Office',
                'verbose_name_plural': 'Offices',
            },
        ),
        migrations.AlterField(
            model_name='screen',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, editable=False, choices=[(0, 'Main screen'), (1, 'Description screen'), (2, 'Advantages screen'), (3, 'Feedbacks screen'), (4, 'Map screen'), (5, 'SignUp screen'), (6, 'Other landings screen'), (7, 'Partners screen'), (8, 'Contacts screen')]),
        ),
        migrations.AddField(
            model_name='contactsscreen',
            name='offices',
            field=models.ManyToManyField(to='landing_builder.Office', verbose_name='Offices'),
        ),
    ]
