# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0019_landing_footer_faq_items'),
    ]

    operations = [
        migrations.AlterField(
            model_name='packageitem',
            name='price',
            field=models.FloatField(null=True, verbose_name='Price', blank=True),
        ),
    ]
