# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='screen',
            name='type',
            field=models.PositiveSmallIntegerField(null=True, editable=False, blank=True),
        ),
    ]
