# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0004_auto_20150601_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='landing',
            name='main_color',
            field=colorfield.fields.ColorField(default=b'438EB4', max_length=10, verbose_name='Main color'),
        ),
        migrations.AlterField(
            model_name='screen',
            name='landing',
            field=models.ForeignKey(related_name='screens', verbose_name='Landing', to='landing_builder.Landing', null=True),
        ),
    ]
