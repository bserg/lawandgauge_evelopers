# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0014_auto_20150620_2111'),
    ]

    operations = [
        migrations.AddField(
            model_name='landing',
            name='preview',
            field=models.ImageField(upload_to=b'landing_builder', verbose_name='Preview', blank=True),
        ),
        migrations.AlterField(
            model_name='screen',
            name='background',
            field=models.ImageField(upload_to=b'landing_builder', verbose_name='Background', blank=True),
        ),
    ]
