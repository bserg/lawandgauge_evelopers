# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0016_auto_20150717_1756'),
    ]

    operations = [
        migrations.RenameField(
            model_name='advantage',
            old_name='description',
            new_name='description_ru',
        ),
        migrations.RenameField(
            model_name='advantage',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='contactsscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='descriptionscreen',
            old_name='text',
            new_name='text_ru',
        ),
        migrations.RenameField(
            model_name='descriptionscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='faqscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='feedback',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='feedback',
            old_name='text',
            new_name='text_ru',
        ),
        migrations.RenameField(
            model_name='feedbackscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='landing',
            old_name='description',
            new_name='description_ru',
        ),
        migrations.RenameField(
            model_name='landing',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='mainscreen',
            old_name='button_text',
            new_name='button_text_ru',
        ),
        migrations.RenameField(
            model_name='mainscreen',
            old_name='description',
            new_name='description_ru',
        ),
        migrations.RenameField(
            model_name='mainscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='office',
            old_name='address',
            new_name='address_ru',
        ),
        migrations.RenameField(
            model_name='office',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='package',
            old_name='description',
            new_name='description_ru',
        ),
        migrations.RenameField(
            model_name='package',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='packageitem',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='partner',
            old_name='name',
            new_name='name_ru',
        ),
        migrations.RenameField(
            model_name='partnersscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='pricesscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.RenameField(
            model_name='signupscreen',
            old_name='title',
            new_name='title_ru',
        ),
        migrations.AddField(
            model_name='advantage',
            name='description_en',
            field=models.TextField(verbose_name='Description (en)', blank=True),
        ),
        migrations.AddField(
            model_name='advantage',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='contactsscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
        migrations.AddField(
            model_name='descriptionscreen',
            name='text_en',
            field=models.TextField(verbose_name='Text (en)', blank=True),
        ),
        migrations.AddField(
            model_name='descriptionscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
        migrations.AddField(
            model_name='faqscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
        migrations.AddField(
            model_name='feedback',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='feedback',
            name='text_en',
            field=models.TextField(verbose_name='Text (en)', blank=True),
        ),
        migrations.AddField(
            model_name='feedbackscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
        migrations.AddField(
            model_name='landing',
            name='description_en',
            field=models.TextField(verbose_name='Description (en)', blank=True),
        ),
        migrations.AddField(
            model_name='landing',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='mainscreen',
            name='button_text_en',
            field=models.CharField(max_length=100, verbose_name='Button text (en)', blank=True),
        ),
        migrations.AddField(
            model_name='mainscreen',
            name='description_en',
            field=models.TextField(verbose_name='Description (en)', blank=True),
        ),
        migrations.AddField(
            model_name='mainscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
        migrations.AddField(
            model_name='office',
            name='address_en',
            field=models.CharField(max_length=255, verbose_name='Address (en)', blank=True),
        ),
        migrations.AddField(
            model_name='office',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='package',
            name='description_en',
            field=models.CharField(max_length=255, verbose_name='Description (en)', blank=True),
        ),
        migrations.AddField(
            model_name='package',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='packageitem',
            name='name_en',
            field=models.CharField(max_length=244, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='name_en',
            field=models.CharField(max_length=255, verbose_name='Name (en)', blank=True),
        ),
        migrations.AddField(
            model_name='partnersscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
        migrations.AddField(
            model_name='pricesscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AddField(
            model_name='signupscreen',
            name='title_en',
            field=models.CharField(max_length=255, verbose_name='Title (en)', blank=True),
        ),
    ]
