# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0009_auto_20150618_2124'),
    ]

    operations = [
        migrations.AddField(
            model_name='screen',
            name='foreground_color',
            field=colorfield.fields.ColorField(default=b'17243F', max_length=10, verbose_name='Foreground color'),
        ),
    ]
