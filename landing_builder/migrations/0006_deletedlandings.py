# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0005_auto_20150603_2145'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeletedLandings',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('landing_builder.landing',),
        ),
    ]
