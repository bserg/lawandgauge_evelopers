# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0011_mainscreen_logo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('logo', models.ImageField(upload_to=b'landings', verbose_name='Logo')),
                ('url', models.URLField(verbose_name='URL')),
            ],
            options={
                'verbose_name': 'Partner',
                'verbose_name_plural': 'Partners',
            },
        ),
        migrations.CreateModel(
            name='PartnersScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('title', models.CharField(max_length=255, verbose_name='Title', blank=True)),
                ('partners', models.ManyToManyField(to='landing_builder.Partner', verbose_name='Partners')),
            ],
            options={
                'verbose_name': 'Partners screen',
                'verbose_name_plural': 'Partners screens',
            },
            bases=('landing_builder.screen',),
        ),
        migrations.AlterField(
            model_name='screen',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, editable=False, choices=[(0, 'Main screen'), (1, 'Description screen'), (2, 'Advantages screen'), (3, 'Feedbacks screen'), (4, 'Map screen'), (5, 'SignUp screen'), (6, 'Other landings screen'), (7, 'Partners screen')]),
        ),
    ]
