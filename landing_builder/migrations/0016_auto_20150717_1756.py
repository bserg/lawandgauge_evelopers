# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0015_auto_20150717_1716'),
    ]

    operations = [
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('description', models.CharField(max_length=255, verbose_name='Description', blank=True)),
                ('image', models.FileField(upload_to=b'landing_builder', verbose_name='Image', blank=True)),
                ('link', models.CharField(max_length=255, verbose_name='URL', blank=True)),
            ],
            options={
                'verbose_name': 'Package',
                'verbose_name_plural': 'Packages',
            },
        ),
        migrations.CreateModel(
            name='PackageItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=244, verbose_name='Name')),
                ('price', models.FloatField(default=0, verbose_name='Price')),
                ('price_template', models.CharField(help_text='For example "$(price)", "(price)rub", etc.', max_length=100, verbose_name='Price template', blank=True)),
                ('package', models.ForeignKey(related_name='items', verbose_name='Package', to='landing_builder.Package')),
            ],
            options={
                'verbose_name': 'Package item',
                'verbose_name_plural': 'Package items',
            },
        ),
        migrations.CreateModel(
            name='PricesScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('title', models.CharField(max_length=255, verbose_name='Title', blank=True)),
                ('packages', models.ManyToManyField(to='landing_builder.Package', verbose_name='Packages')),
            ],
            options={
                'verbose_name': 'Prices screen',
                'verbose_name_plural': 'Prices screens',
            },
            bases=('landing_builder.screen',),
        ),
        migrations.AlterField(
            model_name='screen',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, editable=False, choices=[(0, 'Main screen'), (1, 'Description screen'), (2, 'Advantages screen'), (3, 'Feedbacks screen'), (4, 'Map screen'), (5, 'SignUp screen'), (6, 'Other landings screen'), (7, 'Partners screen'), (8, 'Contacts screen'), (9, 'FAQ Screen'), (10, 'Prices screen')]),
        ),
    ]
