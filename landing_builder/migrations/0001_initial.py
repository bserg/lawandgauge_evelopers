# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Advantage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('img', models.ImageField(upload_to=b'landings/anvantage', verbose_name='Image', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('office', models.CharField(max_length=255, verbose_name='Office')),
                ('img', models.ImageField(upload_to=b'landings/photo', verbose_name='Photo', blank=True)),
                ('text', models.TextField(verbose_name='Text', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Landing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('slug', models.SlugField(verbose_name='Slug')),
                ('main_color', colorfield.fields.ColorField(max_length=10, verbose_name='Main color')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
            ],
            options={
                'verbose_name': 'Landing',
                'verbose_name_plural': 'Landings',
            },
        ),
        migrations.CreateModel(
            name='Screen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('background', models.ImageField(upload_to=b'landings/bg', verbose_name='Background', blank=True)),
                ('order', models.PositiveSmallIntegerField(default=0, verbose_name='Ordering')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('template', models.CharField(max_length=255, editable=False, blank=True)),
            ],
            options={
                'verbose_name': 'Screen',
                'verbose_name_plural': 'Screens',
            },
        ),
        migrations.CreateModel(
            name='AdvantagesScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('advantages', models.ManyToManyField(to='landing_builder.Advantage', verbose_name='Advantages')),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='DescriptionScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('img', models.ImageField(upload_to=b'landings', verbose_name='Image', blank=True)),
                ('text', models.TextField(verbose_name='Text')),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='FeedbackScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('feedbacks', models.ManyToManyField(to='landing_builder.Feedback', verbose_name='Feedbacks')),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='MainScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='MapScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='OtherLangingsScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.CreateModel(
            name='SignupScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
            ],
            options={
                'abstract': False,
            },
            bases=('landing_builder.screen',),
        ),
        migrations.AddField(
            model_name='screen',
            name='landing',
            field=models.ForeignKey(related_name='screens', verbose_name='Landing', to='landing_builder.Landing'),
        ),
        migrations.AddField(
            model_name='screen',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='polymorphic_landing_builder.screen_set+', editable=False, to='contenttypes.ContentType', null=True),
        ),
    ]
