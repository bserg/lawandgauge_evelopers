# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0017_auto_20150723_1821'),
    ]

    operations = [
        migrations.CreateModel(
            name='FooterItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name_ru', models.CharField(max_length=255, verbose_name='Name (ru)', blank=True)),
                ('name_en', models.CharField(max_length=255, verbose_name='Name (en)', blank=True)),
                ('url', models.CharField(max_length=255, verbose_name='URL', blank=True)),
            ],
            options={
                'verbose_name': 'Footer item',
                'verbose_name_plural': 'Footer items',
            },
        ),
        migrations.AlterField(
            model_name='advantage',
            name='description_ru',
            field=models.TextField(verbose_name='Description (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='advantage',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='contactsscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='descriptionscreen',
            name='text_ru',
            field=models.TextField(verbose_name='Text (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='descriptionscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='faqscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='feedback',
            name='text_ru',
            field=models.TextField(verbose_name='Text (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='feedbackscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='landing',
            name='description_ru',
            field=models.TextField(verbose_name='Description (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='landing',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='mainscreen',
            name='button_text_ru',
            field=models.CharField(max_length=100, verbose_name='Button text (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='mainscreen',
            name='description_ru',
            field=models.TextField(verbose_name='Description (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='mainscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='address_ru',
            field=models.CharField(max_length=255, verbose_name='Address (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='office',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='package',
            name='description_ru',
            field=models.CharField(max_length=255, verbose_name='Description (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='package',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='packageitem',
            name='name_ru',
            field=models.CharField(max_length=244, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='name_ru',
            field=models.CharField(max_length=255, verbose_name='Name (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='partnersscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='pricesscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AlterField(
            model_name='signupscreen',
            name='title_ru',
            field=models.CharField(max_length=255, verbose_name='Title (ru)', blank=True),
        ),
        migrations.AddField(
            model_name='landing',
            name='footer_items',
            field=models.ManyToManyField(to='landing_builder.FooterItem', verbose_name='Footer items', blank=True),
        ),
    ]
