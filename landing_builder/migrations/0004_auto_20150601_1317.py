# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import colorfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0003_auto_20150531_0511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='landing',
            name='main_color',
            field=colorfield.fields.ColorField(default=b'#438EB4', max_length=10, verbose_name='Main color'),
        ),
    ]
