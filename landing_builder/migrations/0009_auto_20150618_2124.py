# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0008_descriptionscreen_title'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='screen',
            options={'ordering': ['order'], 'verbose_name': 'Screen', 'verbose_name_plural': 'Screens'},
        ),
        migrations.AddField(
            model_name='feedbackscreen',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Title', blank=True),
        ),
        migrations.AddField(
            model_name='signupscreen',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Title', blank=True),
        ),
    ]
