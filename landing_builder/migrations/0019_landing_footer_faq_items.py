# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0003_auto_20150724_1503'),
        ('landing_builder', '0018_auto_20150724_1437'),
    ]

    operations = [
        migrations.AddField(
            model_name='landing',
            name='footer_faq_items',
            field=models.ManyToManyField(to='faq.FaqItem', verbose_name='Faq items', blank=True),
        ),
    ]
