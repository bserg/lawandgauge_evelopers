# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0001_initial'),
        ('landing_builder', '0013_auto_20150620_2047'),
    ]

    operations = [
        migrations.CreateModel(
            name='FaqScreen',
            fields=[
                ('screen_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='landing_builder.Screen')),
                ('title', models.CharField(max_length=255, verbose_name='Title', blank=True)),
                ('items', models.ManyToManyField(to='faq.FaqItem', verbose_name='FAQ items')),
            ],
            options={
                'verbose_name': 'FAQ screen',
                'verbose_name_plural': 'FAQ screens',
            },
            bases=('landing_builder.screen',),
        ),
        migrations.AlterModelOptions(
            name='contactsscreen',
            options={'verbose_name': 'Contacts screen', 'verbose_name_plural': 'Contacts screens'},
        ),
        migrations.AlterField(
            model_name='screen',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, editable=False, choices=[(0, 'Main screen'), (1, 'Description screen'), (2, 'Advantages screen'), (3, 'Feedbacks screen'), (4, 'Map screen'), (5, 'SignUp screen'), (6, 'Other landings screen'), (7, 'Partners screen'), (8, 'Contacts screen'), (9, 'FAQ Screen')]),
        ),
    ]
