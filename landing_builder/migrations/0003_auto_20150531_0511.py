# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_builder', '0002_screen_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advantage',
            options={'verbose_name': 'Advantage', 'verbose_name_plural': 'Advantages'},
        ),
        migrations.AlterModelOptions(
            name='advantagesscreen',
            options={'verbose_name': 'Advantages screen', 'verbose_name_plural': 'Advantages screens'},
        ),
        migrations.AlterModelOptions(
            name='descriptionscreen',
            options={'verbose_name': 'Description screen', 'verbose_name_plural': 'Description screens'},
        ),
        migrations.AlterModelOptions(
            name='feedback',
            options={'verbose_name': 'Feedback', 'verbose_name_plural': 'Feedbacks'},
        ),
        migrations.AlterModelOptions(
            name='feedbackscreen',
            options={'verbose_name': 'Feedback screen', 'verbose_name_plural': 'Feedback screens'},
        ),
        migrations.AlterModelOptions(
            name='mainscreen',
            options={'verbose_name': 'Main screen', 'verbose_name_plural': 'Main screens'},
        ),
        migrations.AlterModelOptions(
            name='mapscreen',
            options={'verbose_name': 'Map screen', 'verbose_name_plural': 'Map screens'},
        ),
        migrations.AlterModelOptions(
            name='otherlangingsscreen',
            options={'verbose_name': 'Other landings screen', 'verbose_name_plural': 'Other landings screens'},
        ),
        migrations.AlterModelOptions(
            name='signupscreen',
            options={'verbose_name': 'Signup screen', 'verbose_name_plural': 'Signup screens'},
        ),
        migrations.AlterField(
            model_name='advantage',
            name='img',
            field=models.FileField(upload_to=b'landings/anvantage', verbose_name='Image', blank=True),
        ),
        migrations.AlterField(
            model_name='screen',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, editable=False, choices=[(0, 'Main screen'), (1, 'Description screen'), (2, 'Advantages screen'), (3, 'Feedbacks screen'), (4, 'Map screen'), (5, 'SignUp screen'), (6, 'Other landings screen')]),
        ),
    ]
