from django.core.mail import send_mail, mail_managers
from django.template import Context
from django.template.loader import render_to_string
from django.views.generic import FormView
from django.views.generic.detail import DetailView
from landing_builder.forms import LandingRegistrationForm

from landing_builder.models import Landing
from lawandgauge.mixins import JsonResponseMixin
from django.utils.translation import ugettext as _
from lawandgauge.settings import DEFAULT_FROM_EMAIL
from mail.models import MailTemplate, TEMPLATE_LANDING_BUILDER_REQUEST, Mail
from mail.utils import _mail


class LandingBuilderView(DetailView):
    template_name = 'landing_builder/base_landing.html'
    queryset = Landing.objects.filter(active=True)
    context_object_name = 'landing'

    def get_context_data(self, **kwargs):
        context = super(LandingBuilderView, self).get_context_data(**kwargs)
        context.update({
            'color': self.object.main_color,
            'screens': self.object.screens.all()
        })
        return context


class LandingRegistration(JsonResponseMixin, FormView):
    form_class = LandingRegistrationForm

    def form_valid(self, form):
        data = form.cleaned_data
        landing = data['landing']
        first_name = data['first_name']
        last_name = data['last_name']
        email = data['email']
        phone = data['phone']
        html = render_to_string('email/landing_builder_email.html', Context({
            'landing': landing,
            'first_name': first_name,
            'last_name': last_name,
            'email': email,
            'phone': phone
        }))
        mail_managers(_('New job request'), '', html_message=html, fail_silently=True)

        _mail(TEMPLATE_LANDING_BUILDER_REQUEST, email=email, language='ru', extra_context={'name': first_name})

        return self.json_success()

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)