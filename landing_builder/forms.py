from django import forms
from landing_builder.models import Landing


class LandingRegistrationForm(forms.Form):
    landing = forms.ModelChoiceField(Landing.objects.filter(active=True))
    first_name = forms.CharField(max_length=255)
    last_name = forms.CharField(max_length=255)
    email = forms.EmailField()
    phone = forms.CharField(max_length=100)
