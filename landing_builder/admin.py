import uuid

from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.forms import ModelForm

from landing_builder.models import Landing, Screen, MainScreen, DescriptionScreen, AdvantagesScreen, FeedbackScreen, \
    MapScreen, SignupScreen, OtherLangingsScreen, Advantage, Feedback, DeletedLandings, PartnersScreen, Partner, \
    ContactsScreen, Office, FaqScreen, PricesScreen, Package, PackageItem, FooterItem


class AlwaysChangedModelForm(ModelForm):
    def has_changed(self):
        return True


class DeleteNotAllowedModelAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False


class ScreenAdmin(admin.ModelAdmin):
    list_display = ['landing__name', 'order', 'active']

class ScreenInline(admin.TabularInline):
    model = Screen
    extra = 0
    sortable_field_name = "order"
    fields = ['name', 'order', 'active']
    readonly_fields = ['name']
    ordering = ['order']

    def name(self, obj):
        return obj._meta.verbose_name

class MainScreenInline(admin.StackedInline):
    model = MainScreen
    exclude = ['order']
    extra = 0

class DescriptionScreenInline(admin.StackedInline):
    model = DescriptionScreen
    exclude = ['order']
    extra = 0

class AdvantagesScreenInline(admin.StackedInline):
    model = AdvantagesScreen
    exclude = ['order']
    extra = 0

class FeedbackScreenInline(admin.StackedInline):
    model = FeedbackScreen
    exclude = ['order']
    extra = 0

class MapScreenInline(admin.StackedInline):
    model = MapScreen
    exclude = ['order']
    extra = 0

class SignupScreenInline(admin.StackedInline):
    model = SignupScreen
    exclude = ['order']
    extra = 0

class OtherLangingsScreenInline(admin.StackedInline):
    model = OtherLangingsScreen
    exclude = ['order']
    extra = 0

class PartnersScreenInline(admin.StackedInline):
    model = PartnersScreen
    exclude = ['order']
    extra = 0

class ContactsScreenInline(admin.StackedInline):
    model = ContactsScreen
    exclude = ['order']
    extra = 0

class FaqScreenInline(admin.StackedInline):
    model = FaqScreen
    exclude = ['order']
    extra = 0

class PricesScreenInline(admin.StackedInline):
    model = PricesScreen
    exclude = ['order']
    extra = 0

class LandingAdmin(DeleteNotAllowedModelAdmin):
    inlines = [
        ScreenInline,
        MainScreenInline,
        DescriptionScreenInline,
        AdvantagesScreenInline,
        FeedbackScreenInline,
        MapScreenInline,
        SignupScreenInline,
        OtherLangingsScreenInline,
        PartnersScreenInline,
        ContactsScreenInline,
        FaqScreenInline,
        PricesScreenInline,
    ]
    list_display = ['_preview', 'name', 'slug', 'main_color', 'active']
    list_editable = ['active']
    filter_horizontal = ['footer_items', 'footer_faq_items']
    actions = ['clone']

    def _preview(self, obj):
        if obj.preview:
            return mark_safe('<img src="%s">' % obj.get_preview(100, 100))
        return mark_safe('<img src="http://placehold.it/100x100">')

    _preview.short_description = _('Preview')

    def get_queryset(self, request):
        return super(LandingAdmin, self).get_queryset(request).filter(active=True)

    def clone(self, request, queryset):
        for index, landing in enumerate(queryset):
            screens = landing.screens.all()
            landing.pk = None
            landing.slug = str(uuid.uuid4())
            landing.name = landing.name + u'_clone'
            landing.save()
            for screen in screens:
                screen.pk = None
                screen.id = None
                screen.landing = landing
                screen.save()

    clone.short_description = _('Clone selected landings')

admin.site.register(Screen)

class MainScreenAdmin(admin.ModelAdmin):
    list_display = ['title', 'landing', 'order', 'active']

admin.site.register(MainScreen, MainScreenAdmin)
admin.site.register(DescriptionScreen)
admin.site.register(Landing, LandingAdmin)

class AdvantageScreenAdmin(admin.ModelAdmin):
    filter_horizontal = ['advantages']

admin.site.register(Advantage)
admin.site.register(AdvantagesScreen, AdvantageScreenAdmin)

class FeedbackScreenAdmin(admin.ModelAdmin):
    filter_horizontal = ['feedbacks']

class FeebackAdmin(admin.ModelAdmin):
    list_display = ['name', 'office', 'text']

class PartnersScreenAdmin(admin.ModelAdmin):
    filter_horizontal = ['partners']

class PartnerAdmin(admin.ModelAdmin):
    list_display = ['preview', 'name_ru', 'name_en', 'url']

    def preview(self, obj):
        return mark_safe('<img width="100" src="%s">' % obj.logo.url)

    preview.short_description = _('Logo')


class ContactsScreenAdmin(admin.ModelAdmin):
    filter_horizontal = ['offices']

class OfficeAdmin(admin.ModelAdmin):
    list_display = ['name', 'address', 'phone', 'lat', 'lng']

class FaqScreenAdmin(admin.ModelAdmin):
    filter_horizontal = ['items']

class PackageItemInline(admin.TabularInline):
    model = PackageItem
    extra = 0

class PackageAdmin(admin.ModelAdmin):
    inlines = [PackageItemInline]

class PackageItemAdmin(admin.ModelAdmin):
    pass

class PricesScreenAdmin(admin.ModelAdmin):
    filter_horizontal = ['packages']

class FooterItemAdmin(admin.ModelAdmin):
    pass

admin.site.register(Feedback, FeebackAdmin)
admin.site.register(MapScreen)
admin.site.register(SignupScreen)
admin.site.register(OtherLangingsScreen)
admin.site.register(FeedbackScreen, FeedbackScreenAdmin)
admin.site.register(Partner, PartnerAdmin)
admin.site.register(PartnersScreen, PartnersScreenAdmin)
admin.site.register(Office, OfficeAdmin)
admin.site.register(ContactsScreen, ContactsScreenAdmin)
admin.site.register(FaqScreen, FaqScreenAdmin)
admin.site.register(Package, PackageAdmin)
admin.site.register(PackageItem, PackageItemAdmin)
admin.site.register(PricesScreen, PricesScreenAdmin)
admin.site.register(FooterItem, FooterItemAdmin)

class DeletedLandingAdmin(DeleteNotAllowedModelAdmin):
    list_display = ['name', 'slug', 'main_color', 'active']
    list_editable = ['active']

    def get_queryset(self, request):
        return super(DeletedLandingAdmin, self).get_queryset(request).filter(active=False)

admin.site.register(DeletedLandings, DeletedLandingAdmin)

