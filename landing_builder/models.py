from colorfield.fields import ColorField
from django.db import models
from django.db.models import Sum
from django.template import Context, RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _, get_language
from polymorphic import PolymorphicModel
from sorl.thumbnail.shortcuts import get_thumbnail

from faq.models import FaqItem

MAIN_SCREEN = 0
DESCRIPTION_SCREEN = 1
ADVANTAGES_SCREEN = 2
FEEDBACK_SCREEN = 3
MAP_SCREEN = 4
SIGNUP_SCREEN = 5
OTHER_LANDINGS_SCREEN = 6
PARTNERS_SCREEN = 7
CONTACTS_SCREEN = 8
FAQ_SCREEN = 9
PRICES_SCREEN = 10

LANDING_TYPE = [
    (MAIN_SCREEN, _('Main screen')),
    (DESCRIPTION_SCREEN, _('Description screen')),
    (ADVANTAGES_SCREEN, _('Advantages screen')),
    (FEEDBACK_SCREEN, _('Feedbacks screen')),
    (MAP_SCREEN, _('Map screen')),
    (SIGNUP_SCREEN, _('SignUp screen')),
    (OTHER_LANDINGS_SCREEN, _('Other landings screen')),
    (PARTNERS_SCREEN, _('Partners screen')),
    (CONTACTS_SCREEN, _('Contacts screen')),
    (FAQ_SCREEN, _('FAQ Screen')),
    (PRICES_SCREEN, _('Prices screen')),
]


class Landing(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    slug = models.SlugField(_('Slug'))
    main_color = ColorField(_('Main color'), max_length=255, default='438EB4')
    preview = models.ImageField(_('Preview'), upload_to='landing_builder', blank=True)
    description_ru = models.TextField(_('Description') + ' (ru)', blank=True)
    description_en = models.TextField(_('Description') + ' (en)', blank=True)

    footer_items = models.ManyToManyField('FooterItem', verbose_name=_('Footer items'), blank=True)
    footer_faq_items = models.ManyToManyField(FaqItem, verbose_name=_('Faq items'), blank=True)

    active = models.BooleanField(_('Active'), default=True)

    def __init__(self, *args, **kwargs):
        super(Landing, self).__init__(*args, **kwargs)
        self._meta.get_field('footer_items').editable = False

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    @property
    def description(self):
        return getattr(self, 'description_' + get_language(), None)

    def get_preview(self, width=200, height=200):
        return get_thumbnail(self.preview, '%dx%d' % (width, height), crop='center')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Landing')
        verbose_name_plural = _('Landings')

    class I18NMeta:
        fields = ['name', 'description']


class DeletedLandings(Landing):
    class Meta:
        proxy = True
        verbose_name = _('Deleted landing')
        verbose_name_plural = _('Deleted landings')


class FooterItem(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    url = models.CharField(_('URL'), max_length=255, blank=True)

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Footer item')
        verbose_name_plural = _('Footer items')


class Screen(PolymorphicModel):
    landing = models.ForeignKey(Landing, verbose_name=_('Landing'), related_name='screens', null=True)
    background = models.ImageField(_('Background'), upload_to='landing_builder', blank=True)
    foreground_color = ColorField(_('Foreground color'), default='17243F')
    order = models.PositiveSmallIntegerField(_('Ordering'), default=0)
    active = models.BooleanField(_('Active'), default=True)

    type = models.PositiveSmallIntegerField(editable=False, choices=LANDING_TYPE, blank=True, null=True)
    template = models.CharField(max_length=255, editable=False, blank=True)

    def get_context(self):
        return {}

    def get_bg(self, width=1440, height=1050):
        if self.background:
            return get_thumbnail(self.background, '%dx%d' % (width, height), crop='center')

    def get_menu_item(self):
        return self.ScreenMeta.menu_item % _(self.ScreenMeta.menu_item_name)

    def render(self, request=None):
        context = self.get_context()
        context.update(background=self.get_bg(), menu_item=self.get_menu_item(), foreground_color=self.foreground_color)
        return render_to_string(self.ScreenMeta.template, RequestContext(request, context) if request else Context(context))

    def save(self, *args, **kwargs):
        if not self.pk:
            self.order = self.landing.screens.count()
        super(Screen, self).save(*args, **kwargs)

    def __unicode__(self):
        return _('Screen')

    class Meta:
        ordering = ['order']
        verbose_name = _('Screen')
        verbose_name_plural = _('Screens')

    class ScreenMeta:
        template = None
        menu_item = None
        menu_item_name = None

LOGO = [
    (1, _('Color logo')),
    (2, _('White logo')),
    (3, _('Without logo')),
]

class MainScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)
    description_ru = models.TextField(_('Description') + ' (ru)', blank=True)
    description_en = models.TextField(_('Description') + ' (en)', blank=True)
    button_text_ru = models.CharField(_('Button text') + ' (ru)', max_length=100, blank=True)
    button_text_en = models.CharField(_('Button text') + ' (en)', max_length=100, blank=True)
    logo = models.PositiveSmallIntegerField(_('Logo'), choices=LOGO, default=1)

    def __init__(self, *args, **kwargs):
        super(MainScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/main_screen.html'
        self._meta.get_field('type').default = MAIN_SCREEN

    def get_context(self):
        return {'title': self.title,
                'description': self.description,
                'button': self.button_text,
                'background': self.background,
                'logo': self.logo
                }

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    @property
    def description(self):
        return getattr(self, 'description_' + get_language(), None)

    @property
    def button_text(self):
        return getattr(self, 'button_text_' + get_language(), None)

    def __unicode__(self):
        return u'{screen} - {title}'.format(screen=_('Main screen'), title=self.title)

    class Meta:
        verbose_name = _('Main screen')
        verbose_name_plural = _('Main screens')

    class ScreenMeta:
        template = 'landing_builder/main_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray menu_line__item--active js-goto-top">%s</li>'
        menu_item_name = 'up'


class DescriptionScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)
    img = models.ImageField(_('Image'), upload_to='landings', blank=True)
    text_ru = models.TextField(_('Text') + ' (ru)', blank=True)
    text_en = models.TextField(_('Text') + ' (en)', blank=True)

    def __init__(self, *args, **kwargs):
        super(DescriptionScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/description_screen.html'
        self._meta.get_field('type').default = DESCRIPTION_SCREEN

    def get_context(self):
        return {'img': self.img, 'text': self.text, 'title': self.title}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    @property
    def text(self):
        return getattr(self, 'text_' + get_language(), None)

    def __unicode__(self):
        return _('Description screen')

    class Meta:
        verbose_name = _('Description screen')
        verbose_name_plural = _('Description screens')

    class ScreenMeta:
        template = 'landing_builder/description_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-project">%s</li>'
        menu_item_name = 'service description'


class Advantage(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    description_ru = models.TextField(_('Description') + ' (ru)', blank=True)
    description_en = models.TextField(_('Description') + ' (en)', blank=True)
    img = models.FileField(_('Image'), upload_to='landings/anvantage', blank=True)

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    @property
    def description(self):
        return getattr(self, 'description_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Advantage')
        verbose_name_plural = _('Advantages')


class AdvantagesScreen(Screen):
    advantages = models.ManyToManyField(Advantage, verbose_name=_('Advantages'))

    def __init__(self, *args, **kwargs):
        super(AdvantagesScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/advantages_screen.html'
        self._meta.get_field('type').default = ADVANTAGES_SCREEN

    def get_context(self):
        return {'advantages': self.advantages.all()}

    def __unicode__(self):
        return _('Advantage screen')

    class Meta:
        verbose_name = _('Advantages screen')
        verbose_name_plural = _('Advantages screens')

    class ScreenMeta:
        template = 'landing_builder/advantages_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-benefits">%s</li>'
        menu_item_name = 'advantages'


class Feedback(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    office = models.CharField(_('Office'), max_length=255)
    img = models.ImageField(_('Photo'), upload_to='landings/photo', blank=True)
    text_ru = models.TextField(_('Text') + ' (ru)',  blank=True)
    text_en = models.TextField(_('Text') + ' (en)',  blank=True)

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    @property
    def text(self):
        return getattr(self, 'text_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Feedback')
        verbose_name_plural = _('Feedbacks')


class FeedbackScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)
    feedbacks = models.ManyToManyField(Feedback, verbose_name=_('Feedbacks'))

    def __init__(self, *args, **kwargs):
        super(FeedbackScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/feedback_screen.html'
        self._meta.get_field('type').default = FEEDBACK_SCREEN

    def get_context(self):
        return {'feedbacks': self.feedbacks.all(), 'title': self.title}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    def __unicode__(self):
        return _('Feedback screen')

    class Meta:
        verbose_name = _('Feedback screen')
        verbose_name_plural = _('Feedback screens')

    class ScreenMeta:
        template = 'landing_builder/feedback_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-feedbacks ">%s</li>'
        menu_item_name = 'feedbacks'


class MapScreen(Screen):

    def __init__(self, *args, **kwargs):
        super(MapScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/map_screen.html'
        self._meta.get_field('type').default = MAP_SCREEN

    def __unicode__(self):
        return _('Map screen')

    class Meta:
        verbose_name = _('Map screen')
        verbose_name_plural = _('Map screens')

    class ScreenMeta:
        template = 'landing_builder/map_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-map">%s</li>'
        menu_item_name = 'map'


class SignupScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)

    def __init__(self, *args, **kwargs):
        super(SignupScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/signup_screen.html'
        self._meta.get_field('type').default = SIGNUP_SCREEN

    def get_context(self):
        return {'title': self.title}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    def __unicode__(self):
        return _('Signup screen')

    class Meta:
        verbose_name = _('Signup screen')
        verbose_name_plural = _('Signup screens')

    class ScreenMeta:
        template = 'landing_builder/signup_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-form">%s</li>'
        menu_item_name = 'ordering'


class OtherLangingsScreen(Screen):
    def __init__(self, *args, **kwargs):
        super(OtherLangingsScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/other_screen.html'
        self._meta.get_field('type').default = OTHER_LANDINGS_SCREEN

    def get_context(self):
        return {'landings': Landing.objects.exclude(pk=self.landing.pk).filter(active=True)}

    def __unicode__(self):
        return _('Other landings screen')

    class Meta:
        verbose_name = _('Other landings screen')
        verbose_name_plural = _('Other landings screens')

    class ScreenMeta:
        template = 'landing_builder/other_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-articles">%s</li>'
        menu_item_name = 'read more'


class Partner(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    logo = models.ImageField(_('Logo'), upload_to='landings')
    url = models.URLField(_('URL'))

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Partner')
        verbose_name_plural = _('Partners')


class PartnersScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)
    partners = models.ManyToManyField(Partner, verbose_name=_('Partners'))

    def __init__(self, *args, **kwargs):
        super(PartnersScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/partners_screen.html'
        self._meta.get_field('type').default = PARTNERS_SCREEN

    def get_context(self):
        return {'title': self.title, 'partners': self.partners.all()}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    def __unicode__(self):
        return _('Partners screen')

    class Meta:
        verbose_name = _('Partners screen')
        verbose_name_plural = _('Partners screens')

    class ScreenMeta:
        template = 'landing_builder/partners_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-articles">%s</li>'
        menu_item_name = 'partners'


class Office(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    address_ru = models.CharField(_('Address') + ' (ru)', max_length=255, blank=True)
    address_en = models.CharField(_('Address') + ' (en)', max_length=255, blank=True)
    phone = models.CharField(_('Phone'), max_length=100)
    lat = models.FloatField(_('Latitude'), default=0)
    lng = models.FloatField(_('Longitude'), default=0)

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    @property
    def address(self):
        return getattr(self, 'address_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Office')
        verbose_name_plural = _('Offices')


class ContactsScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)
    offices = models.ManyToManyField(Office, verbose_name=_('Offices'))

    def __init__(self, *args, **kwargs):
        super(ContactsScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/contacts_screen.html'
        self._meta.get_field('type').default = CONTACTS_SCREEN

    def get_context(self):
        return {'title': self.title, 'offices': self.offices.all()}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    def __unicode__(self):
        return _('Contacts screen')

    class Meta:
        verbose_name = _('Contacts screen')
        verbose_name_plural = _('Contacts screens')

    class ScreenMeta:
        template = 'landing_builder/contacts_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-addresses">%s</li>'
        menu_item_name = 'contacts'


class FaqScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (en)', max_length=255, blank=True)
    items = models.ManyToManyField(FaqItem, verbose_name=_('FAQ items'))

    def __init__(self, *args, **kwargs):
        super(FaqScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/faq_screen.html'
        self._meta.get_field('type').default = FAQ_SCREEN

    def get_context(self):
        return {'title': self.title, 'items': self.items.all()}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    def __unicode__(self):
        return _('FAQ screen')

    class Meta:
        verbose_name = _('FAQ screen')
        verbose_name_plural = _('FAQ screens')

    class ScreenMeta:
        template = 'landing_builder/faq_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-faq">%s</li>'
        menu_item_name = 'faq'


class Package(models.Model):
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=255, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=255, blank=True)
    description_ru = models.CharField(_('Description') + ' (ru)', max_length=255, blank=True)
    description_en = models.CharField(_('Description') + ' (en)', max_length=255, blank=True)
    image = models.FileField(_('Image'), upload_to='landing_builder', blank=True)
    link = models.CharField(_('URL'), max_length=255, blank=True)

    def get_total_price(self):
        try:
            price_template = self.items.filter(price_template__isnull=False).distinct().first().price_template
        except:
            return
        if price_template:
            return price_template.replace('(price)', '{:.0f}'.format(sum([i.price for i in self.items.filter(price__isnull=False).distinct()])))

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    @property
    def description(self):
        return getattr(self, 'description_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Package')
        verbose_name_plural = _('Packages')


class PackageItem(models.Model):
    package = models.ForeignKey(Package, verbose_name=_('Package'), related_name='items')
    name_ru = models.CharField(_('Name') + ' (ru)', max_length=244, blank=True)
    name_en = models.CharField(_('Name') + ' (en)', max_length=244, blank=True)
    price = models.FloatField(_('Price'), blank=True, null=True)
    price_template = models.CharField(_('Price template'), max_length=100, blank=True, help_text=_('For example "$(price)", "(price)rub", etc.'))

    def get_price(self):
        if self.price_template and self.price:
            return self.price_template.replace('(price)', '{:.0f}'.format(self.price))

    @property
    def name(self):
        return getattr(self, 'name_' + get_language(), None)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Package item')
        verbose_name_plural = _('Package items')


class PricesScreen(Screen):
    title_ru = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    title_en = models.CharField(_('Title') + ' (ru)', max_length=255, blank=True)
    packages = models.ManyToManyField(Package, verbose_name=_('Packages'))

    def __init__(self, *args, **kwargs):
        super(PricesScreen, self).__init__(*args, **kwargs)
        self._meta.get_field('template').default = 'landing_builder/prices_screen.html'
        self._meta.get_field('type').default = PRICES_SCREEN

    def get_context(self):
        return {'title': self.title, 'packages': self.packages.all()}

    @property
    def title(self):
        return getattr(self, 'title_' + get_language(), None)

    def __unicode__(self):
        return _('Prices screen')

    class Meta:
        verbose_name = _('Prices screen')
        verbose_name_plural = _('Prices screens')

    class ScreenMeta:
        template = 'landing_builder/prices_screen.html'
        menu_item = u'<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-faq">%s</li>'
        menu_item_name = 'prices'
