jQuery(document).ready(function($) {
	var window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	$( window ).on( "orientationchange", function( event ) {
	  location.reload();
	});	
	
	function infoPopup(text) {
	  $("#modalInfo .modal-content").html(text);
	  $('#modalInfo').reveal({
	     animation: 'fadeAndPop',                   //fade, fadeAndPop, none
	     animationspeed: 300,                       //how fast animtions are
	     closeonbackgroundclick: true,              //if you click background will modal close?
	     dismissmodalclass: 'close-reveal-modal'    //the class of a button or element that will close an open modal
	});
	}
	
	function enableParallax() {
        if (typeof $.fn.parallax != 'undefined') {
            $('.parallax').each(function() {
                var $t = $(this);
                $t.addClass("parallax-enabled");
                $t.parallax("100%", 0.1, false);
            });
        }
    }
    var is_mobiles
    // parallax function
	if (window_width < 1200 ){
		is_mobiles = true;
	}else{
		is_mobiles = false;
	}

	if(!is_mobiles){
		enableParallax();	
	}
   $('.partners li').height($('.partners li').width() * 0.7857);
	
    var slovar = {
    	main_screen: 'наверх',
    	project: 'Об услуге',
    	signup: $('#signup h2').html(),
    	benefits: 'ПРЕИМУЩЕСТВА',
    	addresses: 'Контакты',
    	partners: 'НАШИ ПАРТНЕРЫ',
    	feedbacks: 'отзывы',
    	faq: 'ВОПРОСЫ И ОТВЕТЫ',
    	map_style: 'Карта наших юристов',
    	articles: 'Другие услуги'
    }
    var herz = [];
	var as =0;
	var curentSlide;
	var scr = $(window).scrollTop();
	var htt ="";
	var curh = $('.page__wrapper > div:first-child').height();
	setTimeout(function () {
		$('.page__wrapper > div').each(function () {
			herz[as] = {'name': $(this).attr('id'), 'vall': $(this).offset().top} ;
			as++;
			htt += '<li class="menu_line__item colorize_background colorize_color text-caps text-gray js-goto-top"><a href="#'+$(this).attr('id')+'">'+slovar[$(this).attr('id')]+'</a></li>';
		});
		$('.menu_line__menu').html(htt)
		console.log(herz);
	},900);
	$(window).scroll(function() {
			scr = $(window).scrollTop();
			for(var i = 0; i < herz.length; i++) {
				if ( scr - document.body.clientHeight / 2 <= herz[i].vall ) {
					curentSlide = herz[i].name;
					var ths = $('.menu_line__menu a[href="#'+curentSlide+'"]').closest('li');
					if (!ths.hasClass('menu_line__item--active')){
						$('.menu_line__menu .menu_line__item--active').removeClass('menu_line__item--active')
						ths.addClass('menu_line__item--active');
					}
					break;
				}
			};
		    if ($(window).scrollTop() > curh) {
		       $('.menu_line').fadeIn(400);
		    } else {
		        $('.menu_line').fadeOut(400);
		    }
		});
	
	//signup form validation
	 $('#form_signup').validate({
	    sendForm : true,
	    onKeyup : true,
	    valid : function() {
	        $('#form_signup').removeClass("error");
	        console.log("form valid");
	    },
	    invalid : function() {
	      $('#form_signup').addClass("error");
	      console.log("form invalid");
	    },
	    eachField: function(event, status, options) {
	      if (!status.required || !status.pattern) {
	        $(this).addClass("error");
	      } else {
	        $(this).removeClass("error");
	      }
	    }
	  });
	 
	//signup form phone field mask
  $("#form_signup input[name=phone]").intlTelInput("destroy");
  $("input[name=phone]").intlTelInput({
    utilsScript: "",
    defaultCountry: "auto",
    nationalMode: false,
    autoHideDialCode: false,
    autoPlaceholder: false
  });
  
	//signup form submit
  $('#form_signup').on('submit', function(e){
    e.preventDefault();
    
    var telInput = $($("#form_signup input[name=phone]")[0]);
    
    if ($.trim(telInput.val())) {
      if (telInput.intlTelInput("isValidNumber")) {
        telInput.removeClass("error");
        telInput.parents(".intl-tel-input").removeClass("error");
      } else {
        telInput.addClass("error");
        telInput.parents(".intl-tel-input").addClass("error");
      }
    }
    
    if ( $(this).find(".error").length == 0 ) {
      var url = $(this).attr('action');
      var data = $(this).serializeArray();
      data.push({name: 'landing', value: $('input[name=landing]').val() })
      $.post(url, data, function(r){
        console.log(r);
        if (r.success){
          infoPopup('Данные успешно отправлены');
          /*alert('Данные успешно отправлены');*/
        } else {
          infoPopup('Ошибка отправки. Все поля обязательны для заполнения');
          /*alert('Ошибка отправки. Все поля обязательны для заполнения')*/
        }
      })
    }
  });
	
	
	$('.menu_line').on('click', 'a', function () {
			if(!$(this).hasClass('link')) {
				var herf = $(this).attr('href');
				var as = $(herf).offset().top + 20;
				$('body').animate({"scrollTop":as},1200);
				return false;
			}
		});



    // toggle hamburger
    $('#nav-toggle').on('click', function(event) {
    	event.preventDefault();
        $(this).toggleClass('active');
		if (window_width <= 1000){
			$('.menu_line__menu').slideToggle();
		}
    });


    $('.js-switch').on('click', function(event) {
    	event.preventDefault();
    	if ($(this).text() === 'далее'){
    		$(this).text('назад');
	    	$('.js-form-block-1').fadeOut(200, function() {
	    		$('.js-form-block-2').fadeIn(200);
	    	});
    	}else{
    		$(this).text('далее');
	    	$('.js-form-block-2').fadeOut(200, function() {
	    		$('.js-form-block-1').fadeIn(200);
	    	});
    	}
    });
    $('.js-goto-project').click(function () {
    	$('body').animate({"scrollTop":$('#main_screen').height()},600);
    });




    $('body')
    	.on('mouseenter', '.menu_line__item', function(event) {
    		$(this).addClass('menu_line__item--hovered');
    	})
    	.on('mouseleave', '.menu_line__item', function(event) {
    		event.preventDefault();
    		$(this).removeClass('menu_line__item--hovered');	
    	});



    $('.menu_line')
    	.on('mouseenter', function(event) {
    		$('.white_overlay').fadeIn(500);
    		$('.menu_line__item').animate({'width': 350}, 150);
    	})
    	.on('mouseleave', function(event) {
    		event.preventDefault();
    		$('.white_overlay').fadeOut(500);
    		$('.menu_line__item').animate({'width': 5}, 10);
    	});


    if ($('.feedback_slider').length > 1) {
    	//swipe only on mobiles



        var feedbacks_slider_options = {
            slides: '.feedbacks_slide', // Класс слайдов
            swipe: is_mobiles, // добавляет возможность свайпа (требуется встроить touchSwipe.js
            slideTracker: true, // добавляет навигацию в форме ul с li
            slideTrackerID: 'feedbacks_slideposition', // id блока внутри которого ul навигации
            slideOnInterval: false, // перелистывать слайды по интервалу
            interval: 5000, // Интервал перелистывания
            animateDuration: 500, // продолжительность анимации
            animationEasing: 'ease', // Доступные изинги: linear ease in out in-out snap easeOutCubic easeInOutCubic easeInCirc easeOutCirc easeInOutCirc easeInExpo easeOutExpo easeInOutExpo easeInQuad easeOutQuad easeInOutQuad easeInQuart easeOutQuart easeInOutQuart easeInQuint easeOutQuint easeInOutQuint easeInSine easeOutSine easeInOutSine easeInBack easeOutBack easeInOutBack
            pauseOnHover: false // Приостановить слайдинг при ховере
        };
        $('.feedback_slider').simpleSlider(feedbacks_slider_options); //инициализация
        var feedbacks_slider = $('.feedback_slider').data("simpleslider");
        $('.feedbacks_indicator').on('click', function(event) {
            event.preventDefault();
            feedbacks_slider.nextSlide(+($(this).attr("data-index")));
        });

        $('.feedbacks_slider__navi_triagle-prev').on('click', function(event) {
            event.preventDefault();
            feedbacks_slider.prevSlide();
        });
        $('.feedbacks_slider__navi_triagle-next').on('click', function(event) {
            event.preventDefault();
            feedbacks_slider.nextSlide();
        });

        //контроль появления навигационных стрелок
        $('.feedback_slider').on("beforeSliding", function(event) {

            if (feedbacks_slider.currentSlide + 1 == 1) {
                $('.feedbacks_slider__navi_triagle-prev').css('display', 'none');
                $('.feedbacks_slider__navi_triagle-next').css('display', 'block');
            } else if (feedbacks_slider.currentSlide + 1 == feedbacks_slider.totalSlides) {
                $('.feedbacks_slider__navi_triagle-next').css('display', 'none');
                $('.feedbacks_slider__navi_triagle-prev').css('display', 'block');
            } else {
                $('.feedbacks_slider__navi_triagle-next').css('display', 'block');
                $('.feedbacks_slider__navi_triagle-prev').css('display', 'block');
            }
        });
        //контроль появления навигационных стрелок

    } 
   else {
   	$('.feedbacks_slider__navi_triagle').hide();
   	$('.feedbacks_slideposition').hide();
   }

    var max_text_height = 0;

    if($(window).width() <= 460){
    	var feedback_photo_text_padding = 297;
    }else{
    	var feedback_photo_text_padding = 371;
    }
    $.each($('.feedbacks_slider__text'), function(index, val) {
    	if ($(this).height() > max_text_height) {
    		max_text_height = $(this).height();
    	}
    });
    $('.feedback_slider').css('height', feedback_photo_text_padding + max_text_height);

	var small_mini_slider_width = 1300;
	var single_slide_mini_slider_width = 920;
	var slide_width = 384;
	var margin = 0;

	if (window_width < small_mini_slider_width){
		slide_width = 284;
	}
	// else if (window_width <= single_slide_mini_slider_width){
		// margin = 400;
	// }
	var slide_full_width = $('.mini_slide').width();
	var slider_count = $('.mini_slide').length;
	$('.mini_slider').css('width', slide_full_width * slider_count);

	function resizeProject() {
		var height = $(".project__info").height();
		var width = $(".project__info").width();
		$(".project-inner").css("height", height);
		if (width/640 < height/480) {
			$(".project__passports").css("width", width);
		} else {
			$(".project__passports").css("width", "");
		}



	}
	$(document).ready(function() {
		resizeProject();
	});
	window.onresize = function(event) {
		resizeProject();

		window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	    if(window_width <= 460){
	    	var feedback_photo_text_padding = 297;
	    }else{
	    	var feedback_photo_text_padding = 371;
	    }
		max_text_height = 0;
	    $.each($('.feedbacks_slider__text'), function(index, val) {
	    	if ($(this).height() > max_text_height) {
	    		max_text_height = $(this).height();
	    	}
	    });
	    $('.feedback_slider').css('height', feedback_photo_text_padding + max_text_height);


		slide_full_width = $('.mini_slide').width();
		slider_count = $('.mini_slide').length;
		$('.mini_slider').css('width', slide_full_width * slider_count);	    
	};



	


    var slider_width = $('.mini_slide').length * (slide_width + margin) - margin,
        current_translate = 0,
        news_sliding_in_progress = false;
    $('.slider__navi_triagle-next').on('click', function() {
        $('.slider__navi_triagle-prev').show();
        current_translate -= slide_width + margin;
        $('.mini_slider').css({
            'transform': 'translate(' + current_translate + 'px, 0px)'
        });
        if (slider_width + current_translate < (slide_width + slide_width + margin + slide_width + margin + slide_width + margin)) {
            $(this).hide();
            return false;
        }
    });

    $('.slider__navi_triagle-prev').on('click', function() {
        $('.slider__navi_triagle-next').show();
        if (!news_sliding_in_progress){
	        current_translate += slide_width + margin;
	        $('.mini_slider').css({
	            'transform': 'translate(' + current_translate + 'px, 0px)'
	        });
        	setTimeout(function () {
        		news_sliding_in_progress = false;
        	}, 500);
	        if ($('.mini_slider').css('transform') === 'matrix(1, 0, 0, 1, -' + (slide_width + margin) + ', 0)') {
	            
	            $('.slider__navi_triagle-prev').hide();
	            return false;
	        }
        }
    });



});


// map = = = = = = = =  

function initializeMap ($mapEl, coords, infoboxtext) {

    var mapOptions = {
		center: coords,
		zoom: 16,
            panControl: false,
			zoomControl: false,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false
    	};


	var map = new google.maps.Map($mapEl[0], mapOptions);

    var stylesArray = [ { "featureType": "landscape.man_made", "elementType": "geometry.fill", "stylers": [ { "color": "#17243F" } ] },{ "featureType": "water", "elementType": "geometry.fill", "stylers": [ { "color": "#17243F" }, { "saturation": -43 }, { "lightness": 48 } ] },{ "featureType": "poi", "stylers": [ { "visibility": "off" } ] },{ "featureType": "landscape.natural", "elementType": "geometry.fill", "stylers": [ { "color": "#17243F" } ] },{ "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#438EB4" } ] },{ "featureType": "road.arterial", "stylers": [ { "color": "#438EB4" } ] },{ "featureType": "road.local", "elementType": "geometry.fill", "stylers": [ { "color": "#438EB4" }, { "lightness": 48 } ] },{ "featureType": "road.highway", "elementType": "labels.text.stroke", "stylers": [ { "visibility": "off" } ] },{ "featureType": "road.highway", "elementType": "labels.text.fill", "stylers": [ { "color": "#ffffff" } ] },{ "featureType": "road.local", "elementType": "labels.text.stroke", "stylers": [ { "color": "#438EB4" }, { "lightness": 48 } ] },{ "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [ { "color": "#000000" } ] },{ "featureType": "road.highway", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] },{ "featureType": "water", "elementType": "labels.text.stroke", "stylers": [ { "visibility": "off" } ] },{ "featureType": "landscape", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] },{ } ];

	var styledMap = new google.maps.StyledMapType(stylesArray,
    {name: "Styled Map"});

	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');

	// var image = 'img/i__map_marker.png';

	var image = {
	  url: 'img/i__map_marker.png',
	  // size: new google.maps.Size(71, 71),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(15, 5),
	  scaledSize: new google.maps.Size(15, 15)
	};

	var marker = new google.maps.Marker({
	  position: map.getCenter(),
	  map: map,
	  icon: image
	});

	var moveX = $mapEl.width() * 0.35;
	var moveY = $mapEl.height() * -0.2;

	map.panBy(moveX, moveY);

	var infoboxW = $mapEl.width() * 0.7;

	var infowindow = new google.maps.InfoWindow({
	  content: infoboxtext,
	  maxWidth: infoboxW,
	  pixelOffset: new google.maps.Size(infoboxW/2 + 15, 10)
	});

	infowindow.open(map,marker);


	google.maps.event.addDomListener(window, "resize", function() {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);

		infowindow.setContent(infowindow.getContent());

		// moveX = $mapEl.width() * 0.2;
		// moveY = $mapEl.height() * -0.3;
		
		// map.panBy(moveX, moveY);

	});

}

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
/* load maps */
	//var address_cyprus = {lat: 34.774512, lng: 32.418942};
	//var address_moscow = {lat: 55.750704, lng: 37.610200};
    //
    //var text_cyprus = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.';
    //var text_moscow = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.';
    //
    //google.maps.event.addDomListener(window, 'load', initializeMap($('#office_cyprus'), address_cyprus, text_cyprus));
    //google.maps.event.addDomListener(window, 'load', initializeMap($('#office_moscow'), address_moscow, text_moscow));

// = = = = = = = = map