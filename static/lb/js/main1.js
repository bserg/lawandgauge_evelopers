jQuery(document).ready(function($) {
    var window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    $( window ).on( "orientationchange", function( event ) {
      location.reload();
    }); 
    function enableParallax() {
        if (typeof $.fn.parallax != 'undefined') {
            $('.parallax').each(function() {
                var $t = $(this);
                $t.addClass("parallax-enabled");
                $t.parallax("100%", 0.1, false);
            });
        }
    }
    // parallax function
    if (window_width < 1200 ){
        var is_mobiles = true;
    }else{
        var is_mobiles = false;
    }

    if(!is_mobiles){
        enableParallax();   
    }
    if (window_width < 1000){
        $('.benefits').css('height', $('.benefits').height()*0.87)  
    }
    

    var scrolled = $(window).scrollTop(),
        window_height = $(window).height();
        // $('.menu_line').css('top', scrolled);
    if (scrolled >= window_height){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('.js-goto-top').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }

    if (scrolled <= window_height){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('.js-goto-top').addClass('menu_line__item--active');
        $('.menu_line').fadeOut(400);
    }else if(((scrolled - 10)  >= $('.project').offset().top) && (scrolled < $('.benefits').offset().top)){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('li.js-goto-project').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }else if((scrolled >= $('.benefits').offset().top) && (scrolled < $('.feedbacks').offset().top)){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('li.js-goto-benefits').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }else if((scrolled >= $('.feedbacks').offset().top) && (scrolled < $('.map').offset().top)){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('li.js-goto-feedbacks').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }else if((scrolled >= $('.map').offset().top) && (scrolled < $('.form').offset().top)){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('li.js-goto-map').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }else if(scrolled + 500 > $('.articles').offset().top){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('li.js-goto-articles').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }else if((scrolled >= $('.form').offset().top) && (scrolled < $('.articles').offset().top)){
        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('li.js-goto-form').addClass('menu_line__item--active');
        $('.menu_line').fadeIn(400);
    }

    window.onscroll = function() {
        scrolled = $(window).scrollTop();
        if(window_width <= 1000 && scrolled > window_height - 40){
            // $('.menu_line__item--active').removeClass('menu_line__item--active');
            // $('.js-goto-top').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }else if(scrolled <= window_height){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('.js-goto-top').addClass('menu_line__item--active');
            $('.menu_line').fadeOut(400);
        }else if(((scrolled - 10) >= $('.project').offset().top) && (scrolled < $('.benefits').offset().top)){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('li.js-goto-project').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }else if((scrolled >= $('.benefits').offset().top) && (scrolled < $('.feedbacks').offset().top)){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('li.js-goto-benefits').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }else if((scrolled >= $('.feedbacks').offset().top) && (scrolled < $('.map').offset().top)){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('li.js-goto-feedbacks').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }else if((scrolled >= $('.map').offset().top) && (scrolled < $('.form').offset().top)){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('li.js-goto-map').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }else if(scrolled + 500 > $('.articles').offset().top){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('li.js-goto-articles').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }else if((scrolled >= $('.form').offset().top) && (scrolled < $('.articles').offset().top)){
            $('.menu_line__item--active').removeClass('menu_line__item--active');
            $('li.js-goto-form').addClass('menu_line__item--active');
            $('.menu_line').fadeIn(400);
        }
    }




    $('.js-goto-top').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop:0 - 40}, '500');
        }else{
            $("html, body").animate({scrollTop:0}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $(this).addClass('menu_line__item--active');
        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);
    });
    $('.js-goto-project').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000  && !$(this).is('.js-goto-project-main_screen')){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop: $('.project').offset().top + 11 - 40}, '500');
        }else if(window_width <= 1000){
            $("html, body").animate({scrollTop: $('.project').offset().top + 11 - 40}, '500');
        }else{
            $("html, body").animate({scrollTop: $('.project').offset().top + 11}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $('.menu_line__item.js-goto-project').addClass('menu_line__item--active');
        if ($(this).hasClass('main_screen__go_down_arrow')) {
            setTimeout(function(){
                $('.menu_line').fadeIn(400);
            }, 505);
        }
        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);
    });
    $('.js-goto-benefits').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop: $('.benefits').offset().top - 40}, '500');
        }else{
            $("html, body").animate({scrollTop: $('.benefits').offset().top}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $(this).addClass('menu_line__item--active');

        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);
    });
    $('.js-goto-feedbacks').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop: $('.feedbacks').offset().top - 40}, '500');
        }else{
            $("html, body").animate({scrollTop: $('.feedbacks').offset().top}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $(this).addClass('menu_line__item--active');

        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);
    });
    $('.js-goto-map').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop: $('.map').offset().top - 40}, '500');
        }else{
            $("html, body").animate({scrollTop: $('.map').offset().top}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $(this).addClass('menu_line__item--active');

        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);
    });
    $('.js-goto-form').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000 && !$(this).is('.js-goto-form-main_screen')){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop: $('.form').offset().top - 40}, '500');
        }else if(window_width <= 1000){
            $("html, body").animate({scrollTop: $('.form').offset().top - 40}, '500');
        }else{
            $("html, body").animate({scrollTop: $('.form').offset().top}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $(this).addClass('menu_line__item--active');

        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);
    });
    $('.js-goto-articles').on('click', function(event) {
        event.preventDefault();
        

        if (window_width <= 1000){
            $('#nav-toggle').click();
            $("html, body").animate({scrollTop: $('.articles').offset().top - 40}, '500');
        }else{
            $("html, body").animate({scrollTop: $('.articles').offset().top}, '500');
        }

        $('.menu_line__item--active').removeClass('menu_line__item--active');
        $(this).addClass('menu_line__item--active');

        $('.white_overlay').fadeOut(500);
        $('.menu_line__item').animate({'width': 5}, 10);

    });


    // toggle hamburger
    $('#nav-toggle').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass('active');
        if (window_width <= 1000){
            $('.menu_line__menu').slideToggle();
        }
    });
    // toggle hamburger


    // $('.js-next').on('click', function(event) {
    //  event.preventDefault();
    //  $('.js-form-block-1').fadeOut(300, function() {
    //      $('.js-form-block-2').fadeIn(300);
    //  });
    // });

    // $('.js-back').on('click', function(event) {
    //  event.preventDefault();
    //  $('.js-form-block-1').fadeOut(300, function() {
    //      $('.js-form-block-2').fadeIn(300);
    //  });
    // });

    $('.js-switch').on('click', function(event) {
        event.preventDefault();
        if ($(this).text() === 'далее'){
            $(this).text('назад');
            $('.js-form-block-1').fadeOut(200, function() {
                $('.js-form-block-2').fadeIn(200);
            });
        }else{
            $(this).text('далее');
            $('.js-form-block-2').fadeOut(200, function() {
                $('.js-form-block-1').fadeIn(200);
            });
        }
    });





    $('.menu_line__item')
        .on('mouseenter', function(event) {
            $(this).addClass('menu_line__item--hovered');
        })
        .on('mouseleave', function(event) {
            event.preventDefault();
            $(this).removeClass('menu_line__item--hovered');    
        });



    $('.menu_line')
        .on('mouseenter', function(event) {
            $('.white_overlay').fadeIn(500);
            $('.menu_line__item').animate({'width': 350}, 150);
        })
        .on('mouseleave', function(event) {
            event.preventDefault();
            $('.white_overlay').fadeOut(500);
            $('.menu_line__item').animate({'width': 5}, 10);
        });


    if ($('.feedback_slider').length > 0) {
        //swipe only on mobiles



        var feedbacks_slider_options = {
            slides: '.feedbacks_slide', // Класс слайдов
            swipe: is_mobiles, // добавляет возможность свайпа (требуется встроить touchSwipe.js
            slideTracker: true, // добавляет навигацию в форме ul с li
            slideTrackerID: 'feedbacks_slideposition', // id блока внутри которого ul навигации
            slideOnInterval: false, // перелистывать слайды по интервалу
            interval: 5000, // Интервал перелистывания
            animateDuration: 500, // продолжительность анимации
            animationEasing: 'ease', // Доступные изинги: linear ease in out in-out snap easeOutCubic easeInOutCubic easeInCirc easeOutCirc easeInOutCirc easeInExpo easeOutExpo easeInOutExpo easeInQuad easeOutQuad easeInOutQuad easeInQuart easeOutQuart easeInOutQuart easeInQuint easeOutQuint easeInOutQuint easeInSine easeOutSine easeInOutSine easeInBack easeOutBack easeInOutBack
            pauseOnHover: false // Приостановить слайдинг при ховере
        };
        $('.feedback_slider').simpleSlider(feedbacks_slider_options); //инициализация
        var feedbacks_slider = $('.feedback_slider').data("simpleslider");
        $('.feedbacks_indicator').on('click', function(event) {
            event.preventDefault();
            feedbacks_slider.nextSlide(+($(this).attr("data-index")));
        });

        $('.feedbacks_slider__navi_triagle-prev').on('click', function(event) {
            event.preventDefault();
            feedbacks_slider.prevSlide();
        });
        $('.feedbacks_slider__navi_triagle-next').on('click', function(event) {
            event.preventDefault();
            feedbacks_slider.nextSlide();
        });

        //контроль появления навигационных стрелок
        $('.feedback_slider').on("beforeSliding", function(event) {

            if (feedbacks_slider.currentSlide + 1 == 1) {
                $('.feedbacks_slider__navi_triagle-prev').css('display', 'none');
                $('.feedbacks_slider__navi_triagle-next').css('display', 'block');
            } else if (feedbacks_slider.currentSlide + 1 == feedbacks_slider.totalSlides) {
                $('.feedbacks_slider__navi_triagle-next').css('display', 'none');
                $('.feedbacks_slider__navi_triagle-prev').css('display', 'block');
            } else {
                $('.feedbacks_slider__navi_triagle-next').css('display', 'block');
                $('.feedbacks_slider__navi_triagle-prev').css('display', 'block');
            }
        });
        //контроль появления навигационных стрелок

    }

    var max_text_height = 0;

    if($(window).width() <= 460){
        var feedback_photo_text_padding = 297;
    }else{
        var feedback_photo_text_padding = 371;
    }
    $.each($('.feedbacks_slider__text'), function(index, val) {
        if ($(this).height() > max_text_height) {
            max_text_height = $(this).height();
        }
    });
    $('.feedback_slider').css('height', feedback_photo_text_padding + max_text_height);

    var small_mini_slider_width = 1300;
    var single_slide_mini_slider_width = 920;
    var slide_width = 384;
    var margin = 0;

    if (window_width < small_mini_slider_width){
        slide_width = 284;
    }
    // else if (window_width <= single_slide_mini_slider_width){
        // margin = 400;
    // }
    var slide_full_width = $('.mini_slide').width();
    var slider_count = $('.mini_slide').length;
    $('.mini_slider').css('width', slide_full_width * slider_count);
    window.onresize = function(event) {

        window_width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if(window_width <= 460){
            var feedback_photo_text_padding = 297;
        }else{
            var feedback_photo_text_padding = 371;
        }
        max_text_height = 0;
        $.each($('.feedbacks_slider__text'), function(index, val) {
            if ($(this).height() > max_text_height) {
                max_text_height = $(this).height();
            }
        });
        $('.feedback_slider').css('height', feedback_photo_text_padding + max_text_height);


        slide_full_width = $('.mini_slide').width();
        slider_count = $('.mini_slide').length;
        $('.mini_slider').css('width', slide_full_width * slider_count);        
    };



    


    var slider_width = $('.mini_slide').length * (slide_width + margin) - margin,
        current_translate = 0,
        news_sliding_in_progress = false;
    $('.slider__navi_triagle-next').on('click', function() {
        $('.slider__navi_triagle-prev').show();
        current_translate -= slide_width + margin;
        $('.mini_slider').css({
            'transform': 'translate(' + current_translate + 'px, 0px)'
        });
        if (slider_width + current_translate < (slide_width + slide_width + margin + slide_width + margin + slide_width + margin)) {
            $(this).hide();
            return false;
        }
    });

    $('.slider__navi_triagle-prev').on('click', function() {
        $('.slider__navi_triagle-next').show();
        if (!news_sliding_in_progress){
            current_translate += slide_width + margin;
            $('.mini_slider').css({
                'transform': 'translate(' + current_translate + 'px, 0px)'
            });
            setTimeout(function () {
                news_sliding_in_progress = false;
            }, 500);
            if ($('.mini_slider').css('transform') === 'matrix(1, 0, 0, 1, -' + (slide_width + margin) + ', 0)') {
                
                $('.slider__navi_triagle-prev').hide();
                return false;
            }
        }
    });

    //init phone international plugin
    $("input[name=phone]").intlTelInput("destroy");
    $("input[name=phone]").intlTelInput({
      utilsScript: "",
      defaultCountry: "auto",
      nationalMode: false,
      autoHideDialCode: false,
      autoPlaceholder: false
    });
});