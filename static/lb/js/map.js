$(document).ready(function () {
    $('#map_style').height($('#map_style').width() / 3);
    var markers = [];
    var map;
    var sizeFactor = 0;
    var cnt = 0;
        $('body').on('click', '.bal_sl_arr', function() {
          if ($(this).hasClass('baslarr_right')) {
            cnt++;
            if (cnt > $('.bal_slide').length - 1) {
              cnt = 0;
            }
          } else {
            cnt--;
            if (cnt < 0) {
              cnt = $('.bal_slide').length - 1;
            }
          }
          $('.bal_slider').css({
            'margin-left': '-' + cnt * 286 + 'px'
          });
          $('.bal_slider_name_wrp').css({
            'margin-left': '-' + cnt * 45 + 'px'
          });
          $('.ogn_bal').removeClass('active');
          $('.ogn_bal:nth-child('+(cnt+1)+')').addClass('active');
        });
    function initialize() {
      var styles = [{
        "stylers": [{
          "color": "#5b5bc9"
        }]
      }, {
        "elementType": "labels.text",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#3f628c"
        }]
      }, {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#2e4766"
        }]
      }, {
        "featureType": "landscape.natural.landcover",
        "stylers": [{
          "color": "#23344e"
        }]
      }, {
        "featureType": "landscape.natural.terrain",
        "stylers": [{
          "color": "#23344e"
        }]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{
          "visibility": "off"
        }]
      }];
      
      // Create a new StyledMapType object, passing it the array of styles,
      // as well as the name to be displayed on the map type control.
      var styledMap = new google.maps.StyledMapType(styles, {
        name: "Styled Map"
      });

      // Create a map object, and include the MapTypeId to add
      // to the map type control.
      var mapOptions;
      mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(55.8134104, 37.7063927),
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
      };
      map = new google.maps.Map(document.getElementById('map_style'),
        mapOptions);

      $.get('/map/data/', {
        language: 'ru'
      }, function(data){
        for (var i = 0; i < data.data.length; i++) {
          if (sizeFactor < data.data[i].count) {
            sizeFactor = data.data[i].count;
          }
        }
        for (var i = 0; i < data.data.length; i++) {
          createMarker(data.data[i]);
        }
        setMarkersMap(map, markers)
        console.log('/map/data/', data.data);
      });

      //Associate the styled map with the MapTypeId and set it to display.
      map.mapTypes.set('map_style', styledMap);
      map.setMapTypeId('map_style');

      function setMarkersMap(map, markers) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      function createMarker(place) {
        var ns = Math.floor(35 * (place.count / (sizeFactor / 100)) / 100);
        var ps =  ns / 2;
        if (ns < 8) {
          ns = 8;
          ps =  ns / 2;
        }
        var markerImage = new google.maps.MarkerImage('/static/landing/img/regularBall.png',
          new google.maps.Size(34, 35), //size
          new google.maps.Point(0, 0), //origin point
          new google.maps.Point(ps, ps), // offset point
          new google.maps.Size(ns, ns)
        );
        var markerImageHover = new google.maps.MarkerImage('/static/landing/img/regularBall2.png',
          new google.maps.Size(34, 35), //size
          new google.maps.Point(0, 0), //origin point
          new google.maps.Point(17, 17), // offset point
          new google.maps.Size(34, 35)
        );
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(place.lat, place.lng),
          map: map,
          icon: markerImage,
          title: place.name
        });

        markers.push(marker);

        // function cizz(n,c){
        //     return c[0]+((/^[0,2-9]?[1]$/.test(n))?c[2]:((/^[0,2-9]?[2-4]$/.test(n))?c[3]:c[1]))
        // }
        // var jur_word = ['юрист','ов','','а'];
        // var texxxt = place.count + ' ' + cizz(place.count,jur_word);
        var tooltip_content = '<div class="balloon"><div class="bal_slider nclear">';
        for (var i = 0; i < place.contractors.length; i++) {
          tooltip_content += '<div class="bal_slide">' +
            '<div class="bal_slide_name">' + place.contractors[i].first_name + ' ' + place.contractors[i].last_name + '</div><div class="bal_poi">' +
            '<div style="background-image: url(' + place.contractors[i].avatar + ');" class="bal_poi_ava"></div>' +
            '</div>' +
            '</div>';
        }
        tooltip_content += '</div>' +
          // '<div class="bal_count">'+texxxt+'</div>'+
          '<div class="bal_slider_name_wrp"><div class="bal_slider_name nclear">';
        for (var i = 0; i < place.contractors.length; i++) {
          tooltip_content += '<div class="ogn_bal '+ (i==0?'active':'') +'"></div>';
        }
        tooltip_content += '</div></div><div class="bal_city">' + place.name + '</div>';
        if (place.contractors.length != 1) {
          tooltip_content += '<div class="bal_sl_arr baslarr_left"></div><div class="bal_sl_arr baslarr_right"></div>';
        }
        tooltip_content += '</div>';

        var tooltip_content2 = '<div class="bala2_name">'+ place.name +'</div>';
        var var_infobox_props = {
          content: tooltip_content,
          disableAutoPan: false,
          maxWidth: 0,
          pixelOffset: new google.maps.Size(-171, -373),
          zIndex: null,
          boxClass: "myInfobox",
          closeBoxMargin: "2px",
          closeBoxURL: "/static/landing/img/close_bu.png",
          infoBoxClearance: new google.maps.Size(1, 1),
          visible: true,
          pane: "floatPane",
          enableEventPropagation: true
        };
        var var_infobox_props2 = {
          content: tooltip_content2,
          disableAutoPan: false,
          maxWidth: 265,
          pixelOffset: new google.maps.Size(-132, 20),
          zIndex: null,
          boxClass: "myInfobox2",
          closeBoxMargin: "2px",
          closeBoxURL: "",
          infoBoxClearance: new google.maps.Size(1, 1),
          visible: true,
          pane: "floatPane",
          enableEventPropagation: true
        };
        
        var var_infobox = new InfoBox(var_infobox_props);
        google.maps.event.addListener(var_infobox, 'closeclick', function() {
          marker.setMap(map);
        });


        google.maps.event.addListener(marker, 'click', function() {
          var_infobox.open(map, marker);
          marker.setMap(null);
        });

        var var_infobox2 = new InfoBox(var_infobox_props2);
        google.maps.event.addListener(marker, 'mouseover', function() {
          var_infobox2.open(map, marker);
          marker.setIcon(markerImageHover);
        });

        google.maps.event.addListener(marker, 'mouseout', function() {
          var_infobox2.close();
          marker.setIcon(markerImage);
        });

      }
    }


    google.maps.event.addDomListener(window, 'load', initialize);
});