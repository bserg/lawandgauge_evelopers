function initLand() {
	$('#form').validate({
		sendForm : true,
	    onKeyup : true,
		valid : function() {
				sendEmail();
			},
		invalid : function() {
			},
		eachField: function(event, status, options) {
			if (!status.required || !status.pattern) {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
			}
		}
	});
	
	$('#country_sel').combobox({bsVersion: '3'});

/*	if (lawandgaugeapp.lang=='ru') {
		var placeholder = "Страна";
	} else {
		var placeholder = "Country";
	}*/
	var placeholder = "Страна";
	$(".combobox-container input[type=text]")
		.attr("placeholder", placeholder)
		.attr("data-required", "")
		.attr("name", "country_name");
	
	$('#l_reg_form').validate({
		sendForm : true,
	    onKeyup : true,
		valid : function() {
				sendEmail("L");
			},
		invalid : function() {
			},
		eachField: function(event, status, options) {
			if (!status.required || !status.pattern) {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
			}
		}
	});
	
	$(window).load(function() {
		drawCircles(2500);
		animateNumbers();
		selectContent(location.hash);
	});

	$(window).on("resize", limitExecByInterval(function(e) {
		drawCircles();
	}, 2000));
	
	function Resize() {
		if($(window).width()<1200) {
			return false;
		}
		
		var scale = Scale;
		var res = $(window).height()/$(window).width();

		if ($(window).height()<=MinHeight || $(window).width()<=MinWidth) {
			scale = MinWidth/DefWidth;
		} else {
			scale = $(window).height()/DefHeight;
		}
		
		scale_percent = parseInt(scale*100)-5;
		$(".scalable").css("font-size", scale_percent+"%");

		var Scale = scale;
	}
	
	MinHeight = 530;
	MinWidth = 1200;
	DefHeight = 1050;
	DefWidth = 1920;
	DefRes = DefHeight/DefWidth; //100% scale
	ScreensHeight = [ // screen height percent (window)
		1, // screen-one 100% height
		0.7, // screen-two 70% height
		1, // screen-three 100% height
		1.32, // screen-four normal height
		1
	];
	Scale = 1;
	
	$(window).bind('resize', Resize);
	$(window).resize();
	Resize();
	
	function selectContent(hash) {
		if (!hash) {
			hash="#/ru/clients";
			location.hash = hash;
		}
		
		var content = hash.split("/");
		content = content[content.length-1];
		
		var header = $("body");
		var content_clients = $("#content_clients");
		var content_lawyers = $("#content_lawyers");
		var fc = $(".for_clients");
		
		header.find(".header-btn").removeClass("selected");
		
		if (content=="clients") {
			
			resetLawyers();
			drawCircles();
			
			header.find(".header-btn[href*=clients]").addClass("selected");
			content_lawyers.hide();
			content_clients.fadeIn(1000);
			header.addClass("clients").removeClass("lawyers");
			fc.fadeIn(1000);
			
			setTimeout(function() {
				var logo_left = $("#logo").offset().left;
				var svg_left = $("#logo svg:first-child polygon:first-child")
					.offset().left;
				var dx = Math.abs(logo_left) - Math.abs(svg_left);
				
				if (dx!=0) {
					$("#logo svg")
					.css("transform", "translateX("+dx+"px)")
					.css("-webkit-transform", "translateX("+dx+"px)")
					.css("-moz-transform", "translateX("+dx+"px)")
					.css("-ms-transform", "translateX("+dx+"px)");
				}
			}, 100);
			
		} else if (content=="lawyers") {
			header.find(".header-btn[href*=lawyers]").addClass("selected");
			content_clients.hide();
			content_lawyers.fadeIn(1000);
			header.removeClass("clients").addClass("lawyers");
			
			setTimeout(function() {
				var dx = 0;
				
				$("#logo svg")
					.css("transform", "translateX("+dx+"px)")
					.css("-webkit-transform", "translateX("+dx+"px)")
					.css("-moz-transform", "translateX("+dx+"px)")
					.css("-ms-transform", "translateX("+dx+"px)");
			}, 100);
		}
		Resize();
	}
	
	function resetLawyers(){
		$(".sec1").removeClass("s4");
		$(".reg-btn").removeAttr("style");
		$(".reg-btn").removeClass("increase-white chide");
		$("#logo").removeClass("color");
		$(".step4").removeClass("active").removeAttr("style");
		$("#logo").fadeIn(1000);
		$(".step3, .step2").hide();
		$("#login").removeClass("dark");
		$(".lang").removeClass("dark");
		$(".reg-btn").removeClass("increase");
	}
	
	function l_step(step) {
		switch (step) {
		case "2":
			$(".reg-btn").addClass("increase");
			setTimeout(function() {
				$(".step2").fadeIn(1000);
			}, 500);
			break;
			
		case "3":
			$(".step:not(.step1)").hide();
			$(".step1").removeClass("cshow").addClass("chide");
			$(".step3").fadeIn(1000);
			break;
			
		case "4":
			var t = $(".reg-btn").offset();
			var trigger = document.getElementById("anm");
			
			$("#login").addClass("dark");
			$(".lang").addClass("dark");
			$("#logo").addClass("color");
			$(".step:not(.step1)").hide();
			$(".sec1").addClass("s4");
			
			setTimeout(function () {
				trigger.beginElement();
			},370);
			
			$(".reg-btn").css("top", t.top+"px").css("left", t.left+"px");
			
			var l = "0px";
			
			$(".reg-btn").addClass("increase-white")
				.css("width", ($(window).width())+"px")
				.css("height", "100%")
				.css("top", "0px")
				.css("left", l);
			
			setTimeout(function() {
				$(".step4").addClass("active").css("display", "block");
			}, 1000);
			break;
			
		default:
			break;
		}
	}

	function drawCircles(timeout) {
		if (!timeout) {
			timeout = 0;
		}
		
		if ($(document).width() > 1023) {
			var csize = 193;
			
			if ($(window).height() < 1000) {
				var circ_scale = parseInt($(window).height()*100/1000);
				var thickness = 3;
				
				csize = csize*circ_scale/100;
				
				$(".circle").css("width", csize).css("height", csize);
				$(".circle-border").css("border-width", thickness + "px");
				
			} else {
				var thickness = 5;
				
				$(".circle").removeAttr("style");
				$(".circle-border").removeAttr("style");
			}
			
		} else {
			var csize = parseInt($(document).width() / 4);
			
			if ($(window).height() < 1000) {
				var circ_scale = parseInt($(window).height()*100/1000)+15;
				
				csize = csize*circ_scale/100;
				
				if (csize<67) {
					csize = 67;
				}
			}
			
			var thickness = 3;

			$(".circle").css("width", csize).css("height", csize);
			$(".circle-border").css("border-width", thickness + "px");
		}
		
		if ($(".circle-border:nth-child(1) canvas").lengtn>0) {
			var old_size = $(this).width();
			
			if (( Math.abs(old_size) - Math.abs(csize) )<20) {
				return false;
			} else {
				alert("resize");
			}
		}
		
		if ( $(window).width() > $(window).height() ) {
			var space = parseInt(($(".info-bar").width() - (csize+10+5*2+thickness*2)*4)/6);
			
			$(".circle-border:nth-child(1)")
				.css("margin-left", "0px")
				.css("margin-right", space+"px");
			
			$(".circle-border:nth-child(4)")
				.css("margin-right", "0px")
				.css("margin-left", space+"px");
			
			$(".circle-border:nth-child(2), .circle-border:nth-child(3)")
				.css("margin-right", space+"px")
				.css("margin-left", space+"px");
			
			$(".circle-border")
				.css("border-width", thickness + "px")
				.css("float", "none");

			var top_ = $("#logo").offset().top+$("#logo").height();
			var bot = $(".sec1").height();
			var dy = bot - top_;
			var panel_height = $(".info-bar").height();
			
			if (panel_height<dy) {
				var mt = (dy - $(".slogan.for_clients").height() 
						- panel_height)/2 + $(".slogan.for_clients").height();
				
			$(".info-bar").css("margin-top", mt+"px");
			
			}
			
		} else {
			$(".circle-border").removeAttr("style");
			$(".circle-border").css("border-width", thickness + "px");
		}
		
		$('.circle1').circleProgress({
			value : 0.4,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});
		
		$('.circle2').circleProgress({
			value : 0.75,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});
		
		$('.circle3').circleProgress({
			value : 0.75,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});
		
		$('.circle4').circleProgress({
			value : 0.4,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});

	}

	function animateNumbers() {
		$(".circle-border:nth-child(1) .val").prop('number', 0).animateNumber({
			number : 87,
			easing : 'easeOutCirc'
		}, 2500);
		
		$(".circle-border:nth-child(2) .val").prop('number', 0).animateNumber({
			number : 57,
			easing : 'easeOutCirc'
		}, 2500);
		
		$(".circle-border:nth-child(3) .val").prop('number', 0).animateNumber({
			number : 59,
			easing : 'easeOutCirc'
		}, 2500);
		
		$(".circle-border:nth-child(4) .val").prop('number', 0).animateNumber({
			number : 118,
			easing : 'easeOutCirc'
		}, 2500);
	}

	function selectLang() {
		/*var wrapper = $(".lang");
		wrapper.find("div").each(function() {
			$(this).toggleClass("selected");
		});*/
	}

	function limitExecByInterval(fn, time) {
		var lock, execOnUnlock, args;
		return function() {
			args = arguments;
			if (!lock) {
				lock = true;
				var scope = this;
				setTimeout(function() {
					lock = false;
					if (execOnUnlock) {
						args.callee.apply(scope, args);
						execOnUnlock = false;
					}
				}, time);
				return fn.apply(this, args);
			} else
				execOnUnlock = true;
		}
	}

	function sendEmail(target) {
		if (target=="L") {
			url = "./lawyers_send.php"
			var msg = $("#l_reg_form").serialize();
			var frm = $("#l_reg_form");
		} else {
			url = "./send.php"
			var msg = $("#form").serialize();
			var frm = $("#form");
		}
		
		$.ajax({
			type : "POST",
			url : url,
			data : msg,
			success : function(data) {
				if (data == 1) {
					frm.addClass("success");
					frm.removeClass("error");
				} else if (data == 0) {
					frm.addClass("error");
					frm.removeClass("success");
				}
			},
			error : function(xhr, str) {
				alert("Возникла ошибка!");
			}
		});
	}
	
	
	$(".yes-btn").on("click", function(e) {
		l_step("4");
		e.stopPropagation();
	});
	
	$(".no-btn").on("click", function(e) {
		l_step("3");
		e.stopPropagation();
	});
	
	$(".reg-btn").on("click", function(e) {
		l_step("2");
		
	});
	
	$(window).on('hashchange', function() {
	     selectContent(location.hash);
	});

	$(".lang div").on("click", function() {
		selectLang();
	});

	$("#carousel1").swiperight(function() {
		$(this).carousel('prev');
	});
	
	$("#carousel1").swipeleft(function() {
		$(this).carousel('next');
	});
	
	$("#login-form").on("submit", function(e) {
		e.preventDefault();
	});
	
	$("#login-btn").on("click", function() {
		$("#login").toggleClass("show");
	});
	
	$(document).mouseup(function (e) {
	    var container = $("#login");
	    var container1 = $("#login-btn");
	    
	    if (container.has(e.target).length === 0 && container1.has(e.target).length === 0) {
	        container.removeClass("show");
	    }
	});
	console.log("main.js loaded");
}
initLand();