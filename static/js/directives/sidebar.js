'use strict';

/**
 * @ngdoc directive
 * @name purinaApp.directive:footer
 * @description
 * # footer
 */
angular.module('lawgauge')
  .directive('sidebar', function () {
    return {
      restrict: 'A',
      templateUrl:'/angular/templates/sidebar/',
      link: function postLink() {

      }
    };
  });