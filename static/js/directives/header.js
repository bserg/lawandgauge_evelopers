'use strict';

/**
 * @ngdoc directive
 * @name purinaApp.directive:footer
 * @description
 * # footer
 */
angular.module('lawgauge')
    .directive('mainheader', function () {
	    return {
	      restrict: 'A',
	      templateUrl:'/angular/templates/header',
	    }
});

angular.module('lawgauge')
    .directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished', element, attr);
                });
            }
        }
    }
});