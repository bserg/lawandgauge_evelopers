angular.module('lawgauge')
	.directive('dropzone', function () {
  return function (scope, element, attrs) {
    var config, dropzone;
 
    config = scope[attrs.dropzone];
 
    // create a Dropzone for the element with the given options
    dropzone = new Dropzone(element[0], config.options);
    // bind the given event handlers
    angular.forEach(config.eventHandlers, function (handler, event) {
      dropzone.on(event, handler);
    });
  };
});


angular.module('lawgauge')
	.directive('dragEnter', function(){
	  return function($scope, $element){
	    $element.bind('dragenter', function(){
	    	$scope.showDropzone = true;
	    	$scope.$apply();
	    	console.log('dragenter');
	    });
	  };
	});

angular.module('lawgauge')
	.directive('dragLeave', function(){
	  return function($scope, $element){
	    $element.bind('dragleave', function(){
	    	$scope.showDropzone = false;
	    	$scope.$apply();
	    	console.log('dragleave');
	    });
	  };
	});
angular.module('lawgauge')
	.directive('clickAnywhereButHere', ['$document', function ($document) {
	    return {
	        link: function postLink(scope, element, attrs) {
	            var onClick = function (event) {
	                var isChild = $(element).has(event.target).length > 0;
	                var isSelf = element[0] == event.target;
	                var isIgnore = $(event.target).hasClass("ignoreClickOutside");
	                var isInside = isChild || isSelf || isIgnore;
	                if (!isInside) {
	                    scope.$apply(attrs.clickAnywhereButHere)
	                }
	            }
	            scope.$watch(attrs.isActive, function(newValue, oldValue) {
	                if (newValue !== oldValue && newValue == true) {
	                    $document.bind('click', onClick);
	                }
	                else if (newValue !== oldValue && newValue == false) {
	                    $document.unbind('click', onClick);
	                }
	            });
	        }
	    };
}]);