"use strict";

angular.module("lawgauge")
    .factory('socketJs', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
      
        return {
            oNconnect: (function (data) {

                var deferred = $q.defer();

                sock.onconnect = function(data) {
                    if (data.error) {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(data);
                    }
                };

                return deferred.promise;
            })(),
            oNopen: (function (data) {

                var deferred = $q.defer();

                sock.onopen = function (data) {
                    if (data.error) {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(data);
                    }
                };

                return deferred.promise;
            })(),
            oNmessage: (function (data) {

                var deferred = $q.defer();

                sock.onmessage = function (data) {
                    console.log(data);
                     if(data.type == 'message') {
                          deferred.resolve(data.data);
                        } else { 
                          deferred.reject(data.data);
                        }
                };
                return deferred.promise;
            })(),
            oNclose: (function (data) {

                var deferred = $q.defer();

                sock.onclose = function (data) {
                    if (data.error) {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(data);
                    }
                };

                return deferred.promise;
            })(),
            oNopenchat: function (data) {

                var deferred = $q.defer();

                sock.send(JSON.stringify({
                    type: "connect",
                    chat: data
                }));

                return deferred.promise;
            },
            oNsendmessage: function (data) {

                var deferred = $q.defer();

                sock.send(JSON.stringify({
                  type: 'message',
                  text: data
              }));
                return deferred.promise;
            }
        };
    }]);

