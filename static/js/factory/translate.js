'use strict';
angular.module('lawgauge')
.factory('translateService', function(){
    return{
        t: function(phrase) {
        if (!$scope.translate) {
            return false;
        }

        var result = "";

        if ($scope.translate.hasOwnProperty(phrase)) {
            var _result = $scope.translate[phrase];
            if (_result.hasOwnProperty($scope.user.lang)) {
                result = _result[$scope.user.lang];
            }
        }

        if (!result) {
            result = phrase;
        }

        var resultParsed = result.split(/\[\[(.*?)\]\]/);

        if((typeof resultParsed != "string") && resultParsed.length > 1) {
            var parsed = "";

            for (var key in resultParsed) {
                if (resultParsed.hasOwnProperty(key)) {
                    if (resultParsed[key].indexOf("!") > -1) {

                        var varName = resultParsed[key].split("!")[1];
                        if (varName.indexOf(".") > -1) {

                            varName = varName.split(".");
                            var varValue = $scope[varName[0]];
                            varValue = varValue[varName[1]];
                            parsed += varValue;

                        } else {
                            parsed += $scope[varName];
                        }

                    } else {
                        parsed += resultParsed[key];
                    }
                }
            }
            result = parsed;
        }

        return result;
    }
    };
});