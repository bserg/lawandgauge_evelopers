"use strict";

angular.module("black_wood")
    .factory('vkAPI', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {

    VK.init({
        apiId: 4847239
      });



        return {
            getLog: (function (data) {

                var deferred = $q.defer();

                VK.Auth.getLoginStatus(function(data) {
                    if (data.error) {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(data);
                    }
                });

                return deferred.promise;
            })(),
            getAuth: (function (data) {

                var deferred = $q.defer();

                VK.Auth.login(function(data) {
                    if (data.error) {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(data);
                    }
                });

                return deferred.promise;
            })(),
            getProfile: function (id) {

                var deferred = $q.defer();

                VK.api("users.get", { uid: id, fields: 'sex, photo_200_orig'}, function (data) {
                    if (data.error) {
                        deferred.reject(data);
                    } else {
                        deferred.resolve(data.response[0]);
                    }
                });

                return deferred.promise;

            }
        };
    }]);

