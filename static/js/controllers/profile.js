'use strict';

/* Controllers */
/*angular.module('lawgauge', ['ipCookie']);*/
angular.module('lawgauge')
  .filter('reverse', function() {
    return function(items) {
      if (items) {
        return items.slice().reverse();
      }
    };
  });
angular.module('lawgauge')
  .controller('profileCtrl', ['$scope', '$http', '$rootScope', '$cookies', function($scope, $http, $rootScope, $cookies) {

    /*predefined*/
    $scope.defaultLicenseFileName = "Attach";
    $scope.activeTab = "personal-info";
    $scope.temp = {};
    $scope.temp.licenseFileName = $scope.defaultLicenseFileName;
    $scope.temp.diplomaFileName = $scope.defaultLicenseFileName;
    $scope.temp.resumeFileName = $scope.defaultLicenseFileName;
    /* ========== */

    /* jquery */
    
    var fbAppId;
    var vkAppId;
    if (location.hostname == "lawandgauge.webtm.ru") {
      fbAppId = '1427231024251881';
      vkAppId = '4918770';
    } else {
      fbAppId = '360246200839744';
      vkAppId = '4918770';
    }

    VK.init({
      apiId: vkAppId
    });

    window.fbAsyncInit = function() {
      FB.init({
        appId: fbAppId,
        xfbml: true,
        version: 'v2.3'
      });
    };
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(".custom-select").on("focus", function() {
      $(this).addClass("active");
    });

    $(".custom-select").on("keyup", function(e) {

      $(this).trigger("focus");

      var elements = $(this).siblings(".custom-select-list").find("li");
      var i;

      if (!elements.filter(".active").length) {
        i = -1;
      } else {
        console.log($(elements.filter(".active")[0]).index());
        i = $(elements.filter(".active")[0]).index();
      }

      if (e.keyCode == 40) {
        if (i == elements.length - 1) {
          i = -1;
        }
        i++;
      } else if (e.keyCode == 38) {
        if (i == 0) {
          i = elements.length;
        }
        i--;
      }

      $(elements).removeClass('active');
      $(elements[i]).addClass('active');

      if (e.keyCode == 13) {
        $(elements.filter(".active")[0]).trigger("click");
      }

    });

    $(document).ready(function() {
      var telInput = $("input[name=phone]");

      telInput.intlTelInput("destroy");
      telInput.intlTelInput({
        utilsScript: "",
        defaultCountry: "auto",
        nationalMode: false,
        autoHideDialCode: false,
        autoPlaceholder: false
      });
    });
    
    $(document).mousedown(function(event) {
      var container = $(".custom-select");
      var container1 = $(".custom-select-list");

      if ((!$(container).is($(event.target)) && !$(event.target).closest($(container)).length) &&
        (!$(container1).is($(event.target)) && !$(event.target).closest($(container1)).length)) {
        $(container).removeClass('active');
      } else {
        container.each(function(i, el) {
          if (el == event.target) {} else {
            $(el).removeClass('active');
          }
        });
      }

    });

    $("form").on('submit', function(e) {
      e.preventDefault();
    });
    
    $(".tip_ac").on("change", function() {
      if (!$scope.temp.currentCurrency) {
        $(".switcher-wrapper .switch:nth-child(1)").trigger("click");
      }
    });

    /* ========== */

    
    
    $scope.licenseFileNameChanged = function(files) {
      if (files) {
        $scope.temp.licenseFileName = files[0].name;
        $scope.$apply();
      }
    };

    $scope.diplomaFileNameChanged = function(files) {
      if (files) {
        $scope.temp.diplomaFileName = files[0].name;
        $scope.$apply();
      }
    };

    $scope.resumeFileNameChanged = function(files) {
      if (files) {
        $scope.temp.resumeFileName = files[0].name;
        $scope.$apply();
        $scope.updateProfessionalInfo();
      }
    };

/*    $http.get('/localization/locales/').success(function(data) {
      $scope.locales = data.data;
      console.log(data);
    });*/


    //$scope.getTranslations();

    $scope.t = function(phrase) {
      if (!$scope.translate) {
        return false;
      }

      var result = "";

      if ($scope.translate.hasOwnProperty(phrase)) {
        var _result = $scope.translate[phrase];
        if (_result.hasOwnProperty($scope.user.lang)) {
          result = _result[$scope.user.lang];
        }
      }

      if (!result) {
        result = phrase;
      }

      var resultParsed = result.split(/\[\[(.*?)\]\]/);

      if((typeof resultParsed != "string") && resultParsed.length > 1) {
        var parsed = "";

        for (var key in resultParsed) {
          if (resultParsed.hasOwnProperty(key)) {
            if (resultParsed[key].indexOf("!") > -1) {

              var varName = resultParsed[key].split("!")[1];
              if (varName.indexOf(".") > -1) {

                varName = varName.split(".");
                var varValue = $scope[varName[0]];
                varValue = varValue[varName[1]];
                parsed += varValue;

              } else {
                parsed += $scope[varName];
              }

            } else {
              parsed += resultParsed[key];
            }
          }
        }
        result = parsed;
      }

      return result;
    };

    $scope.getServices = function() {
      $http.get('/account/services/').success(function(data) {
        $scope.temp.areas = data.data;

        var areas = $scope.temp.areas;
        var vals = [];
        for (var key in areas) {
          vals.push({
            label: areas[key].name,
            value: areas[key].name,
            id: areas[key].id
          });
        }

        $(".area_ac").autocomplete({
          source: vals,
          minLength: 0,
          position: {
            my: "left top-3",
            at: "left bottom"
          },
          select: function(event, ui) {

            var area = "";
            for (var key in areas) {
              if (areas[key].id == ui.item.id) {
                area = areas[key];
                break;
              }
            }
            $scope.selectArea(area);
          }
        });

        $(".area_ac").on("focus", function() {
          $($(this)).autocomplete("search", "");
        }).on("blur", function() {
          $($(this)).autocomplete("close");
        });
      });
    };
    $scope.getServices();
    
    $http.get('/account/currencies/').success(function(data) {
      $scope.temp.currencies = data.data;
    });

    $scope.getCountries = function() {
      $http.get('/map/countries/').success(function(data) {

        $scope.countries = data.data;
        $scope.postCountries = data.data;

        $scope.countries.forEach(function(item, i, arr) {
          if (item.id == $scope.user.country) {
            $scope.selectCountry(item);
          }
        });
        $scope.postCountries.forEach(function(item, i, arr) {
          if (item.id == $scope.user.address.country) {
            $scope.selectPostCountry(item);
          }
        });
        if ($scope.user.address) {
          $http({
            url: '/map/cities/',
            method: "GET",
            params: {
              country: $scope.user.address.country
            }
          }).success(function(data) {

            $scope.postCities = data.data;
            $scope.cities = data.data;

            $scope.cities.forEach(function(item, i, arr) {
              if (item.id == $scope.user.city) {
                $scope.selectCity(item);
              }
            });
            $scope.postCities.forEach(function(item, i, arr) {
              if (item.id == $scope.user.address.city) {
                $scope.selectPostCity(item);
              }
            });

            /*get user post address country name by id*/
            $scope.user.address.country_name = $scope.getNameById($scope.user.address.country, $scope.countries);
            /*get user post address city name by id*/
            $scope.user.address.city_name = $scope.getNameById($scope.user.address.city, $scope.postCities);
          });
        } else {
          $scope.user.address = {};
        }

        var countries = $scope.countries;
        var vals = [];
        for (var key in countries) {
          vals.push({
            label: countries[key].name,
            value: countries[key].name,
            id: countries[key].id
          });
        }

        $.extend($.ui.autocomplete.prototype.options, {
          open: function(event, ui) {
            $(this).autocomplete("widget").css({
              "width": ($(this).outerWidth() + "px")
            });
          }
        });

        $(".country_ac").autocomplete({
          source: vals,
          minLength: 0,
          position: {
            my: "left top-3",
            at: "left bottom"
          },
          select: function(event, ui) {

            var country = "";
            for (var key in $scope.countries) {
              if ($scope.countries[key].id == ui.item.id) {
                country = $scope.countries[key];
                break;
              }
            }

            if ($(event.target).hasClass("country_post")) {
              $scope.selectPostCountry(country);
            } else {
              $scope.selectCountry(country)
            }
          }
        });

        $(".country_ac, .city_ac").on("focus", function() {
          $($(this)).autocomplete("search", "");
        }).on("blur", function() {
          $($(this)).autocomplete("close");
        });
      });
    };
    $scope.getCountries();

    $scope.getNameById = function(id, arr) {
      var result = "";
      arr.forEach(function(item, i, arr) {
        if (item.id == id) {
          result = item.name;
        }
      });
      return result;
    };

    /*personal info tab update functions*/
    $scope.updatePersonalInfo = function() {

      //$scope.updateAvatar();
      $scope.updatePostal();

      var frm = $("#form-personal-info");
      var formData = new FormData(frm.get(0));

      $http.post('/account/cabinet/main_info/', formData, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).success(function(data) {
        console.log(data);
        $scope.user.avatar = data.avatar_url;

      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.updatePostal = function() {

      var frm = $("#form-postal-info");
      var formData = new FormData(frm.get(0));

      $http.post('/account/cabinet/address/', formData, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).success(function(data) {
        console.log(data);

      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.updateAvatar = function() {
        var files = $("input[name=avatar]")[0].files;

        if (files) {
          var token = $cookies['csrftoken'];

          var formData = new FormData;
          var file = files[0];
          formData.append('avatar', file);

          $http.post('/account/cabinet/avatar/', formData, {
            transformRequest: angular.identity,
            headers: {
              'Content-Type': undefined
            }
          }).success(function(data) {
            console.log(data);
            $scope.user.avatar = data.avatar_url;

          }).error(function(data) {
            console.log(data);
          });
        }
      };
      /*============*/

    /*career info update functions*/
    $scope.updateProfessionalInfo = function() {

      var frm = $("#form-professional-info");
      var formData = new FormData(frm.get(0));

      $http.post('/account/cabinet/professional_info/', formData, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).success(function(data) {
        console.log(data);
        $scope.updateUser();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.addDiploma = function() {

      var frm = $("#form-diplomas");
      var files = $("#form-diplomas input[type=file]")[0].files;

      if (files) {
        var formData = new FormData;

        formData.append("file", files[0]);
        formData.append("name", files[0].name);

        $http.post('/account/cabinet/diplom/add/', formData, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': undefined
          }
        }).success(function(data) {
          console.log(data);
          frm.get(0).reset();
          $scope.temp.diplomaFileName = $scope.defaultLicenseFileName;
          $scope.updateUser();

        }).error(function(data) {
          console.log(data);
        });
      }
    };

    $scope.deleteDiploma = function(url) {
      $http.post(url).success(function(data) {
        console.log(data);
        $scope.updateUser();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.addLicense = function() {

      var frm = $("#form-licenses");
      var files = $("#form-licenses input[type=file]")[0].files;

      if (files) {
        var formData = new FormData;

        formData.append("file", files[0]);
        formData.append("name", files[0].name);

        $http.post('/account/cabinet/license/add/', formData, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': undefined
          }
        }).success(function(data) {
          console.log(data);
          frm.get(0).reset();
          $scope.temp.licenseFileName = $scope.defaultLicenseFileName;
          $scope.updateUser();

        }).error(function(data) {
          console.log(data);
        });
      }
    };

    $scope.deleteLicense = function(url) {
      $http.post(url).success(function(data) {
        console.log(data);
        $scope.updateUser();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.addAssociation = function() {

      var frm = $("#form-associations");
      var name = $("#form-associations input[name=association]").val();

      if (name) {
        var formData = new FormData;

        formData.append("name", name);

        $http.post('/account/cabinet/association/add/', formData, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': undefined
          }
        }).success(function(data) {
          console.log(data);
          $scope.updateUser();
          $scope.temp.inputAssociation = "";
        }).error(function(data) {
          console.log(data);
        });
      }
    };

    $scope.deleteAssociation = function(url) {
      $http.post(url).success(function(data) {
        console.log(data);
        $scope.updateUser();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.clearResume = function() {
      if ($scope.user.resume) {
        $http.post($scope.user.resume.delete_url).success(function(data) {
          console.log(data);
          $scope.updateUser();
        }).error(function(data) {
          console.log(data);
        });
      }
    };

    $scope.updateCareer = function() {
        $scope.updateProfessionalInfo();
      };
      /*============*/

    $scope.updateUser = function() {
      $http.get('/account/data/').success(function(data) {
        if (data.success == true) {
          $scope.isUser = true;
          $rootScope.user = data;
          console.log($scope.user);
          if ($scope.user.phone) {
            $("input[name=phone]").intlTelInput("setNumber", $scope.user.phone);
          }
          
          if ($scope.user.resume) {
            $scope.temp.resumeFileName = $scope.user.resume.name;
          } else {
            $scope.temp.resumeFileName = $scope.defaultLicenseFileName;
          }

          if (!$scope.user.address) {
            $scope.user.address = {};
          }

          $scope.getTranslations();

          console.log("User info updated");
        }



      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.selectCountry = function(country) {
      $(".custom-select").removeClass("active");
      $("input[name=countryName]").removeClass("error");

      $scope.user.country = country.id;
      $scope.user.country_name = country.name;

      $http({
        url: '/map/cities/',
        method: "GET",
        params: {
          country: country.id
        }
      }).success(function(data) {
        $scope.cities = data.data;
        $scope.cities.forEach(function(city, i, arr) {
          if (city.id == $scope.user.city) {
            $scope.selectCity(city);
          }
        });

        var cities = $scope.cities;
        var vals = [];
        for (var key in cities) {
          vals.push({
            label: cities[key].name,
            value: cities[key].name,
            id: cities[key].id
          });
        }

        $(".city_ac:not(.city_post)").autocomplete({
          source: vals,
          minLength: 0,
          position: {
            my: "left top-3",
            at: "left bottom"
          },
          select: function(event, ui) {

            var city = "";
            for (var key in $scope.cities) {
              if ($scope.cities[key].id == ui.item.id) {
                city = $scope.cities[key];
                break;
              }
            }

            $scope.selectCity(city);
          }
        });

      });
    };

    $scope.selectPostCountry = function(country) {
      $("input[name=countryName]").removeClass("error");
      $scope.user.address.country_name = $scope.getNameById(country.id, $scope.countries);;
      $scope.user.address.country = country.id;
      $http({
        url: '/map/cities/',
        method: "GET",
        params: {
          country: country.id
        }
      }).success(function(data) {
        $scope.postCities = data.data;

        var cities = $scope.postCities;
        var vals = [];
        for (var key in cities) {
          vals.push({
            label: cities[key].name,
            value: cities[key].name,
            id: cities[key].id
          });
        }

        $(".city_ac.city_post").autocomplete({
          source: vals,
          minLength: 0,
          position: {
            my: "left top-3",
            at: "left bottom"
          },
          select: function(event, ui) {

            var city = "";
            for (var key in $scope.postCities) {
              if ($scope.postCities[key].id == ui.item.id) {
                city = $scope.postCities[key];
                break;
              }
            }
            console.log("city: ", city);
            $scope.selectPostCity(city);
          }
        });
      });
    };

    $scope.selectCity = function(city) {
      $("input[name=cityName]").removeClass("error");
      /*$scope.currentCity = city;*/
      $scope.user.city = city.id;
      $scope.user.city_name = city.name;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };

    $scope.selectPostCity = function(city) {
      $("input[name=cityName]").removeClass("error");
      $scope.user.postCity = city;
      $scope.user.address.city = city.id;
      $scope.user.address.city_name = city.name;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
      console.log(city);
      console.log($scope.user);
    };

    $scope.selectArea = function(area) {
      $scope.temp.currentArea = area;
      if (!$scope.$$phase) {
        $scope.$apply();
      }

      var institutes = $scope.temp.currentArea.institutes;
      var vals = [];
      for (var key in institutes) {
        vals.push({
          label: institutes[key].name,
          value: institutes[key].name,
          id: institutes[key].id
        });
      }

      $(".institute_ac").autocomplete({
        source: vals,
        minLength: 0,
        position: {
          my: "left top-3",
          at: "left bottom"
        },
        select: function(event, ui) {

          var institute = "";
          for (var key in institutes) {
            if (institutes[key].id == ui.item.id) {
              institute = institutes[key];
              break;
            }
          }
          $scope.selectInstitute(institute);
        }
      });

      $(".institute_ac").on("focus", function() {
        $($(this)).autocomplete("search", "");
      }).on("blur", function() {
        $($(this)).autocomplete("close");
      });

    };

    $scope.selectInstitute = function(institute) {
      $scope.temp.currentInstitute = institute;
      if (!$scope.$$phase) {
        $scope.$apply();
      }

      var tips = $scope.temp.currentInstitute.service_tips;
      var vals = [];
      for (var key in tips) {
        vals.push({
          label: tips[key].name,
          value: tips[key].name,
          id: tips[key].id
        });
      }

      $(".tip_ac").autocomplete({
        source: vals,
        minLength: 0,
        position: {
          my: "left top-3",
          at: "left bottom"
        },
        select: function(event, ui) {

          var tip = "";
          for (var key in tips) {
            if (tips[key].id == ui.item.id) {
              tip = tips[key];
              break;
            }
          }
          $scope.selectTip(tip);
        }
      });

      $(".tip_ac").on("focus", function() {
        $($(this)).autocomplete("search", "");
      }).on("blur", function() {
        $($(this)).autocomplete("close");
      });

      setTimeout(function() {
        $(".switch:nth-child(1)").click();
      }, 0);
    };

    $scope.selectTip = function(tip) {
      $scope.temp.currentTip = tip;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };

    $scope.selectCurrency = function(currency, $event) {
      $scope.temp.currentCurrency = currency;
      $(".switch").removeClass("selected");
      var element;
      if ($($event.target).hasClass("switch")) {
        element = $($event.target);
      } else {
        element = $($event.target).parents(".switch");
      }
      element.addClass("selected");
    };

    $scope.addService = function($event) {
      if (!$($event.currentTarget).hasClass('active')) {
        return false;
      }
      var formData = new FormData;

      if (!$scope.priceFrom) {
        console.log($scope.priceFrom);
        $scope.priceFrom = 0;
      }

      if (!$scope.priceTo) {
        console.log($scope.priceTo);
        $scope.priceTo = 0;
      }
      
      if (!$scope.temp.currentArea) {
        $scope.temp.currentArea = {name: ""};
      }
      if (!$scope.temp.currentInstitute) {
        $scope.temp.currentInstitute = {name: ""};
      }
      
      if ($(".area_ac").val() !=  $scope.temp.currentArea.name && $(".area_ac").val() != "") {
        
        console.log("area_ac", $(".area_ac").val());
        formData.append('branch_name', $(".area_ac").val());
        
        console.log("institute_ac", $(".institute_ac").val());
        formData.append('institute_name', $(".institute_ac").val());
        
      } else {
        
        formData.append('branch', $scope.temp.currentArea.id);
        
        if ($(".institute_ac").val() !=  $scope.temp.currentInstitute.name && $(".institute_ac").val() != "") {
          console.log("institute_ac", $(".institute_ac").val());
          formData.append('institute_name', $(".institute_ac").val());
        } else {
          console.log("institute id", $scope.temp.currentInstitute.id);
          formData.append('parent', $scope.temp.currentInstitute.id);
        }
        
      }
      
      formData.append('name', $scope.temp.currentTip.name);
      formData.append('price_min', $scope.priceFrom);
      formData.append('price_max', $scope.priceTo);
      formData.append('currency', $scope.temp.currentCurrency.id);

      $http.post('/account/cabinet/service/add/', formData, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).success(function(data) {
        console.log(data);
        $scope.updateUser();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.deleteService = function(url) {
      $http.post(url).success(function(data) {
        console.log(data);
        $scope.updateUser();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.setNotifications = function() {
      var frm = $("#form-notifications");
      var formData = new FormData(frm.get(0));

      $http.post('/account/cabinet/notifications/', formData, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).success(function(data) {
        console.log(data);
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.langUpdate = function() {

      $scope.locales = [
        {"id": 1, "code": "en", "name": $scope.translate["en"][$scope.user.lang]},
        {"id": 2, "code": "ru", "name": $scope.translate["ru"][$scope.user.lang]}
      ];

      $scope.defaultLicenseFileName = $scope.translate['attach'][$scope.user.lang];
      console.log("change defaultLicenseFileName", $scope.defaultLicenseFileName, $scope.user.lang);
      var formData = new FormData;
      formData.append("lang", $scope.user.lang);

      $http.post('/localization/lang/', formData, {
        transformRequest: angular.identity,
        headers: {
          'Content-Type': undefined
        }
      }).success(function(data) {
        console.log(data);
        $scope.getTranslations();
        $scope.getServices();
        $scope.getCountries();
      }).error(function(data) {
        console.log(data);
      });
    };

    $scope.avatarChanged = function(files) {
      var reader = new FileReader();
      var container = $(".panel-avatar");

      reader.onloadend = function() {
        container.css("background-image", reader.result);
        $scope.user.avatar = reader.result;
        $scope.$apply();
        console.log(reader.result);
      };
      if (files) {
        reader.readAsDataURL(files[0]);
      } else {

      }
    };

    $scope.addVk = function() {
      VK.Auth.login(function(r) {
        console.log(r);

        var formData = new FormData;
        formData.append("user_id", r.session.mid);

        $http.post('/account/cabinet/vk/', formData, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': undefined
          }
        }).success(function(data) {
          console.log(data);
          $scope.updateUser();
        }).error(function(data) {
          console.log(data);
        });
      });
    };

    $scope.addFb = function() {
      FB.login(function(r) {
        console.log(r);

        var formData = new FormData;
        formData.append("user_id", r.authResponse.userID);
        formData.append("access_token", r.authResponse.accessToken);

        $http.post('/account/cabinet/fb/', formData, {
          transformRequest: angular.identity,
          headers: {
            'Content-Type': undefined
          }
        }).success(function(data) {
          console.log(data);
          $scope.updateUser();
        }).error(function(data) {
          console.log(data);
        });

      });
    };

    $scope.updateUser();

/*    $scope.getTranslation = function() {
      $http.get('/localization/translations/').success(function(data) {
        $scope.translation = data.data;
      });
    };*/

    /*      $scope.t = function(text) {
            console.log(1);
            return $scope.translation[text];
          }*/
  }]);