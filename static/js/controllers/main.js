'use strict';

/* Controllers */
/*angular.module('lawgauge', ['ipCookie']);*/
angular.module('lawgauge')
.controller('MainCtrl', ['$scope', '$http', '$rootScope', '$cookies', '$route', function ($scope, $http, $rootScope, $cookies, $route) {
	var fbAppId;
	var vkAppId;
	
	if (location.hostname == "lawandgauge.webtm.ru") {
		fbAppId = '1427231024251881';
		vkAppId = '4918770';
	} else {
		fbAppId = '360246200839744';
		vkAppId = '4976598';
	}
	
	VK.init({ apiId: vkAppId });
	
  window.fbAsyncInit = function() {
    FB.init({
      appId      : fbAppId,
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));



  $scope.user = {
    id: '',
    name: '',
    avatar: ''
  };

  $http.get('/account/data/').success(function (data) {
      if (data.success == true) {
        $scope.isUser = true;
        $scope.user = data;
        $scope.getchats();

      }
      
      if (!$scope.user.is_active) {
    	  /*alert($scope.user.id);*/
    	  /*return false;*/
  	      location.hash = "";
  	      location.pathname = "/";
      }
      
    }).error(function (data) {
      console.log(data);
    });
    
  $scope.main = {
    togl: false
  };
  $scope.authObject = {
    tell: '',
    pass: ''
  };
		$scope.getTranslations = function() {
			$http({
				url: '/localization/translations/',
				method: "GET",
				params: {
					use_slug: true
				}
			}).success(function(data) {
				console.log(data.data);
				$rootScope.translate = data.data;

				$scope.locales = [
					{"id": 1, "code": "en", "name": $scope.translate["en"][$scope.user.lang]},
					{"id": 2, "code": "ru", "name": $scope.translate["ru"][$scope.user.lang]}
				];

				$rootScope.menu = [
					{
						link: '#/consults',
						name: $scope.translate['consultations'][$scope.user.lang]
					},
					/*{
					 link: '#/money',
					 name: 'Деньги'
					 },
					 {
					 link: '#/contract',
					 name: 'Договор'
					 },
					 {
					 link: '#/setings',
					 name: 'Настройки'
					 },
					 {
					 link: '#/services',
					 name: 'Услуги'
					 }*/
				];

			}).error(function(data) {
				console.log(data);
			});

		};
		$scope.getTranslations();

		$scope.t = function(phrase) {
			if (!$scope.translate) {
				return false;
			}

			var result = "";

			if ($scope.translate.hasOwnProperty(phrase)) {
				var _result = $scope.translate[phrase];
				if (_result.hasOwnProperty($scope.user.lang)) {
					result = _result[$scope.user.lang];
				}
			}

			if (!result) {
				result = phrase;
			}

			var resultParsed = result.split(/\[\[(.*?)\]\]/);

			if((typeof resultParsed != "string") && resultParsed.length > 1) {
				var parsed = "";

				for (var key in resultParsed) {
					if (resultParsed.hasOwnProperty(key)) {
						if (resultParsed[key].indexOf("!") > -1) {

							var varName = resultParsed[key].split("!")[1];
							if (varName.indexOf(".") > -1) {

								varName = varName.split(".");
								var varValue = $scope[varName[0]];
								varValue = varValue[varName[1]];
								parsed += varValue;

							} else {
								parsed += $scope[varName];
							}

						} else {
							parsed += resultParsed[key];
						}
					}
				}
				result = parsed;
			}

			return result;
		};
  $scope.login = function () {
    if($scope.authObject.tell) {
      if($scope.authObject.pass) {
        $http({
            method: 'POST',
            url: '/account/login/',
            data: $.param({ 
              username: $scope.authObject.tell,
              password: $scope.authObject.pass
          }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .success(function (data) {
          $http.get('/account/data/').success(function (data) {
            if (data.success == true) {
              $scope.isUser = true;
              $scope.user.id = data.id;
              $scope.user.name = data.name;
              $scope.user.avatar = data.avatar;
              $scope.getchats();
              location.reload();

            }
            else {
              alert(data.error);
            }
          }).error(function (data) {
          });
        }).error(function (data) {
        });
      }
      else {
        alert('Обязательное поле');
      }
    }
    else {
      alert('Обязательное поле');
    }
  };
  
  function fbLogoutUser() {
	    FB.getLoginStatus(function(response) {
	        if (response && response.status === 'connected') {
	            FB.logout(function(response) {
	            });
	        }
	    });
	}
  
  $scope.logout = function () {
    $http.post('/account/logout/').success(function (data) {
    	VK.Auth.logout();
    	fbLogoutUser();
      $scope.user = {
        id: '',
        name: '',
        avatar: ''
      };
      $scope.isUser = false;
      $scope.yourchats = [];
      
      setTimeout(function() {
        location.hash = "";
        location.pathname = "/";
      }, 200);
      
    }).error(function (data) {
       console.log(data);
    });
  }

  
  $scope.show_auth = function () {
    if ($scope.main.togl == false) {
      $scope.main.togl = true;
    }
    else {
      $scope.main.togl = false;
    }
  };
  
  $scope.getchats = function () {
    $http.get('/chat/data/').success(function (data) {
      $scope.yourchats = data.data;
      /*$scope.thisChat = $scope.yourchats[0];*/
      console.log($scope.yourchats);
    }).error(function (data) {});
  };

	$scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent, element, attrs) {
		document.getElementsByClassName("chat_block_list")[0].scrollTop = 
			document.getElementsByClassName("chat_block_list")[0].scrollHeight;
		console.log(ngRepeatFinishedEvent.targetScope.message.date.split(" "));
	});
	
	$scope.parseLinks = function(text) {
	  var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	  return text.replace(exp, "<a href='$1'>$1</a>");
	};
	
	$scope.getDividerDate = function(date) {
		var ds = date.split(" ");
		return ds[0];
	}
	
	$scope.updateChat = function(chatUid, chatTitle, members) {
		var participans = [];
		for (var key in members) {
			participans.push("participans=" + key);
		}
		
		var serial_participans = participans.join("&"); 
		var fdata = "title=" + chatTitle + "&" + serial_participans;
		
		$http.post('/chat/update/' + chatUid + '/', fdata).
			success(function(data, status, headers, config) {
				console.log(data);
			}).
			error(function(data, status, headers, config) {
			  	console.log(data);
			});
	}
	
	$scope.addNewChat = function(chatTitle, members) {
		var participans = [];
		for (var key in members) {
			participans.push("participans=" + key);
		}
		
		var serial_participans = participans.join("&"); 
		var fdata = "title=" + chatTitle + "&" + serial_participans;
		
		$http.post('/chat/create/', fdata).
			success(function(data, status, headers, config) {
				console.log(data);
			}).
			error(function(data, status, headers, config) {
			  	console.log(data);
			});
	}
	
	$scope.getAllUsers = function() {
		$http.get('/account/users/').
			success(function(data, status, headers, config) {
				console.log(data);
				$scope.allUsers = data.data;
			}).
			error(function(data, status, headers, config) {
			  	console.log(data);
			});
	}
	

	
	$scope.addUser = function(user) {
		$scope.newChat.members[user.id] = {name: user.name, avatar: user.avatar};
		console.log($scope.newChat.members);
	}
	
	$scope.addUserInCurrentChat = function(user) {
		$scope.thisChat.members[user.id] = {name: user.name, avatar: user.avatar};
		console.log($scope.thisChat.members);
	}
	
	$scope.chatAdmDel = function(chat_uid, index_del, members) {
		console.log(index_del);
		console.log(members[index_del]);
		delete members[index_del];
	}
	
	$scope.newChatUserDel = function(index_del, members) {
		console.log(index_del);
		console.log(members[index_del]);
		delete members[index_del];
	}
	
	$scope.divider = function (date) {
		var ds = date.split(" ");
		if (ds.length  > 1) {
			return true;
		}
			return false;
	}
	
	$.fn.draghover = function(options) {
	  return this.each(function() {

	    var collection = $(),
	        self = $(this);

	    self.on('dragenter', function(e) {
	      if (collection.length === 0) {
	        self.trigger('draghoverstart');
	      }
	      collection = collection.add(e.target);
	      console.log(collection);
	    });

	    self.on('dragleave drop', function(e) {
	      collection = collection.not(e.target);
	      console.log(collection);
	      if (collection.length === 0) {
	        self.trigger('draghoverend');
	      }
	    });
	  });
	};
	
	$(window).draghover().on({
	  'draghoverstart': function(e) {
			if (!$scope.showDropzone) {
				$scope.showDropzone = true;
				$scope.$apply();
				console.log("draghoverstart");
			}
	  },
	  'draghoverend': function(e) {
			if ($scope.showDropzone && !$scope.dzDragenter) {
				$scope.showDropzone = false;
				$scope.$apply();
				console.log("draghoverend");
			}
	  }
	});
	
	$scope.sendMsg = false;
	
  $scope.dropzoneConfig = {
      'options': { // passed into the Dropzone constructor
        'url': function(files) {
        	console.log(files);
        	
        	var msg = '';
        	
        	files.forEach(function(item, i, arr) {
        		msg += item.name;
        	});

        	/*$scope.thisChat.newmessage.text += msg + ', ';*/
        	/*$scope.thisChat.newmessage.text = "1";*/
        	
        	console.log($rootScope.thisChat);
  				$scope.showDropzone = false;
  				$scope.$apply();
  				
        	return '/chat/attachment/upload/';
        },
        'maxFilesize': 50,
      },
      'eventHandlers': {
        'sending': function (file, xhr, formData) {
        	
        	var token = $cookies['csrftoken'];
        	
        	xhr.setRequestHeader("X-CSRFToken", token);
        	
        	console.log(1);
        },
        'success': function (file, response) {
        	var r = jQuery.parseJSON(response);
        	$scope.thisChat.newmessage.attachments.push(r.attachment);
        	$scope.$apply();
        	console.log($scope.thisChat.newmessage);
        	console.log(response);
        },
        'queuecomplete': function() {
        	/*$rootScope.send_message();*/
        	$scope.sendMsg = true;
        	$scope.$apply();
        	$scope.sendMsg = false;
        	$scope.$apply();
        },
        'dragenter': function() {

    				$scope.showDropzone = true;
    				$scope.dzDragenter = true;
    				$scope.$apply();
    				console.log("dz dragenter");

        },
        'dragleave': function() {
  				$scope.dzDragenter = false;
  				$scope.$apply();
  				console.log("dz dragleave");
        }
      }
    };


	  $scope.myData = {
	      modalShown: false,
	      modalNewChat: false,
	    }
	  
	    $scope.logClose = function() {
	      console.log('close!');
	    };
	    
	  $scope.toggleModal = function() {
	    $scope.myData.modalShown = !$scope.myData.modalShown;
	  };
	  
	  $scope.toggleModalNewChat = function() {
		    $scope.myData.modalNewChat = !$scope.myData.modalNewChat;
	  };
  
		$scope.showDropzone = false;
		$scope.dzDragenter = false;
		$scope.getAllUsers();				/*get all users on document load*/
		$scope.newChat = {};				/*create empty new chat*/
		$scope.newChat.members = {};		/*create empty new chat collection*/
		$scope.yourchats = [];

}]);
