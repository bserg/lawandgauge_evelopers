'use strict';

/* Controllers */

var lawandgaugeapp = angular.module('lawgauge');
/*angular.module('lawgauge')*/
lawandgaugeapp.controller('landingCtrl', ['$scope', '$http', '$location', '$sce', function ($scope, $http, $location, $sce) {
	
lawandgaugeapp.location = $location;

	var hash = location.hash.split("/");
	var lang = hash[1];
	if(lang=="en"){
		$scope.lang = "en";
	} else {
		$scope.lang = "ru";
	}
	lawandgaugeapp.lang = $scope.lang;
		
  $http.get('/static/translate.json').success(function(data) {
    $scope.trans = data[0];
  });
  
  $scope.selectlang = function(lang){
	  $scope.lang = lang;
	  lawandgaugeapp.lang = $scope.lang;
	  var hash = location.hash;
	  
	  hash = hash.split("/");
	  hash[1]=lang;
	  hash = hash[0]+"/"+hash[1]+"/"+hash[2];
	  location.hash = hash;
  }
  
  $scope.getHtml = function(input){
	  $scope.customHtml = $sce.trustAsHtml(input);
  }
}]);
