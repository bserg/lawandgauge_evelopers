'use strict';

/* Controllers */

angular.module('lawgauge')
.controller('ChatCtrl', ['$scope', '$http', 'socketJs', '$rootScope', function ($scope, $http, socketJs, $rootScope) {

	socketJs.oNconnect.then(function (data) {

	});
	socketJs.oNopen.then(function (data) {

	});
	
/*	console.log("yourchats");
	console.log($scope.yourchats);*/
	
	$scope.open_chat = function (chat) {
		console.log(chat);
		$scope.generate_mess();
		$scope.thisChat = chat;
		$scope.thisChat.newmessage = {text: "", attachments: []};
		$rootScope.thisChat = $scope.thisChat;
		sock.send(JSON.stringify({
            type: "connect",
            chat: $scope.thisChat.id
        }));
        sock.send(JSON.stringify({type: 'history', chat: $scope.thisChat.id, number:10}));
	};

    sock.onmessage = function(data){
     	if (data.data.type != 10) {
     		
				console.log(data.data);
				
				$scope.allchatmess[data.data.chat].messages.push(data.data);
				
				if (data.data.chat != $scope.thisChat.id) {
					$scope.allchatmess[data.data.chat].newMesageCout++;
				}
				
        $scope.$apply();
        
        console.log('$scope.messages', $scope.allchatmess);
        
     	} else if (data.data.type == 10) {
     		
     		console.log(data.data);
     		
     	} else if (data.data.type == 20) {
     		
     		console.log(data.data);
     	/*	$scope.allchatmess[data.data.chat].messages.push(data.data);
     		$scope.$apply();*/
     		
     	}
     };
     
	socketJs.oNclose.then(function (data) {

	});

    
    $scope.send_message = function () {
    	if ($scope.thisChat.newmessage.text != undefined) {
    	console.log($scope.thisChat.newmessage);
			sock.send(JSON.stringify({
              type: 'message',
              text: $scope.thisChat.newmessage.text,
              attachments: $scope.thisChat.newmessage.attachments
          	}));
						
          	$scope.thisChat.newmessage = {text: "", attachments: []};
    	}

	};
	
	/*$rootScope.send_message = $scope.send_message;*/
	
	$scope.keydowntext = function (k) {
		if (k.keyCode == 13 && k.shiftKey == true) {
			$scope.reim++;
			$scope.curHeight = {height: 41 * $scope.reim};
		}
		if (k.keyCode == 8 && $scope.reim >1) {
			$scope.reim--;
			$scope.curHeight = {height: 41 * $scope.reim};
		}
		if (k.keyCode == 13 && k.shiftKey == false) {
			$scope.send_message();
		}
	};
	$scope.reim = 1;
	

	$scope.thisChat = {};
	$scope.thisChat.newmessage = {text: "", attachments: []};


	$scope.allchatmess = {};
	
	console.log($scope.yourchats);
	
	$scope.generate_mess = function () {
	    for (var i = 0; i < $scope.yourchats.length; i++) {
	      $scope.allchatmess[$scope.yourchats[i].id] = {
	        messages: [],
	        newMesageCout: 0
	      };
	    }
		console.log($scope.allchatmess);
  	};
  	/*$scope.open_chat($scope.yourchats[1]);*/
  	
  	$scope.$watch('yourchats', function (vall) {
  		if (vall[0] != undefined) {
  			if (vall.length > 0) {
  	  	  		console.log(vall[0]);
  	  	  		setTimeout(function() {
  	  	  			$scope.open_chat(vall[0]);
  	  	  			
  	  	  		}, 1000);
  	  	  		
  			}
  		}
  	});
  	
  	$scope.$watch('sendMsg', function (vall) {
  		if (vall) {
  				$scope.send_message();
  	  		console.log('sendMsg');
  		}
  	});
  	
}]);
