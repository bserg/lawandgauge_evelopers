var fbAppId = "";
var vkAppId = "";

function showSmsAuth(tel, type) {
	$("#auth-phone input[type=tel]").val(tel);
	$("#auth-phone").addClass(type).modal('show');
	$(".auth-step1").addClass("show");
	$(".auth-step2").removeClass("show");
	
  $("input[name=phone]").intlTelInput("destroy");
  $("input[name=phone]").intlTelInput({
    utilsScript: "",
    defaultCountry: "auto",
    nationalMode: false,
    autoHideDialCode: false,
    autoPlaceholder: false
  });
  
}

function showSmsCodeInput() {
	$(".auth-step2").addClass("show");
	$(".auth-step1").removeClass("show");
}

function infoPopup(text) {
	$(".info .modal-body").html(text);
	$(".info").modal('show');
}

function authFB(response) {
	if (response.status === 'connected') {
		console.log(response);
	} else if (response.status === 'not_authorized') {
		console.log(response);
	} else {

	}
}

function getSmsCode(tel, callback, param) {
	
	if (!tel) {
		return false;
	}
	
	$.post("/account/generate_phone_code/", {phone: tel}, function(r) {
		console.log(r);
		if (r.success) {
			if (param) {
				callback(param);
			}
		} else if (r.error) {
			showErrorPopap(r.error);
		}
	});
}

function registrationVk(type, response, message, redirect, callback, param) {
	$.post("/account/logout/", function() {
	  
    var scope = angular.element($("#lawandgaugeScope")).scope();
    scope.$apply(function(){
      scope.user = {
          id: '',
          name: '',
          avatar: ''
        };
        scope.isUser = false;
    });
	  
		console.log(response);
		$.post("/account/registration/", 
				{
					first_name: response.session.user.first_name,
					last_name: response.session.user.last_name,
					type: type,
					vk_id: response.session.mid,
					message: message,
					language: lawandgaugeapp.lang
				},
				function(r) {
					console.log(r);
					if (r.success) {
						
						/*$(".step4 input[name=firstName]").val(response.session.user.first_name);
						$(".step4 input[name=lastName]").val(response.session.user.last_name);*/

			      scope.$apply(function(){
			        scope.firstName = response.session.user.first_name;
			        scope.lastName = response.session.user.last_name;
			      });
						
						if (redirect) {
							location.pathname = "/chat/";
						}
						if (param) {
							callback(param);
						}
						if (type == 2) {
							$("input[name=lngToken]").val(r.token);
						}
					} else if (r.error) {
				    	$.post("/account/registration/check/", {vk_id: response.session.mid}, function(resp) {
				    		console.log(resp);
				    		if (resp.success && resp.need_registration) {
				    		/*$(".step4 input[name=firstName]").val(response.session.user.first_name);
								$(".step4 input[name=lastName]").val(response.session.user.last_name);*/
		            scope.$apply(function(){
		              scope.firstName = response.session.user.first_name;
		              scope.lastName = response.session.user.last_name;
		            });
								if (param) {
									callback(param);
								}
								if (type == 2) {
									$("input[name=lngToken]").val(resp.token);
								}
				    		} else {
				    			showErrorPopap(r.error);
				    		}
				    	});
						
					}
				});
	});
}

function registrationFb(type, fbId, access_token, message, redirect, callback, param) {
	$.post("/account/logout/", function() {

	    var scope = angular.element($("#lawandgaugeScope")).scope();
	    scope.$apply(function(){
	      scope.user = {
	          id: '',
	          name: '',
	          avatar: ''
	        };
	        scope.isUser = false;
	    });
	    
		FB.api('/me', {fields: 'first_name, last_name'}, function(response) {
			console.log(response);
			$.post("/account/registration/", 
					{
						first_name: response.first_name,
						last_name: response.last_name,
						type: type,
						fb_id: fbId,
						access_token: access_token,
						message: message,
						language: lawandgaugeapp.lang
					},
					function(r) {
						console.log(r);
						if (r.success) {
							
							/*$(".step4 input[name=firstName]").val(response.first_name);
							$(".step4 input[name=lastName]").val(response.last_name);*/
							
		          scope.$apply(function(){
		              scope.firstName = response.first_name;
		              scope.lastName = response.last_name;
		          });
		            
							if (redirect) {
								location.pathname = "/chat/";
							}
							if (param) {
								callback(param);
							}
							if (type == 2) {
								$("input[name=lngToken]").val(r.token);
							}
						} else if (r.error) {
					    	$.post("/account/registration/check/", {fb_id: fbId}, function(resp) {
					    		console.log(resp);
					    		if (resp.success && resp.need_registration) {
/*					    			$(".step4 input[name=firstName]").val(response.first_name);
									$(".step4 input[name=lastName]").val(response.last_name);*/
		              scope.$apply(function(){
	                  scope.firstName = response.first_name;
	                  scope.lastName = response.last_name;
		              });
									if (param) {
										callback(param);
									}
									if (type == 2) {
										$("input[name=lngToken]").val(resp.token);
									}
					    		} else {
					    			showErrorPopap(r.error);
					    		}
					    	});
						}
					});
		});
	});
}

function registrationSms(type, phone, code, message, redirect, callback, param) {
	$.post("/account/logout/", function() {
	  
    var scope = angular.element($("#lawandgaugeScope")).scope();
    scope.$apply(function(){
      scope.user = {
          id: '',
          name: '',
          avatar: ''
        };
        scope.isUser = false;
    });
	  
		$.post("/account/registration/", 
				{
					type: type,
					phone: phone,
					code: code,
					message: message,
					language: lawandgaugeapp.lang
				},
				function(r) {
					console.log(r);
					if (r.success) {
						/*$(".step4 input[name=phone]").val(phone);*/
				    scope.$apply(function(){
				        scope.phone = phone;
				    });
						if (redirect) {
							location.pathname = "/chat/";
						}
						if (param) {
							callback(param);
						}
						if (type == 2) {
							$("input[name=lngToken]").val(r.token);
						}
					} else if (r.error) {
				    	$.post("/account/registration/check/", {phone: phone}, function(resp) {
				    		console.log(resp);
				    		if (resp.success && resp.need_registration) {
								/*$(".step4 input[name=phone]").val(phone);*/
		            scope.$apply(function(){
	                scope.phone = phone;
		            });
								if (param) {
									callback(param);
								}
								if (type == 2) {
									$("input[name=lngToken]").val(resp.token);
								}
				    		} else {
				    			showErrorPopap(r.error);
				    		}
				    	});
					}
				});
	});
}
var cro = 0;
function accountUpdate(data) {
	var token = $("input[name=lngToken]").val();
	if (!token || !data) {
		return false;
	}
	$.post("/account/registration/"+token+"/", data, function(r) {
		console.log(r);
		if (r.success == true) {
			var pp = '<div class="vk_window"><div class="dsc">Поздравляем!</div><p>Вы прошли предварительную регистрацию. В ближайшее время мы расмотрим вашу заявку и свяжемся с вами.</p></div>'
		}
		infoPopup(pp);
		cro = 1;
	});
}

function isValidJSON(src) {
  var filtered = src;
  filtered = filtered.replace(/\\["\\\/bfnrtu]/g, '@');
  filtered = filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
  filtered = filtered.replace(/(?:^|:|,)(?:\s*\[)+/g, '');

  return (/^[\],:{}\s]*$/.test(filtered));
}

function parseObject(object) {
  var text = "";
  if (!object) {
      return text;
  }
  for (var key in object) {
    if (object[key].toString() == "[object Object]") {
      text += parseObject(object[key]) + "<br />";
    } else {
      if ( isNaN( parseInt(object[key]) ) ) {
          text += object[key] + "<br />";
      }
    }
  }
  
  return text;
}

function showErrorPopap (err) {
  if(isValidJSON(err)) {
    var obj = JSON.parse(err);
    var text = parseObject(obj);
  } else if (err.toString() == "[object Object]") {
    var text = parseObject(err);
  } else {
    var text = err;
  }
    var infoTitle = "";
    if (lawandgaugeapp) {
        if (lawandgaugeapp.hasOwnProperty('lang')) {
            if (lawandgaugeapp.lang == 'en') {
                infoTitle = "Information";
            } else if (lawandgaugeapp.lang == 'ru') {
                infoTitle = "Информация";
            }
        }
    }
	var pp = '<div class="vk_window"><div class="dsc">'+infoTitle+'</div><p>'+text+'</p></div>';
	infoPopup(pp);
}
function loginSms(phone, code, token, redirect) {
	$.post("/account/logout/", function() {
		$.post("/account/login/", {phone: phone, code: code, token: token}, function(r) {
			console.log(r);
			$(".reveal-modal-bg").trigger("click");
			if (redirect && r.success) {
				location.pathname = "/chat/";
			} else if (r.error) {
				showErrorPopap(r.error);
				//infoPopup(JSON.stringify(r.error));
			}
		});
	});
}

function getLoginStatus() {
	console.log("getLoginStatus");
	
	if (vkAppId == "") {
		return false;
	}
	
	var r = getCookie('vk_app_' + vkAppId);
	
	if (r) {
		r = r.split("&");
		r = r.map(function(item) {
			item = item.split("=");
			
			var res = {};
			var key = item[0];
			var value = item[1];
			
			res[key] = value;
			
			return res;
		});
		
		var rObj = {};
		rObj.session = {};
		
		r.forEach(function(rItem, i, arr) {
			for (var key in rItem) {
				rObj[key] = rItem[key];
				rObj.session[key] = rItem[key];
			}
		});

		console.log(rObj);
		
		return rObj;
	} 
	
	return false;
}

function loginFb() {
	/*FB.login(authFB);*/
	FB.login(function(r) {
		$.post("/account/login/", {user_id: r.authResponse.userID, social: "fb", access_token: r.authResponse.accessToken}, function(resp) {
			console.log(resp);
			if (resp.success) {
				location.pathname = "/chat/";
			} else {
				showErrorPopap(resp.error);
			}
		});
	});
}

function loginVk() {
	var r = getLoginStatus();
	/*user_id: <123456>, social: vk|fb, access_token: <access token соцсети FB> , token: <токен для привязки ид соцсети к аккаунту>*/

	if (r) {
		$.post("/account/login/", {user_id: r.session.mid, social: "vk" }, function(resp) {
			console.log(resp);
			if (resp.success) {
				location.pathname = "/chat/";
			} else {
				showErrorPopap(resp.error);
			}
		});
	} else {
		VK.Auth.login(function(r) {
			$.post("/account/login/", {user_id: r.session.mid, social: "vk" }, function(resp) {
				console.log(resp);
				if (resp.success) {
					location.pathname = "/chat/";
				} else {
					showErrorPopap(resp.error);

				}
			});
		});
	}
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}

function setcookie(name, value, expires, path, domain, secure) { // Send a
																																	// cookie
	// 
	// + original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)

	expires instanceof Date ? expires = expires.toGMTString()
			: typeof (expires) == 'number'
					&& (expires = (new Date(+(new Date) + expires * 1e3)).toGMTString());
	var r = [ name + "=" + escape(value) ], s, i;
	for (i in s = {
		expires : expires,
		path : path,
		domain : domain
	}) {
		s[i] && r.push(i + "=" + s[i]);
	}
	return secure && r.push("secure"), document.cookie = r.join(";"), true;
}


function limitExecByInterval(fn, time) {
	var lock, execOnUnlock, args;
	return function() {
		args = arguments;
		if (!lock) {
			lock = true;
			var scope = this;
			setTimeout(function() {
				lock = false;
				if (execOnUnlock) {
					args.callee.apply(scope, args);
					execOnUnlock = false;
				}
			}, time);
			return fn.apply(this, args);
		} else
			execOnUnlock = true;
	}
}

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(HEAD|OPTIONS|TRACE)$/.test(method));
}

function ajaxSetupHandler(){
  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
          }
      }
  });
}

function callVkLogin() {
    var dfr = $.Deferred();
    VK.Auth.login(function(r) {
    	dfr.resolve(r);
    });
    return dfr.promise();
}

function callFbLogin() {
    var dfr = $.Deferred();
    FB.login(function(r) {
    	dfr.resolve(r);
    });
    return dfr.promise();
}

$(document).ready(function() {
	

	
	 $.extend($.ui.autocomplete.prototype.options, {
			open: function(event, ui) {
				$(this).autocomplete("widget").css({
		            "width": ($(this).outerWidth() + "px")
		        });
				console.log($(this));
		    }
	 });
	
	if (location.hostname == "lawandgauge.webtm.ru") {
		fbAppId = '1427231024251881';
		vkAppId = '4918770';
	} else {
		fbAppId = '360246200839744';
		vkAppId = '4976598';
	}
	
	var csrftoken = getCookie('csrftoken');
	
	ajaxSetupHandler();
	
	VK.init({ apiId: vkAppId });
	
  window.fbAsyncInit = function() {
    FB.init({
      appId      : fbAppId,
      xfbml      : true,
      version    : 'v2.3'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  $(".login-fb").on("click", function() {
	  loginFb();
  });
  $(".login-vk").on("click", function() {
	  loginVk();
  });
	$(".custom-select").on("focus", function() {
		$(this).addClass("active");
	});
  $("#auth-phone #codeRequest").on("click", function() {
		
		var tel = $("#auth-phone input[type=tel]").val();
		
		if (!tel) {
			return false;
		}
		
		getSmsCode(tel);
		
		showSmsCodeInput();
		
  });
  
  $(".code-refresh").on("click", function() {
    if (!$(this).hasClass("rotateThis")) {
      var tel = $( $(this).data("phoneClass") ).val();/*$("#auth-phone input[type=tel]").val();*/
      console.log(tel);
      if (!tel) {
        return false;
      }
      
      $(this).addClass("rotateThis");
      var this_var = $(this);
      setTimeout(function() {
        this_var.removeClass("rotateThis");
      }, 5000);
      
      getSmsCode(tel);
    }
  });
  
  $("#auth-phone #smsLogin").on("click", function() {
		var wrapper = $("#auth-phone");
		var phone = wrapper.find("input[type=tel]").val();
		var code = wrapper.find("input.code").val();
		var token = wrapper.find("input[name=lawandgauge_user]").val();
		
		loginSms(phone, code, token, true);
  });
  
  /*input mask*/
  var maskList = $.masksSort($.masksLoad("/static/landing/inputmask/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
  var maskOpts = {
      inputmask: {
          definitions: {
              '#': {
                  validator: "[0-9]",
                  cardinality: 1
              }
          },
          //clearIncomplete: true,
          showMaskOnHover: true,
          autoUnmask: true
      },
      match: /[0-9]/,
      replace: '#',
      list: maskList,
      listKey: "mask",
/*      onMaskChange: function(maskObj, determined) {
          if (determined) {
              var hint = maskObj.name_ru;
              if (maskObj.desc_ru && maskObj.desc_ru != "") {
                  hint += " (" + maskObj.desc_ru + ")";
              }
              $("#descr").html(hint);
          } else {
              $("#descr").html("Маска ввода");
          }
          $(this).attr("placeholder", $(this).inputmask("getemptymask"));
      }*/
  };

  /*$('input[name=phone]').inputmasks(maskOpts);*/
});