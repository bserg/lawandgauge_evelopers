var mapUrl;
var tkn;
var info ={};

function vkPost (data) {
	VK.Api.call('wall.post', data.vk_obj, function(r){
		$(".reveal-modal-bg").trigger("click");
	});
}

function vk_share () {
	var ct = $('input[name="cityName"]').val();
	VK.Auth.login(function(response) {
        if (response.session) {
            var userId = response.session.mid;
            VK.Api.call('users.get', { uid: userId, fields: 'sex, bdate, city, country, photo_200_orig'}, 
	        function(r) {
	        	console.log(r);
	        	var nm = r.response[0].first_name +' '+r.response[0].last_name;
	        	var avtr = r.response[0].photo_200_orig;
	        	console.log(mapUrl, avtr, nm, ct);
	        	VK.Api.call('photos.getWallUploadServer', {uid: userId}, function(r) {
                    console.log(r);
                    if (r.response) {
                    	$.post('/account/upload_image_vk/', { upload_url: r.response.upload_url, image_url: mapUrl, avatar: avtr, name: nm, city: ct, token: tkn }, function(data){
                    		console.log(data);
                    		VK.Api.call('photos.saveWallPhoto', {
                                user_id: userId,
                                photo: data.data.photo,
                                server: data.data.server,
                                hash: data.data.hash
                            }, function (s) {
                                console.log(s);
                                var vk_obj = {};
                                var description = 'Я учавствую в проекте';
                                vk_obj.message = description + "\n" + 'http://'+window.location.hostname;
                                vk_obj.attachments = s.response[0].id;
                                info.img = s.response[0].src_xxbig;
                                info.vk_obj = vk_obj;
                                var hta = '<div class="vk_window"><div class="dsc">Я учавствую в проекте LAW&GAUGE</div><img class="vk_share_img" src="'+s.response[0].src_xxbig+'"><div class="vk_share_bt_pp" onclick="vkPost(info)">опубликовать</div></div>';
                                infoPopup(hta);
                            });
                    	});
                    }
                });      
	        });
        }
    });
}

function fb_share () {
	var ct = $('input[name="cityName"]').val();
	FB.getLoginStatus(function(response) {
	    var fbid = response;
	    if (fbid.status === 'connected') {
	    	FB.api('/me', function (response) {
	            var nm = response.first_name +' '+response.last_name;
	            FB.api('/me/picture?redirect=false&width=200', function (response) {
	                var avtr = response.data.url;
	                console.log(mapUrl, avtr, nm, ct);
	                $.post('/account/upload_image_fb/', { image_url: mapUrl, avatar: avtr, name: nm, city: ct, token: tkn }, function(data){
						console.log(data);
						FB.ui({
				          method: 'feed',
				          name: nm,
				          link: 'http://'+window.location.hostname,
				          picture: 'http://'+window.location.hostname+data.data.image_url,
				          caption: 'LAW&GAUGE',
				          description: 'Я учавствую в проекте'
				        });
					});
				});
	   	 	});
	   	}
	}); 
}

$(document).ready(function() {

	$('.selectpicker').combobox({bsVersion: '3', });
	
	fillCustomComboPlaceholder();
	
	$(".custom-select").on("focus", function() {
		$(this).addClass("active");
	});
	
	$(".custom-select").on("keyup", function(e) {
		
		$(this).trigger("focus");
		
		var elements = $(this).siblings(".custom-select-list").find("li");
		var i;
		
		if (!elements.filter(".active").length) {
			i = -1;
		} else {
			console.log($(elements.filter(".active")[0]).index());
			i = $(elements.filter(".active")[0]).index();
		}
		
		if (e.keyCode == 40) {
			if (i == elements.length - 1) {
				i = -1;
			}
			i++;
		} else if (e.keyCode == 38) {
			if (i == 0) {
				i = elements.length;
			}
			i--;
		}
		
		$(elements).removeClass('active');
		$(elements[i]).addClass('active');
		
		if (e.keyCode == 13) {
			$(elements.filter(".active")[0]).trigger("click");
		}
		
	});
	
	$(document).mousedown(function(event)
			{
			    var container = $(".custom-select");
			    var container1 = $(".custom-select-list");

			    if( (!$(container).is($(event.target)) && !$(event.target).closest($(container)).length) && 
			    		(!$(container1).is($(event.target)) && !$(event.target).closest($(container1)).length) ) {
			    	$(container).removeClass('active');
			    } else {
			    	container.each(function(i, el) {
			    		if (el == event.target) {
			    		} else {
			    			$(el).removeClass('active');
			    		}
			    	});
			    }
			        
	});
	
	$("input[type=file][name=resume]").on("change", function() {
		$(".file_upload>div").text($("input[type=file][name=resume]").get(0).files[0].name);
	});
	
	$(".lang").on("click", function() {
		fillCustomComboPlaceholder()
	});
	
/*	$('#resume').validate({
		sendForm : true,
		onKeyup : true,
		onfocusout: true,
		onclick: true,
		valid : function() {
		  console.log("valid");
				sendEmailResume();
			},
		invalid : function(e) {
		  console.log("invalid");
				e.preventDefault();
			},
		eachField: function(event, status, options) {
			if (!status.required || !status.pattern) {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
			}
		},
        rules: {            
            status: {                
                  required: function() {
                        return $('[name=status]').val() != 'default';
                  }
            },
      }   
	});*/
	
	function fillCustomComboPlaceholder() {
		if (lawandgaugeapp.lang=='ru') {
			var placeholder = "Страна";
		} else {
			var placeholder = "Country";
		}
		
		$(".combobox-container input[type=text]")
			.attr("placeholder", placeholder)
			.attr("data-required", "")
			.attr("name", "country_name");
	}
	
	function sendEmailResume() {

			url = location.origin + location.pathname;
			var frm = $("#resume");
			var formData = new FormData(frm.get(0));
			
			console.log(formData);
		
		var token = getCookie('csrftoken');
		$.ajax({
	    	xhr: function()
	    	  {
				var xhr = new window.XMLHttpRequest();
				return xhr;
	    	  },
			type : "POST",
			url : url,
			/*dataType: 'json',*/
            headers: {"X-CSRFToken": token},
			data : formData,
			processData: false,
			contentType: false,
			success : function(data) {
				console.log(data);
				showMap();
				tkn = data.token;
				var tel = frm.find("input[name=tel]").val();
			},
			error : function(xhr, str) {
				alert("Возникла ошибка!");
			}
		});
	}

	function showMap (data) {
		var markers = '&markers=icon:http://lawandgauge.webtm.ru/static/landing/img/regularBall.png%7Csize:small%7Clabel:S';
		var sizeFactor = 0;
		var city = $('input.form-control.city_ac').val();
		$.get("/map/search/city/", { city: city}, function(data){
			console.log(data);
			var pro = (data.lat + 15)+','+data.lng;
			mapUrl = 'https://maps.googleapis.com/maps/api/staticmap?center='+pro+'&zoom=2&size=600x365&scale=2&style=color:0x5b5bc9&style=element:labels.text%7Cvisibility:off&style=feature:administrative%7Celement:geometry.stroke%7Cvisibility:off&style=feature:road%7Celement:labels%7Cvisibility:off&style=feature:road%7Celement:geometry%7Ccolor:0xefefe3&style=feature:water%7Celement:geometry.fill%7Ccolor:0x3f628c&style=feature:landscape.natural%7Celement:geometry.fill%7Ccolor:0x2e4766&style=feature:landscape.natural.landcover%7Ccolor:0x23344e&style=feature:landscape.natural.terrain%7Ccolor:0x23344e&style=feature:all%7Celement:geometry.stroke%7Cvisibility:off&style=feature:administrative%7Celement:geometry.fill%7Cvisibility:off';
			$.get("/map/data/", { language: lawandgaugeapp.lang }, function(data){
			  for (var i=0; i<data.data.length; i++) {
					if(sizeFactor < data.data[i].count) {
						sizeFactor = data.data[i].count;
					}
				}
				for (var i=0; i<data.data.length; i++) {
					if(data.data[i].name != city) {
						markers += '%7C'+data.data[i].lat+','+data.data[i].lng;					
					}
				}
				mapUrl += markers;
				console.log('/map/data/', data.data);
				console.log('mapUrl', mapUrl);
				$('.map_share').show();
				$('body').addClass('over');
				var mapUrl2 = 'https://maps.googleapis.com/maps/api/staticmap?center='+city+'&zoom=2&size=600x365&scale=2&style=color:0x5b5bc9&style=element:labels.text%7Cvisibility:off&style=feature:administrative%7Celement:geometry.stroke%7Cvisibility:off&style=feature:road%7Celement:labels%7Cvisibility:off&style=feature:road%7Celement:geometry%7Ccolor:0xefefe3&style=feature:water%7Celement:geometry.fill%7Ccolor:0x3f628c&style=feature:landscape.natural%7Celement:geometry.fill%7Ccolor:0x2e4766&style=feature:landscape.natural.landcover%7Ccolor:0x23344e&style=feature:landscape.natural.terrain%7Ccolor:0x23344e&style=feature:all%7Celement:geometry.stroke%7Cvisibility:off&style=feature:administrative%7Celement:geometry.fill%7Cvisibility:off';
				mapUrl2 += markers;
				$('.map_share #map_style').css({'background-image': 'url('+mapUrl2+')'});
				setTimeout(function () {
					$('.map_share').addClass('active');
				},100);
			});
		});
	}

	function Resize() {
		if($(window).width()<1200) {
			return false;
		}
		
		var scale = Scale;
		var res = $(window).height()/$(window).width();

		if ($(window).height()<=MinHeight || $(window).width()<=MinWidth) {
			scale = MinWidth/DefWidth;
		} else {
			scale = $(window).height()/DefHeight;
		}
		
		scale_percent = parseInt(scale*100)-5;
		$(".scalable").css("font-size", scale_percent+"%");

		var Scale = scale;
	}
	
	MinHeight = 530;
	MinWidth = 1200;
	DefHeight = 1050;
	DefWidth = 1920;
	DefRes = DefHeight/DefWidth; //100% scale
	ScreensHeight = [ // screen height percent (window)
		1, // screen-one 100% height
		0.7, // screen-two 70% height
		1, // screen-three 100% height
		1.32, // screen-four normal height
		1
	];
	Scale = 1;
	
	$(window).bind('resize', Resize);
	$(window).resize();
	Resize();
	
	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie != '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            // Does this cookie string begin with the name we want?
	            if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}
	
	var csrftoken = getCookie('csrftoken');

	function limitExecByInterval(fn, time) {
		var lock, execOnUnlock, args;
		return function() {
			args = arguments;
			if (!lock) {
				lock = true;
				var scope = this;
				setTimeout(function() {
					lock = false;
					if (execOnUnlock) {
						args.callee.apply(scope, args);
						execOnUnlock = false;
					}
				}, time);
				return fn.apply(this, args);
			} else
				execOnUnlock = true;
		}
	}
	function csrfSafeMethod(method) {
	    // these HTTP methods do not require CSRF protection
	    return (/^(HEAD|OPTIONS|TRACE)$/.test(method));
	}
	function ajaxSetupHandler(){
	    $.ajaxSetup({
	        beforeSend: function(xhr, settings) {
	            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
	                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	            }
	        }
	    });
	}
	ajaxSetupHandler();
	
	$("#resume").on("submit", function(e){
	  sendEmailResume();
		e.preventDefault();
	});
});
