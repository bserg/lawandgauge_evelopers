function clientsStep(step) {
		if (!step) {
			return false;
		}
		
		var stepclass = ".clients-reg--" + step;
		
		$(".clients-reg").removeClass("show");
		$(stepclass).addClass("show");
	}

function l_step(step) {
		switch (step) {
		case "2":
			$(".reg-btn").addClass("increase");
			setTimeout(function() {
				$(".step2").fadeIn(1000);
			}, 500);
			break;
			
		case "3":
			$(".step:not(.step1)").hide();
			$(".step1").removeClass("cshow").addClass("chide");
			$(".step3").fadeIn(1000);
			break;
		
		case "4":
			var t = $(".reg-btn").offset();
			var trigger = document.getElementById("anm");
			
			$("#l_tel").removeAttr("readonly");
			$("#login").addClass("dark");
			$(".lang").addClass("dark");
			$("#logo").addClass("color");
			$(".step:not(.step1)").hide();
			$(".sec1").addClass("s4");
			
/*			setTimeout(function () {
				trigger.beginElement();
			},370);*/
			
			$(".reg-btn").css("top", t.top+"px").css("left", t.left+"px");
			
			var l = "0px";
			
			$(".reg-btn").addClass("increase-white")
				.css("width", ($(window).width())+"px")
				.css("height", "100%")
				.css("top", "0px")
				.css("left", l);
			setTimeout(function() {
				$(".lawyers-reg--step2").addClass("show");
			}, 1000);
			
			break;
			
		case "5":
		  
	    var scope = angular.element($("#lawandgaugeScope")).scope();
	    scope.$apply(function(){
	      scope.phone = $(".lawyers-reg--step2 .tel").val();
	    })
	    
			$(".lawyers-reg").removeClass("show");
			
			$("#l_tel").val($(".lawyers-reg--step2 .tel").val());
			if ($(".lawyers-reg--step2 .tel").val().length > 7) {
				/*$("#l_tel").attr("disabled", "disabled").attr("name", "tel_");*/
				$("#l_tel").attr("readonly", "readonly");
			}
			
			
			setTimeout(function() {
				$(".step4").addClass("active").css("display", "block");
			}, 1000);
			break;
		case "5":
			/*$(".step5").show();*/
		default:
			break;
		}
	}
	
$(document).ready(function() {
  
  $("input[name=phone]").intlTelInput({
    utilsScript: "",
    defaultCountry: "auto",
    nationalMode: false,
    autoHideDialCode: false,
    autoPlaceholder: false
  });
  
	fillCustomComboPlaceholder();
	
	$('#form').validate({
		sendForm : true,
	    onKeyup : true,
		valid : function() {
				sendEmail();
			},
		invalid : function() {
			},
		eachField: function(event, status, options) {
			if (!status.required || !status.pattern) {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
			}
		}
	});
	
	/*$('#country_sel').combobox({bsVersion: '3'});*/
	$('.selectpicker').combobox({bsVersion: '3'});
	  
	if (lawandgaugeapp.lang=='ru') {
		var placeholder = "Страна";
	} else {
		var placeholder = "Country";
	}
	
	$(".combobox-container input[type=text]")
		.attr("placeholder", placeholder)
		.attr("data-required", "")
		.attr("name", "country_name");
	
/*	$('#l_reg_form').validate({
		sendForm : true,
	    onKeyup : true,
		valid : function() {
				sendEmail("L");
				console.log("sending #l_reg_form");
			},
		invalid : function() {
			console.log("invalid #l_reg_form");
			},
		eachField: function(event, status, options) {
			if (!status.required || !status.pattern) {
				$(this).addClass("error");
			} else {
				$(this).removeClass("error");
			}
		}
	});*/
	
	$(window).load(function() {
		drawCircles(2500);
		animateNumbers();
		selectContent(location.hash);
	});

	$(window).on("resize", limitExecByInterval(function(e) {
		drawCircles();
	}, 500));
	
	function Resize() {
		if($(window).width()<1200) {
			return false;
		}
		
		var scale = Scale;
		var res = $(window).height()/$(window).width();

		if ($(window).height()<=MinHeight || $(window).width()<=MinWidth) {
			scale = MinWidth/DefWidth;
		} else {
			scale = $(window).height()/DefHeight;
		}
		
		scale_percent = parseInt(scale*100)-5;
		$(".scalable").css("font-size", scale_percent+"%");

		var Scale = scale;
	}
	
	MinHeight = 530;
	MinWidth = 1200;
	DefHeight = 1050;
	DefWidth = 1920;
	DefRes = DefHeight/DefWidth; //100% scale
	ScreensHeight = [ // screen height percent (window)
		1, // screen-one 100% height
		0.7, // screen-two 70% height
		1, // screen-three 100% height
		1.32, // screen-four normal height
		1
	];
	Scale = 1;
	
	$(window).bind('resize', Resize);
	$(window).resize();
	Resize();
	

	
	function lawyersStep(step) {
		if (!step) {
			return false;
		}
		
		var stepclass = ".lawyers-reg--" + step;
		
		$(".lawyers-reg").removeClass("show");
		$(stepclass).addClass("show");
	}
	
	function selectContent(hash) {
		if (!hash) {
			hash="#/ru/clients";
			location.hash = hash;
		}
		
		var content = hash.split("/");
		content = content[content.length-1];
		
		var header = $("body");
		var content_clients = $("#content_clients");
		var content_lawyers = $("#content_lawyers");
		var fc = $(".for_clients");
		
		header.find(".header-btn").removeClass("selected");
		
		if (content=="clients") {
			
			$(".lawyers-reg").removeClass("show");
			$(".clients-reg--step1").addClass("show");
			
			resetLawyers();
			drawCircles();
			
			header.find(".header-btn[href*=clients]").addClass("selected");
			content_lawyers.hide();
			content_clients.fadeIn(1000);
			header.addClass("clients").removeClass("lawyers");
			fc.fadeIn(1000);
			
/*			setTimeout(function() {
				var logo_left = $("#logo").offset().left;
				var svg_left = $("#logo svg:first-child path.basePath")
					.offset().left;
				var dx = Math.abs(logo_left) - Math.abs(svg_left);
				
				if (Math.abs(dx)>9) {
					$("#logo svg")
					.css("transform", "translateX("+dx+"px)")
					.css("-webkit-transform", "translateX("+dx+"px)")
					.css("-moz-transform", "translateX("+dx+"px)")
					.css("-ms-transform", "translateX("+dx+"px)");
				}
			}, 200);*/
			
		} else if (content=="lawyers") {
			$(".clients-reg").removeClass("show");
			
			header.find(".header-btn[href*=lawyers]").addClass("selected");
			content_clients.hide();
			content_lawyers.fadeIn(1000);
			header.removeClass("clients").addClass("lawyers");
			
/*			setTimeout(function() {
				var dx = 0;
				
				$("#logo svg")
					.css("transform", "translateX("+dx+"px)")
					.css("-webkit-transform", "translateX("+dx+"px)")
					.css("-moz-transform", "translateX("+dx+"px)")
					.css("-ms-transform", "translateX("+dx+"px)");
			}, 200);*/
		}
		Resize();
	}
	
	function resetLawyers(){
		$(".sec1").removeClass("s4");
		$(".reg-btn").removeAttr("style");
		$(".reg-btn").removeClass("increase-white chide");
		$("#logo").removeClass("color");
		$(".step4").removeClass("active").removeAttr("style");
		$("#logo").fadeIn(1000);
		$(".step3, .step2").hide();
		$("#login").removeClass("dark");
		/*$(".lang").removeClass("dark");*/
		$(".reg-btn").removeClass("increase");
	}
	
/*	function l_step(step) {
		switch (step) {
		case "2":
			$(".reg-btn").addClass("increase");
			setTimeout(function() {
				$(".step2").fadeIn(1000);
			}, 500);
			break;
			
		case "3":
			$(".step:not(.step1)").hide();
			$(".step1").removeClass("cshow").addClass("chide");
			$(".step3").fadeIn(1000);
			break;
		
		case "4":
			var t = $(".reg-btn").offset();
			var trigger = document.getElementById("anm");
			
			$("#l_tel").removeAttr("disabled");
			$("#login").addClass("dark");
			$(".lang").addClass("dark");
			$("#logo").addClass("color");
			$(".step:not(.step1)").hide();
			$(".sec1").addClass("s4");
			
			setTimeout(function () {
				trigger.beginElement();
			},370);
			
			$(".reg-btn").css("top", t.top+"px").css("left", t.left+"px");
			
			var l = "0px";
			
			$(".reg-btn").addClass("increase-white")
				.css("width", ($(window).width())+"px")
				.css("height", "100%")
				.css("top", "0px")
				.css("left", l);
			setTimeout(function() {
				$(".lawyers-reg--step2").addClass("show");
			}, 1000);
			
			break;
			
		case "5":
			
			$(".lawyers-reg").removeClass("show");
			
			$("#l_tel").val($(".lawyers-reg--step2 .tel").val());
			if ($(".lawyers-reg--step2 .tel").val()) {
				$("#l_tel").attr("disabled", "disabled");
			}
			
			
			setTimeout(function() {
				$(".step4").addClass("active").css("display", "block");
			}, 1000);
			break;
		case "5":
			$(".step5").show();
		default:
			break;
		}
	}*/

	function drawCircles(timeout) {
		
		console.log("drawCircles");
		
		if (!timeout) {
			timeout = 0;
		}
		
		if ($(document).width() > 1023) {
			var csize = 193;
			
			if ($(window).height() < 1000) {
				var circ_scale = parseInt($(window).height()*100/1000);
				var thickness = 3;
				
				csize = csize*circ_scale/100;
				
				$(".circle").css("width", csize).css("height", csize);
				$(".circle-border").css("border-width", thickness + "px");
				
			} else {
				var thickness = 5;
				
				$(".circle").removeAttr("style");
				$(".circle-border").removeAttr("style");
			}
			
		} else {
			var csize = parseInt($(document).width() / 4);
			
			if ($(window).height() < 1000) {
				var circ_scale = parseInt($(window).height()*100/1000)+15;
				
				csize = csize*circ_scale/100;
				
				if (csize<67) {
					csize = 67;
				}
			}
			
			var thickness = 3;

			$(".circle").css("width", csize).css("height", csize);
			$(".circle-border").css("border-width", thickness + "px");
		}
		
		if ($(".circle-border:nth-child(1) canvas").lengtn>0) {
			var old_size = $(this).width();
			
			if (( Math.abs(old_size) - Math.abs(csize) )<20) {
				return false;
			} else {
				alert("resize");
			}
		}
		
		if ( $(window).width() > $(window).height() ) {
			var space = parseInt(($(".info-bar").width() - (csize+10+5*2+thickness*2)*4)/6);
			
			$(".circle-border:nth-child(1)")
				.css("margin-left", "0px")
				.css("margin-right", space+"px");
			
			$(".circle-border:nth-child(4)")
				.css("margin-right", "0px")
				.css("margin-left", space+"px");
			
			$(".circle-border:nth-child(2), .circle-border:nth-child(3)")
				.css("margin-right", space+"px")
				.css("margin-left", space+"px");
			
			$(".circle-border")
				.css("border-width", thickness + "px")
				.css("float", "none");

		} else {
			$(".circle-border").removeAttr("style");
			$(".circle-border").css("border-width", thickness + "px");
		}
		
		$('.circle1').circleProgress({
			value : 0.4,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});
		
		$('.circle2').circleProgress({
			value : 0.75,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});
		
		$('.circle3').circleProgress({
			value : 0.75,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});
		
/*		$('.circle4').circleProgress({
			value : 0.4,
			size : csize,
			startAngle : -90 * Math.PI / 180,
			lineCap : "square",
			fill : {
				color : "#438eb4"
			},
			thickness : thickness,
			animation : {
				duration : timeout,
				easing : "easeOutCirc"
			}
		});*/
		
		setTimeout(function() {
			setCirclesMargin();
		}, timeout+500);
		
		if ($(".increase-white").length>0) {
			var l = "0px";
			$(".increase-white")
				.css("width", ($(window).width())+"px")
				.css("height", "100%")
				.css("top", "0px")
				.css("left", l);
		}
	}
	
	function fillCustomComboPlaceholder() {
		if (lawandgaugeapp.lang=='ru') {
			var placeholder = "Страна";
		} else {
			var placeholder = "Country";
		}
		
		$(".combobox-container input[type=text]")
			.attr("placeholder", placeholder)
			.attr("data-required", "")
			.attr("name", "country_name");
	}
	
	function animateNumbers() {
		$(".circle-border:nth-child(1) .val").prop('number', 0).animateNumber({
			number : 87,
			easing : 'easeOutCirc'
		}, 2500);
		
		$(".circle-border:nth-child(2) .val").prop('number', 0).animateNumber({
			number : 57,
			easing : 'easeOutCirc'
		}, 2500);
		
		$(".circle-border:nth-child(3) .val").prop('number', 0).animateNumber({
			number : 59,
			easing : 'easeOutCirc'
		}, 2500);
		
		$(".circle-border:nth-child(4) .val").prop('number', 0).animateNumber({
			number : 118,
			easing : 'easeOutCirc'
		}, 2500);
	}

	function selectLang() {
		/*var wrapper = $(".lang");
		wrapper.find("div").each(function() {
			$(this).toggleClass("selected");
		});*/
	}

	function limitExecByInterval(fn, time) {
		var lock, execOnUnlock, args;
		return function() {
			args = arguments;
			if (!lock) {
				lock = true;
				var scope = this;
				setTimeout(function() {
					lock = false;
					if (execOnUnlock) {
						args.callee.apply(scope, args);
						execOnUnlock = false;
					}
				}, time);
				return fn.apply(this, args);
			} else
				execOnUnlock = true;
		}
	}

	function sendEmail(target) {
		if (target=="L") {
			url = '/landing/lw/';
			var frm = $("#l_reg_form");
			var tel = frm.find("input[name=phone]").val();
			
      var scope = angular.element($("#lawandgaugeScope")).scope();
      
				var msg = {
						'first_name': frm.find("input[name=firstName]").val(),
						'last_name': frm.find("input[name=lastName]").val(),
						'phone': tel,
						'email': frm.find("input[name=email]").val(),
						'country': scope.currentCountry.id,
						'language': lawandgaugeapp.lang
				};


			accountUpdate(msg);
		} else {
			
			var frm = $("#form");
			var name = frm.find("input[type=tel]").val();
			var phone = frm.find("input[type=tel]").val();
			var email = frm.find("input[name=email]").val();
			var message = frm.find("textarea[name=message]").val();
				
			/*registrationSmsClient(name, phone, email, message);*/

			showSmsAuth(phone, 'client');
		}
	}
  
	$(".yes-btn").on("click", function(e) {
		l_step("4");
		e.stopPropagation();
	});
	
	$(".no-btn").on("click", function(e) {
		l_step("3");
		e.stopPropagation();
	});
	
	$(".reg-btn").on("click", function(e) {
		l_step("2");
		
	});
	
	$(window).on('hashchange', function(e) {
	  console.log(e.originalEvent.oldURL, e.originalEvent.newURL);
	  var old_url = e.originalEvent.oldURL;
	  var new_url = e.originalEvent.newURL;
	  if (old_url.split("#").length > 1 && new_url.split("#").length > 1) {
	    var old_lang = old_url.split("#")[1].split("/")[1];
	    var new_lang = new_url.split("#")[1].split("/")[1];
	    
	    if (old_lang == new_lang) {
	      selectContent(location.hash);
	    }
	  }
	});

/*	$(".lang div").on("click", function() {
		selectLang();
	});*/
	
/*	$(".lang").on("click", function() {
		fillCustomComboPlaceholder()
	});*/
	
	$("#carousel1").swiperight(function() {
		$(this).carousel('prev');
	});
	
	$("#carousel1").swipeleft(function() {
		$(this).carousel('next');
	});
	
	$("#login-form").on("submit", function(e) {
		e.preventDefault();
	});
	
	$("#l_reg_form").on("submit", function(e) {
	    sendEmail("L");
	    console.log("sending #l_reg_form");
	    e.preventDefault();
	});
	/* $('#l_reg_form').validate({
  sendForm : true,
    onKeyup : true,
  valid : function() {
      sendEmail("L");
      console.log("sending #l_reg_form");
    },
  invalid : function() {
    console.log("invalid #l_reg_form");
    },
  eachField: function(event, status, options) {
    if (!status.required || !status.pattern) {
      $(this).addClass("error");
    } else {
      $(this).removeClass("error");
    }
  }
});*/
	$("#login-btn").on("click", function() {
		showSmsAuth("", "login");
		/*$("#login").toggleClass("show");*/
	});
	
	$(".sec3").on("click", function() {
		/*showSmsAuth();*/
	});
	
	$(".clients-reg .to-step-2").on("click", function() {
		clientsStep("step2");
	});
	
	$(".clients-reg .to-step-1").on("click", function() {
		clientsStep("step1");
	});

	$(".clients-reg .to-step-3").on("click", function() {
		var tel = $(".clients-reg--step2 .tel").val();
		
		if (!tel) {
			return false;
		}
		
		getSmsCode(tel, clientsStep, "step3");
	});

	
	$(".clients-reg .sms-reg").on("click", function() {
		
		var phone = $(".clients-reg--step2 .tel").val();
		var code = $(".clients-reg--step3 .code").val();
		var message = $(".clients-reg--step1 textarea").val();
		
		if (!phone || !code) {
			return false;
		}
		registrationSms(1, phone, code, message, true);
	});
	
	$(".lawyers-reg .to-step-2").on("click", function() {
		lawyersStep("step2");
	});
	
	$(".lawyers-reg .to-step-1").on("click", function() {
		lawyersStep("step1");
	});
	
	$(".lawyers-reg .to-step-3").on("click", function() {
		var tel = $(".lawyers-reg--step2 .tel").val();
		
		if (!tel) {
			return false;
		}
		
		getSmsCode(tel, lawyersStep, "step3");
	});
	
	$(".lawyers-reg .sms-reg").on("click", function() {
		
		var phone = $(".lawyers-reg--step2 .tel").val();
		var code = $(".lawyers-reg--step3 .code").val();
		var message = $(".lawyers-reg--step1 textarea").val();
		
		if (!phone || !code) {
			return false;
		}
		
		registrationSms(2, phone, code, "", false, l_step, "5");
		
		/*registrationSmsClient(phone, code, message);*/
	});
	
	$(".clients-reg--step2 .login-vk--btn").on("click", function() {
			/*var r = getLoginStatus();*/
			var message = $(".clients-reg--step1 textarea").val();
				
				promise = $.when();
			    promise = promise.then(function(){
			        return callVkLogin();
			    }).then(function(r){
			    	registrationVk(1, r, message, clientsStep, "step3");
			    });

	});
	$(".clients-reg--step2 .login-fb--btn").on("click", function() {

			var message = $(".clients-reg--step1 textarea").val();
			
				promise = $.when();
			    promise = promise.then(function(){
			        return callFbLogin();
			    }).then(function(r){
			    	registrationFb(1, r.authResponse.userID, r.authResponse.accessToken, message, clientsStep, "step3");
			    });

	});
	  
	$(".lawyers-reg--step2 .login-vk--btn").on("click", function() {

		var message = $(".lawyers-reg--step1 textarea").val();
			
			promise = $.when();
		    promise = promise.then(function(){
		        return callVkLogin();
		    }).then(function(r){
		    	registrationVk(2, r, "", false, l_step, "5");
		    });

	});
	$(".lawyers-reg--step2 .login-fb--btn").on("click", function() {
	
			var message = $(".lawyers-reg--step1 textarea").val();
			
				promise = $.when();
			    promise = promise.then(function(){
			        return callFbLogin();
			    }).then(function(r){
			    	registrationFb(2, r.authResponse.userID, r.authResponse.accessToken, "", false, l_step, "5");
			    });
	
	});
	
	$(document).mouseup(function (e) {
	    var container = $("#login");
	    var container1 = $("#login-btn");
	    
	    if (container.has(e.target).length === 0 && container1.has(e.target).length === 0) {
	        container.removeClass("show");
	    }
	});
	
	if (location.hash.split("/")[3]) {
		/*l_step("5");*/
	}
	

});

function setCirclesMargin() {
	
	if ( $(window).width() > $(window).height() ) {
		console.log("setCirclesMargin");
		var top_ = $("#logo").offset().top+$("#logo").height();
		var bot = $(".sec1").height();
		var dy = bot - top_;
		var panel_height = $(".info-bar").height();
		
		if (panel_height<dy) {
			var mt = (dy - $(".slogan.for_clients").height() 
					- panel_height)/2 + $(".slogan.for_clients").height();
			
		$(".info-bar").css("margin-top", mt+"px");
		
		}
	}
	
}