'use strict';

/* Controllers */

var lawandgaugeapp = angular.module('lawandgauge', []);

lawandgaugeapp.directive("fileread", [function () {
  return {
      scope: {
          fileread: "="
      },
      link: function (scope, element, attributes) {
          element.bind("change", function (changeEvent) {
              scope.$apply(function () {
                  scope.fileread = changeEvent.target.files[0];
                  // or all selected files:
                  // scope.fileread = changeEvent.target.files;
              });
          });
      }
  }
}]);
lawandgaugeapp.directive('intlTel', function(){
  return{
    replace:true,
    restrict: 'E',
    require: 'ngModel',
    template: '<input  required type="tel" class="form-control" id="" name="phone">',
    link: function(scope,element,attrs,ngModel){
      var read = function() {
        var inputValue = element.val();
        ngModel.$setViewValue(inputValue);
      };
      element.intlTelInput({
        utilsScript: "",
        defaultCountry: "auto",
        nationalMode: false,
        autoHideDialCode: false,
        autoPlaceholder: false
      });

      element.on('focus blur keyup change', function() {
          scope.$apply(read);
      });
      read();
    }
  }
});
lawandgaugeapp.controller('translateCtrl', ['$scope', '$http', '$location', '$sce', function($scope, $http, $location, $sce) {

  lawandgaugeapp.location = $location;

  var hash = location.hash.split("/");
  var lang = hash[1];
  if (lang == "en") {
    $scope.lang = "en";
  } else {
    $scope.lang = "ru";
  }
  lawandgaugeapp.lang = $scope.lang;



  $(".tip_ac").on("change", function() {
    if(!$scope.currentCurrency) {
      $(".switcher-wrapper .switch:nth-child(1)").trigger("click");
    }
  });

  $http.get('/account/data/').success(function(data) {
    if (data.success == true) {
      $scope.isUser = true;
      $scope.user = data;
      $scope.selectlang($scope.user.lang);
    }
  }).error(function(data) {
    console.log(data);
  });

$(window).load(function() {
  if (window.data) {
    $scope.user_resume = data;

    if ($scope.user_resume.hasOwnProperty('phone')) {
      $scope.phone = $scope.user_resume.phone;
    }
    if ($scope.user_resume.hasOwnProperty('first_name')) {
      $scope.first_name = $scope.user_resume.first_name;
    }
    if ($scope.user_resume.hasOwnProperty('last_name')) {
      $scope.last_name = $scope.user_resume.last_name;
    }
    if ($scope.user_resume.hasOwnProperty('email')) {
      $scope.email = $scope.user_resume.email;
    }
      $scope.$apply();

/*    if ($scope.user_resume.phone) {
          $scope.phone = $scope.user_resume.phone;
          $scope.first_name = $scope.user_resume.first_name;
          $scope.last_name = $scope.user_resume.last_name;
          $scope.email = $scope.user_resume.email;
          $scope.$apply();
      /!*$("input[name=phone]").intlTelInput("setNumber", $scope.user_resume.phone);*!/
    }*/
  }
});


/*  $http.get('/static/landing/translate.json').success(function(data) {
    $scope.trans = data[0];
    console.log($scope.trans);
  });*/

  $scope.getTranslations = function() {
    $http({
      url: '/localization/translations/',
      method: "GET",
      params: {
        use_slug: true
      }
    }).success(function(data) {
      console.log(data.data);
      $scope.translate = data.data;
    }).error(function(data) {
      console.log(data);
    });
/*    $http.get('/localization/translations/', formdata).success(function(data) {
      console.log(data.data);
      $scope.translate = data.data;
    }).error(function(data) {
      console.log(data);
    });*/
  };
  $scope.getTranslations();

  function fbLogoutUser() {
    FB.getLoginStatus(function(response) {
      if (response && response.status === 'connected') {
        FB.logout(function(response) {
          setTimeout(function() {
            location.hash = "";
            location.pathname = "/";
          }, 100);
        });
      }
    });
  }

  $scope.logout = function() {
    $http.post('/account/logout/').success(function(data) {
      VK.Auth.logout();
      fbLogoutUser();

      $scope.user = {
        id: '',
        name: '',
        avatar: ''
      };
      $scope.isUser = false;
      $scope.yourchats = [];

    }).error(function(data) {
      console.log(data);
    });
  };

  $scope.t = function(key) {
    if (!$scope.translate) {
      return false;
    }
    var result;
    if ($scope.translate.hasOwnProperty(key)) {
      var _result = $scope.translate[key];
      if (_result.hasOwnProperty($scope.lang)) {
        result = _result[$scope.lang];
      }
    }

    if (result) {
      return result
    } else {
      return key;
    }
  };

  $scope.getServices = function() {
    var area_ac = $(".area_ac");
    $http.get('/account/services/').success(function(data) {
      $scope.areas = data.data;

      var areas = $scope.areas;
      var vals = [];
      for (var key in areas) {
        if (areas.hasOwnProperty(key)) {
          vals.push({
            label: areas[key].name,
            value: areas[key].name,
            id: areas[key].id
          });
        }
      }

      area_ac.autocomplete({
        source: vals,
        minLength: 0,
        position: {
          my: "left top-3",
          at: "left bottom"
        },
        select: function(event, ui) {

          var area = "";
          for (var key in areas) {
            if (areas.hasOwnProperty(key)){
              if (areas[key].id == ui.item.id) {
                area = areas[key];
                break;
              }
            }
          }
          $scope.selectArea(area);
        }
      });

      $(".area_ac").on("focus", function() {
        $($(this)).autocomplete("search", "");
      }).on("blur", function() {
        $($(this)).autocomplete("close");
      });
    });
  };
  $scope.getServices();

  $scope.getCountries = function() {
    $http.get('/map/countries/').success(function(data) {
      $scope.countries = data.data;

      $scope.countries.forEach(function(item, i, arr) {
        if ($scope.user) {
          if (item.id == $scope.user.country) {
            $scope.selectCountry(item);
          }
        }
      });

      var countries = $scope.countries;
      var vals = [];
      for (var key in countries) {
        if (countries.hasOwnProperty(key)) {
          vals.push({
            label: countries[key].name,
            value: countries[key].name,
            id: countries[key].id
          });
        }
      }

      $(".country_ac").autocomplete({
        source: vals,
        minLength: 0,
        position: {
          my: "left top-3",
          at: "left bottom"
        },
        select: function(event, ui) {
          var country = "";
          for (var key in $scope.countries) {
            if ($scope.countries.hasOwnProperty(key)) {
              if ($scope.countries[key].id == ui.item.id) {
                country = $scope.countries[key];
              }
            }
          }
          $scope.selectCountry(country);
          $(".country_ac").trigger("change");
        }
      });

      $(".country_ac, .city_ac").on("focus", function() {
        $($(this)).autocomplete("search", "");
      }).on("blur", function() {
        $($(this)).autocomplete("close");
      });
    });
  };
  $scope.getCountries();

  $http.get('/account/currencies/').success(function(data) {
    $scope.currencies = data.data;
    console.log(data);
  });

  $scope.setCurrentAreaOfLaw = function(key, areas) {
    $scope.currentAreaOfLaw = areas[key];
    console.log(key, areas);
  };

  $scope.selectCountry = function(country) {
    $(".custom-select").removeClass("active");
    $("input[name=countryName]").removeClass("error");
    $scope.currentCountry = country;
    $scope.q1 = country.name;
    $http({
      url: '/map/cities/',
      method: "GET",
      params: {
        country: country.id
      }
    }).success(function(data) {
      $scope.cities = data.data;
      console.log(data);

      $scope.cities.forEach(function(city, i, arr) {
        if (city.id == $scope.user.city) {
          $scope.selectCity(city);
        }
      });

      var cities = $scope.cities;
      var vals = [];
      for (var key in cities) {
        if (cities.hasOwnProperty(key)) {
          vals.push({
            label: cities[key].name,
            value: cities[key].name,
            id: cities[key].id
          });
        }
      }

      $(".city_ac").autocomplete({
        source: vals,
        minLength: 0,
        position: {
          my: "left top-3",
          at: "left bottom"
        },
        select: function(event, ui) {

          var city = "";
          for (var key in $scope.cities) {
            if ($scope.cities.hasOwnProperty(key)) {
              if ($scope.cities[key].id == ui.item.id) {
                city = $scope.cities[key];
                break;
              }
            }
          }

          $scope.selectCity(city);
        }
      });
    });
  };

  $scope.selectCity = function(city) {
    $(".custom-select").removeClass("active");
    $("input[name=cityName]").removeClass("error");
    $scope.currentCity = city;
    $scope.q2 = city.name;
  };

  $scope.selectCurrency = function(currency, $event) {
    $scope.currentCurrency = currency;
    $(".switch").removeClass("selected");
    var element;
    if ($($event.target).hasClass("switch")) {
      element = $($event.target);
    } else {
      element = $($event.target).parents(".switch");
    }
    element.addClass("selected");
  };

  $scope.selectArea = function(area) {
    var institute_ac = $(".institute_ac");

    $scope.currentArea = area;
    if (!$scope.$$phase) {
      $scope.$apply();
    }

    var institutes = $scope.currentArea.institutes;
    var vals = [];
    for (var key in institutes) {
      if (institutes.hasOwnProperty(key)) {
        vals.push({
          label: institutes[key].name,
          value: institutes[key].name,
          id: institutes[key].id
        });
      }
    }

    institute_ac.autocomplete({
      source: vals,
      minLength: 0,
      position: {
        my: "left top-3",
        at: "left bottom"
      },
      select: function(event, ui) {

        var institute = "";
        for (var key in institutes) {
          if (institutes[key].id == ui.item.id) {
            institute = institutes[key];
            break;
          }
        }
        $scope.selectInstitute(institute);
      }
    });

    institute_ac.on("focus", function() {
      $($(this)).autocomplete("search", "");
    }).on("blur", function() {
      $($(this)).autocomplete("close");
    });
  };

  $scope.getNumPrefix = function($index) {
    if ($index) {
      return $index + '-';
    } else {
      return '';
    }
  };

  $scope.selectInstitute = function(institute) {
    var tip_ac = $(".tip_ac");

    $scope.currentInstitute = institute;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
    var tips = $scope.currentInstitute.service_tips;
    var vals = [];
    for (var key in tips) {
      vals.push({
        label: tips[key].name,
        value: tips[key].name,
        id: tips[key].id
      });
    }

    tip_ac.autocomplete({
      source: vals,
      minLength: 0,
      position: {
        my: "left top-3",
        at: "left bottom"
      },
      select: function(event, ui) {

        var tip = "";
        for (var key in tips) {
          if (tips[key].id == ui.item.id) {
            tip = tips[key];
            break;
          }
        }
        $scope.selectService(tip);
      }
    });

    tip_ac.on("focus", function() {
      $($(this)).autocomplete("search", "");
    }).on("blur", function() {
      $($(this)).autocomplete("close");
    });

    setTimeout(function() {
      $(".switch:nth-child(1)").click();
    }, 0);

  };

  $scope.selectService = function(service) {
    $scope.currentService = service;
    if (!$scope.$$phase) {
      $scope.$apply();
    }
  };

  $scope.currencyClass = function(string) {
    $scope.currencyClassVal = string;
  };

  $scope.currencyClassVal = "glyphicon-rub";

    $scope.addService = function ($event) {
        if (!$($event.currentTarget).hasClass('active')) {
            return false;
        }
        var service = {};
        var area_ac = $(".area_ac");
        var institute_ac = $(".institute_ac");

        if (!$scope.currentArea) {
            $scope.currentArea = {name: ""};
        }
        if (!$scope.currentInstitute) {
            $scope.currentInstitute = {name: ""};
        }

        if (area_ac.val() != $scope.currentArea.name && area_ac.val() != "") {

            console.log("area_ac", area_ac.val());
            service.branch_name = area_ac.val();
            service.branchText = service.branch_name;

            console.log("institute_ac", institute_ac.val());
            service.institute_name = institute_ac.val();
            service.instituteText = service.institute_name;

        } else {

            service.branch = $scope.currentArea.id;
            service.branchText = $scope.currentArea.name;

            if (institute_ac.val() != $scope.currentInstitute.name && institute_ac.val() != "") {

                console.log("institute_ac", institute_ac.val());
                service.institute_name = institute_ac.val();
                service.instituteText = service.institute_name;

            } else {

                console.log("institute id", $scope.currentInstitute.id);
                service.parent = $scope.currentInstitute.id;
                service.instituteText = $scope.currentInstitute.name;

            }

        }

        service.area = $scope.currentArea.name,
            service.institute = $scope.currentInstitute.name,
            //service.parent: $scope.currentInstitute.id,
            service.name = $scope.currentService.name,
            service.tipText = $scope.currentService.name,
            service.priceFrom = $scope.priceFrom,
            service.priceTo = $scope.priceTo,
            service.currency = $scope.currentCurrency.id,
            service.fa_class = $scope.currentCurrency.fa_class

        $scope.addedServices.push(service);

        $scope.currentArea = {};
        $scope.currentInstitute = {};
        $scope.q3 = '';
        $scope.q4 = '';
        $scope.q5 = '';
        $scope.priceFrom = '';
        $scope.priceTo = '';
    };

  $scope.removeService = function(index) {
    delete $scope.addedServices.splice(index, 1);
    console.log(index);
  };

  $scope.addedServices = [];

  $scope.selectlang = function(lang) {
    $scope.lang = lang;
    lawandgaugeapp.lang = $scope.lang;
    var hash = location.hash;
    console.log($scope.lang);
    hash = hash.split("/");
    hash[1] = lang;
    hash = hash[0] + "/" + hash[1] + "/" + hash[2];
    location.hash = hash;

    var formData = new FormData;
    formData.append("lang", $scope.lang);

    $http.post('/localization/lang/', formData, {
      transformRequest: angular.identity,
      headers: {
        'Content-Type': undefined
      }
    }).success(function(data) {
      console.log(data);
      $scope.getTranslations();
      $scope.getServices();
      $scope.getCountries();

    }).error(function(data) {
      console.log(data);
    });
  };

  $scope.getHtml = function(input) {
    $scope.customHtml = $sce.trustAsHtml(input);
  }

}]);

lawandgaugeapp.controller('mapCtrl', ['$scope', '$http', '$location', '$sce', function($scope, $http, $location, $sce) {
  $scope.initMap = function() {
    //resize map
    $('#map_style').height($('#map_style').width() / 3);
    //slider marker
    var cnt = 0;
    $('body').on('click', '.bal_sl_arr', function() {
      if ($(this).hasClass('baslarr_right')) {
        cnt++;
        if (cnt > $('.bal_slide').length - 1) {
          cnt = 0;
        }
      } else {
        cnt--;
        if (cnt < 0) {
          cnt = $('.bal_slide').length - 1;
        }
      }
      $('.bal_slider').css({
        'margin-left': '-' + cnt * 286 + 'px'
      });
      $('.bal_slider_name_wrp').css({
        'margin-left': '-' + cnt * 45 + 'px'
      });
      $('.ogn_bal').removeClass('active');
      $('.ogn_bal:nth-child('+(cnt+1)+')').addClass('active');
    });

    var markers = [];
    var map;
    var sizeFactor = 0;

    function initialize() {
      var styles = [{
        "stylers": [{
          "color": "#5b5bc9"
        }]
      }, {
        "elementType": "labels.text",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#3f628c"
        }]
      }, {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#2e4766"
        }]
      }, {
        "featureType": "landscape.natural.landcover",
        "stylers": [{
          "color": "#23344e"
        }]
      }, {
        "featureType": "landscape.natural.terrain",
        "stylers": [{
          "color": "#23344e"
        }]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{
          "visibility": "off"
        }]
      }];

      // Create a new StyledMapType object, passing it the array of styles,
      // as well as the name to be displayed on the map type control.
      var styledMap = new google.maps.StyledMapType(styles, {
        name: "Styled Map"
      });

      // Create a map object, and include the MapTypeId to add
      // to the map type control.
      var mapOptions;
      mapOptions = {
        zoom: 3,
        center: new google.maps.LatLng(55.8134104, 37.7063927),
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
        }
      };
      map = new google.maps.Map(document.getElementById('map_style'),
        mapOptions);

      $http.get('/map/data/', {
        language: lawandgaugeapp.lang
      }).success(function(data) {
        for (var i = 0; i < data.data.length; i++) {
          if (sizeFactor < data.data[i].count) {
            sizeFactor = data.data[i].count;
          }
        }
        for (var i = 0; i < data.data.length; i++) {
          createMarker(data.data[i]);
        }
        setMarkersMap(map, markers)
        console.log('/map/data/', data.data);
      });

      //Associate the styled map with the MapTypeId and set it to display.
      map.mapTypes.set('map_style', styledMap);
      map.setMapTypeId('map_style');

      function setMarkersMap(map, markers) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      function createMarker(place) {
        var ns = Math.floor(35 * (place.count / (sizeFactor / 100)) / 100);
        var ps =  ns / 2;
        if (ns < 8) {
          ns = 8;
          ps =  ns / 2;
        }
        var markerImage = new google.maps.MarkerImage('/static/landing/img/regularBall.png',
          new google.maps.Size(34, 35), //size
          new google.maps.Point(0, 0), //origin point
          new google.maps.Point(ps, ps), // offset point
          new google.maps.Size(ns, ns)
        );
        var markerImageHover = new google.maps.MarkerImage('/static/landing/img/regularBall2.png',
          new google.maps.Size(34, 35), //size
          new google.maps.Point(0, 0), //origin point
          new google.maps.Point(17, 17), // offset point
          new google.maps.Size(34, 35)
        );
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(place.lat, place.lng),
          map: map,
          icon: markerImage,
          title: place.name
        });

        markers.push(marker);

        // function cizz(n,c){
        //     return c[0]+((/^[0,2-9]?[1]$/.test(n))?c[2]:((/^[0,2-9]?[2-4]$/.test(n))?c[3]:c[1]))
        // }
        // var jur_word = ['юрист','ов','','а'];
        // var texxxt = place.count + ' ' + cizz(place.count,jur_word);
        var tooltip_content = '<div class="balloon"><div class="bal_slider nclear">';
        for (var i = 0; i < place.contractors.length; i++) {
          tooltip_content += '<div class="bal_slide">' +
            '<div class="bal_slide_name">' + place.contractors[i].first_name + ' ' + place.contractors[i].last_name + '</div><div class="bal_poi">' +
            '<div style="background-image: url(' + place.contractors[i].avatar + ');" class="bal_poi_ava"></div>' +
            '</div>' +
            '</div>';
        }
        tooltip_content += '</div>' +
          // '<div class="bal_count">'+texxxt+'</div>'+
          '<div class="bal_slider_name_wrp"><div class="bal_slider_name nclear">';
        for (var i = 0; i < place.contractors.length; i++) {
          tooltip_content += '<div class="ogn_bal '+ (i==0?'active':'') +'"></div>';
        }
        tooltip_content += '</div></div><div class="bal_city">' + place.name + '</div>';
        if (place.contractors.length != 1) {
          tooltip_content += '<div class="bal_sl_arr baslarr_left"></div><div class="bal_sl_arr baslarr_right"></div>';
        }
        tooltip_content += '</div>';

        var tooltip_content2 = '<div class="bala2_name">'+ place.name +'</div>';
        var var_infobox_props = {
          content: tooltip_content,
          disableAutoPan: false,
          maxWidth: 0,
          pixelOffset: new google.maps.Size(-171, -373),
          zIndex: null,
          boxClass: "myInfobox",
          closeBoxMargin: "2px",
          closeBoxURL: "/static/landing/img/close_bu.png",
          infoBoxClearance: new google.maps.Size(1, 1),
          visible: true,
          pane: "floatPane",
          enableEventPropagation: true
        };
        var var_infobox_props2 = {
          content: tooltip_content2,
          disableAutoPan: false,
          maxWidth: 265,
          pixelOffset: new google.maps.Size(-132, 20),
          zIndex: null,
          boxClass: "myInfobox2",
          closeBoxMargin: "2px",
          closeBoxURL: "",
          infoBoxClearance: new google.maps.Size(1, 1),
          visible: true,
          pane: "floatPane",
          enableEventPropagation: true
        };

        var var_infobox = new InfoBox(var_infobox_props);
        google.maps.event.addListener(var_infobox, 'closeclick', function() {
          marker.setMap(map);
        });


        google.maps.event.addListener(marker, 'click', function() {
          var_infobox.open(map, marker);
          marker.setMap(null);
        });

        var var_infobox2 = new InfoBox(var_infobox_props2);
        google.maps.event.addListener(marker, 'mouseover', function() {
          var_infobox2.open(map, marker);
          marker.setIcon(markerImageHover);
        });

        google.maps.event.addListener(marker, 'mouseout', function() {
          var_infobox2.close();
          marker.setIcon(markerImage);
        });

      }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
  };
  $scope.initMap();
}]);

lawandgaugeapp.config(['$httpProvider', "$locationProvider", '$interpolateProvider',
  function($httpProvider, $locationProvider, $interpolateProvider) {

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  }
]);
