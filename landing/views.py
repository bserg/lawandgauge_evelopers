from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import FormView
from django.views.generic.base import TemplateView

from landing.forms import ClientIntentionForm, LawyerIntentionForm, ResumeForm
from landing.models import ResumeToken
from landing_builder.models import Partner
from lawandgauge.mixins import JsonResponseMixin


class LandingView(TemplateView):
    template_name = 'landing/index.html'

    def get_context_data(self, **kwargs):
        context = super(LandingView, self).get_context_data(**kwargs)
        context['partners'] = Partner.objects.all()
        return context

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, request, *args, **kwargs):
        return super(LandingView, self).dispatch(request, *args, **kwargs)


class ResumeView(JsonResponseMixin, FormView):
    template_name = 'landing/contractor_extra_registration.html'
    form_class = ResumeForm

    def form_valid(self, form):
        object = form.save()
        try:
            ResumeToken.objects.filter(token=self.kwargs['token']).update(active=False)
        except ValueError:
            return self.json_fail(error=_('Unexpected error'))
        return self.json_success(msg=_('Resume sent successfully'), id=object.pk)

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)

    def dispatch(self, request, *args, **kwargs):
        try:
            if not ResumeToken.objects.filter(active=True, token=self.kwargs['token']).exists():
                raise PermissionDenied
        except:
            raise PermissionDenied
        return super(ResumeView, self).dispatch(request, *args, **kwargs)


class IntentionView(JsonResponseMixin, FormView):
    form_class = None

    def form_valid(self, form):
        obj = form.save()
        return self.json_success(msg=_('Your application for registration is accepted'), id=obj.pk)

    def form_invalid(self, form):
        return self.json_fail(error=form.errors)


class ClientIntentionView(IntentionView):
    form_class = ClientIntentionForm


class LawyerIntentionView(IntentionView):
    form_class = LawyerIntentionForm