# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0004_auto_20150426_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resumetoken',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u044d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u043e\u0439 \u043f\u043e\u0447\u0442\u044b'),
        ),
    ]
