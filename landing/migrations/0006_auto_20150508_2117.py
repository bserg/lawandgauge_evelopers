# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0005_auto_20150426_1841'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='clientintention',
            options={'ordering': ['-date'], 'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430 \u043d\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044e \u043a\u043b\u0438\u0435\u043d\u0442\u0430', 'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438 \u043d\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044e \u043a\u043b\u0438\u0435\u043d\u0442\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='lawyerintention',
            options={'ordering': ['-date'], 'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0430 \u043d\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044e \u044e\u0440\u0438\u0441\u0442\u0430', 'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438 \u043d\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044e \u044e\u0440\u0438\u0441\u0442\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='resume',
            options={'verbose_name': '\u0420\u0435\u0437\u044e\u043c\u0435'},
        ),
        migrations.AlterModelOptions(
            name='resumetoken',
            options={'verbose_name': '\u0420\u0435\u0437\u044e\u043c\u0435 \u0442\u043e\u043a\u0435\u043d', 'verbose_name_plural': '\u0420\u0435\u0437\u044e\u043c\u0435 \u0442\u043e\u043a\u0435\u043d\u044b'},
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='message',
            field=models.TextField(verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='country',
            field=models.CharField(max_length=255, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='country',
            field=models.CharField(max_length=255, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430', null=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='linkedin',
            field=models.CharField(max_length=100, verbose_name='\u0410\u043a\u043a\u0430\u0443\u043d\u0442 LinkedIn', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='name',
            field=models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='resume',
            field=models.FileField(upload_to=b'resume', null=True, verbose_name='\u0420\u0435\u0437\u044e\u043c\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='services',
            field=models.TextField(verbose_name='\u0423\u0441\u043b\u0443\u0433\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='site',
            field=models.CharField(max_length=100, verbose_name='\u0421\u0430\u0439\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='specialization',
            field=models.TextField(verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='status',
            field=models.PositiveSmallIntegerField(verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u042e\u0440\u0438\u0441\u0442'), (2, '\u0410\u0434\u0432\u043e\u043a\u0430\u0442'), (3, '\u041d\u043e\u0442\u0430\u0440\u0438\u0443\u0441'), (4, '\u0411\u0443\u0445\u0433\u0430\u043b\u0442\u0435\u0440')]),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='active',
            field=models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e'),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='\u042d\u043b\u0435\u043a\u0442\u0440\u043e\u043d\u043d\u0430\u044f \u043f\u043e\u0447\u0442\u0430'),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, verbose_name='\u0422\u043e\u043a\u0435\u043d', editable=False),
        ),
    ]
