# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0007_resume_email'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='clientintention',
            options={'ordering': ['-date'], 'verbose_name': 'Client intention', 'verbose_name_plural': 'Client intentions'},
        ),
        migrations.AlterModelOptions(
            name='lawyerintention',
            options={'ordering': ['-date'], 'verbose_name': 'Lawyer intention', 'verbose_name_plural': 'Lawyer intentions'},
        ),
        migrations.AlterModelOptions(
            name='resume',
            options={'verbose_name': 'Resume'},
        ),
        migrations.AlterModelOptions(
            name='resumetoken',
            options={'verbose_name': 'Resume token', 'verbose_name_plural': 'Resume tokens'},
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='message',
            field=models.TextField(verbose_name='Message'),
        ),
        migrations.AlterField(
            model_name='clientintention',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='country',
            field=models.CharField(max_length=255, verbose_name='Country'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='lawyerintention',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='country',
            field=models.CharField(max_length=255, verbose_name='Country'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date', null=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Email', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='linkedin',
            field=models.CharField(max_length=100, verbose_name='LinkedIn', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='phone',
            field=models.CharField(max_length=100, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='resume',
            field=models.FileField(upload_to=b'resume', null=True, verbose_name='Resume', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='services',
            field=models.TextField(verbose_name='Services', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='site',
            field=models.CharField(max_length=100, verbose_name='Site', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='specialization',
            field=models.TextField(verbose_name='Specialization', blank=True),
        ),
        migrations.AlterField(
            model_name='resume',
            name='status',
            field=models.PositiveSmallIntegerField(verbose_name='Status', choices=[(1, 'Lawyer'), (2, 'Advocate'), (3, 'Notary'), (4, 'Accountant')]),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date'),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='resumetoken',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, verbose_name='Token', editable=False),
        ),
    ]
