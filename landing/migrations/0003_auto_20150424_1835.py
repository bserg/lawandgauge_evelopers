# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0002_resume'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResumeToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=255, verbose_name='Token')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Date')),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
            ],
            options={
                'verbose_name': 'Resume token',
                'verbose_name_plural': 'Resume tokens',
            },
        ),
        migrations.AddField(
            model_name='resume',
            name='date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date', null=True),
        ),
    ]
