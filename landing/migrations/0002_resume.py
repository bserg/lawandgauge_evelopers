# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('phone', models.CharField(max_length=100, verbose_name='Phone')),
                ('country', models.CharField(max_length=255, verbose_name='Country')),
                ('status', models.PositiveSmallIntegerField(verbose_name='Status', choices=[(1, 'Lawyer'), (2, 'Advocate'), (3, 'Notary'), (4, 'Accountant')])),
                ('site', models.CharField(max_length=100, verbose_name='Site', blank=True)),
                ('linkedin', models.CharField(max_length=100, verbose_name='LinkedIn', blank=True)),
                ('resume', models.FileField(upload_to=b'resume', null=True, verbose_name='Resume', blank=True)),
                ('specialization', models.TextField(verbose_name='Specialization', blank=True)),
                ('services', models.TextField(verbose_name='Services', blank=True)),
            ],
            options={
                'verbose_name': 'Resume',
            },
        ),
    ]
