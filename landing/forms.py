from django import forms

from landing.models import ClientIntention, LawyerIntention, Resume


class ClientIntentionForm(forms.ModelForm):
    class Meta:
        model = ClientIntention
        exclude = ('date',)


class LawyerIntentionForm(forms.ModelForm):
    class Meta:
        model = LawyerIntention
        exclude = ('date',)


class ResumeForm(forms.ModelForm):
    class Meta:
        model = Resume
        exclude = ('date',)
