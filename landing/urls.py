from django.conf.urls import patterns, url
from landing.models import ClientIntention
from landing.views import ClientIntentionView, LawyerIntentionView, LandingView, ResumeView

urlpatterns = patterns('',
    url(r'^$', LandingView.as_view(), name='root'),
    url(r'^resume/(?P<token>[\w\-]+)/$', ResumeView.as_view(), name='resume'),
    url(r'^cl/', ClientIntentionView.as_view(), name='client_intention'),
    url(r'^lw/', LawyerIntentionView.as_view(), name='lawyer_intention'),
)