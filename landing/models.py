import uuid
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import Context
from django.template.loader import get_template
from django.utils.translation import ugettext as _
from account.models import User
from lawandgauge.settings import SITE_URL, DEFAULT_FROM_EMAIL


class ClientIntention(models.Model):
    phone = models.CharField(_('Phone'), max_length=100)
    email = models.EmailField(_('Email'))
    message = models.TextField(_('Message'))
    date = models.DateTimeField(_('Date'), auto_now_add=True)

    def __unicode__(self):
        return self.message

    class Meta:
        ordering = ['-date']
        verbose_name = _('Client intention')
        verbose_name_plural = _('Client intentions')


class LawyerIntention(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    phone = models.CharField(_('Phone'), max_length=100)
    email = models.EmailField(_('Email'))
    country = models.CharField(_('Country'), max_length=255)
    date = models.DateTimeField(_('Date'), auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-date']
        verbose_name = _('Lawyer intention')
        verbose_name_plural = _('Lawyer intentions')


@receiver(post_save, sender=ClientIntention)
@receiver(post_save, sender=LawyerIntention)
def intention_send_mail(sender, instance, **kwargs):
    if sender is ClientIntention:
        template_name = 'email/client_intention.html'
    elif sender is LawyerIntention:
        template_name = 'email/lawyer_intention.html'
    else:
        return

    html = get_template(template_name).render(Context({'instance': instance}))
    send_mail(
        _('Application for registration from lawandgauge.com'),
        '',
        'no-reply@lawandgauge.ru',
        [instance.email], html_message=html
    )


RESUME_STATUS = (
    (1, _('Lawyer')),
    (2, _('Advocate')),
    (3, _('Notary')),
    (4, _('Accountant')),
)


class Resume(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    phone = models.CharField(_('Phone'), max_length=100)
    email = models.EmailField(_('Email'), blank=True)
    country = models.CharField(_('Country'), max_length=255)
    status = models.PositiveSmallIntegerField(_('Status'), choices=RESUME_STATUS)
    site = models.CharField(_('Site'), max_length=100, blank=True)
    linkedin = models.CharField(_('LinkedIn'), max_length=100, blank=True)
    resume = models.FileField(_('Resume'), upload_to='resume', blank=True, null=True)
    specialization = models.TextField(_('Specialization'), blank=True)
    services = models.TextField(_('Services'), blank=True)
    date = models.DateTimeField(_('Date'), auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Resume')


class ResumeToken(models.Model):
    email = models.EmailField(_('Email'))
    token = models.UUIDField(_('Token'), default=uuid.uuid4, editable=False)
    date = models.DateTimeField(_('Date'), auto_now_add=True)
    active = models.BooleanField(_('Active'), default=True)

    def url(self):
        return SITE_URL + reverse('landing:resume', kwargs={'token': str(self.token)})

    def save(self, *args, **kwargs):
        html = get_template('email/resume.html').render(Context({'url': self.url()}))
        send_mail(
            _('Access the page to send the resume on the website lawandgauge.com'),
            _('You can send your CV via the form on the website. To do this, click on the link ') + self.url(),
            DEFAULT_FROM_EMAIL,
            [self.email],
            html
        )
        return super(ResumeToken, self).save(*args, **kwargs)

    def __unicode__(self):
        return str(self.token)

    class Meta:
        verbose_name = _('Resume token')
        verbose_name_plural = _('Resume tokens')
